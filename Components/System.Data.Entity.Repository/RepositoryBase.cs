﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Transactions;
using System.Data.Entity.Core.Metadata.Edm;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Repository.Attributes;
using System.Data.Entity.Repository.Model;

namespace System.Data.Entity.Repository
{
    public class RepositoryBase<T> : IRepositoryBase, IDisposable where T : DbContext
    {
        public DbContext Model;

        internal TransactionTypes   _transactionType = TransactionTypes.DbTransaction;
        internal TransactionScope   _transactionScope;
        internal IsolationLevel     _isolationLevel = IsolationLevel.ReadCommitted;
        internal DbTransaction      _transaction;
        internal DbConnection       _connection;
        
        internal bool _proxyCreationEnabled = false;
        internal bool _rethrowExceptions = true;
        internal bool _useTransaction = false;//true;
        internal bool _saveLastExcecutedMethodInfo = false;

        internal int _commandTimeout = 300;

        internal string _connectionString = string.Empty;

        internal MethodBase _lastExecutedMethod = null;

        public event RepositoryBaseExceptionHandler RepositoryBaseExceptionRaised;
        public delegate void RepositoryBaseExceptionHandler(Exception exception);

        public event RepositoryBaseRollBackOccuredHandler RepositoryBaseRollBackRaised;
        public delegate void RepositoryBaseRollBackOccuredHandler(MethodBase lastExecutedMethod);

        internal static bool _staticLazyConnectionOverrideUsed = false;
        internal static bool _lazyConnection = false;

        internal void InitializeRepository()
        {
            if (Model == null)
            {
                //Commented to implement the unit of work
               /* DbContext instance = (DbContext)Activator.CreateInstance(typeof(T));
                ((IObjectContextAdapter)instance).ObjectContext.CommandTimeout = 300;
                Model = instance;

                if (_lazyConnection == false)
                    InitializeConnection();

                Model.Configuration.ProxyCreationEnabled = _proxyCreationEnabled;*/
                throw (new Exception("Repository Base cannot initialize the repository on a null model."));
            }
            else
            {
                Model.Configuration.LazyLoadingEnabled = false;
            }
        }

        internal void InitializeConnection()
        {
            if (Model != null)
            {
                //Commented to implement the unit of work
                /*if (!string.IsNullOrEmpty(_connectionString)) 
                {
                    Model.Database.Connection.ConnectionString = _connectionString;
                }*/

                _connection = ((IObjectContextAdapter)Model).ObjectContext.Connection;
                _connection.Open();
            }
            else
            {
                throw (new Exception("Repository Base cannot set connection from a null model."));
            }
        }

        public RepositoryBase()
        {
            //InitializeRepository();
        }

        public RepositoryBase(DbContext model)
        {
            this.Model = model;
        }

        public RepositoryBase(
            bool throwExceptions,
            bool lazyConnection = false,
            TransactionTypes transactionType = TransactionTypes.DbTransaction,
            IsolationLevel isolationLevel = IsolationLevel.Snapshot,
            bool useTransactions = true,
            bool proxyCreationEnabled = false,
            int commandTimeout = 300,
            bool saveLastExecutedMethodInfo = false,
            DbContext model =null)
        {
            this.Model = model;
            _rethrowExceptions = throwExceptions;
            _useTransaction = useTransactions;
            _proxyCreationEnabled = proxyCreationEnabled;
            _commandTimeout = commandTimeout;
            _isolationLevel = IsolationLevel.ReadCommitted;
            _saveLastExcecutedMethodInfo = saveLastExecutedMethodInfo;

            if (_staticLazyConnectionOverrideUsed == false)
                _lazyConnection = lazyConnection;

            InitializeRepository();
        }

        public RepositoryBase(RepositoryBaseConfiguration configuration)
        {
            _rethrowExceptions = configuration.RethrowExceptions;
            _useTransaction = configuration.UseTransaction;
            _proxyCreationEnabled = configuration.ProxyCreationEnabled;
            _commandTimeout = configuration.CommandTimeout;
            _isolationLevel = configuration.IsolationLevel;
            _connectionString = configuration.ConnectionString;
            _saveLastExcecutedMethodInfo = configuration.SaveLastExecutedMethodInfo;

            if (_staticLazyConnectionOverrideUsed == false)
                _lazyConnection = configuration.LazyConnection;

            InitializeRepository();
        }

        #region functions
        public R Add<R>(R entity) where R : class //Starts transacton - Does not commmit
        {
            R result = entity;

            ProcessTransactionableMethod(() =>
            {
                try
                {
                    SetEntity<R>()
                        .Add(entity);

                    SaveChanges();

                    result = entity;
                }
                catch (Exception error)
                {
                    var entry = Model.Entry(entity);
                    entry.State = EntityState.Unchanged;

                    RollBack();
                    Detach(entity);

                    if (_rethrowExceptions)
                    {
                        throw;
                    }
                    else
                    {
                        if (RepositoryBaseExceptionRaised != null)
                        {
                            OnRepositoryBaseExceptionRaised(error);
                        }
                    }
                }
                finally
                { }

            });
            
            return result;
        }

        public bool AddMultiple<R>(List<R> entities) where R : class //Does not start Transaction - Does Not commit
        {
            R result;

            entities.ForEach(e => result = Add<R>(e)); // Needs better error handling

            return true;
        }

        public bool AddMultipleWithCommit<R>(List<R> entities) where R : class //Starts transaction(?) - Commits for each record
        {
            bool result = false;

            entities.ForEach(e =>
            {

                R result2 = Add<R>(e); // needs better error handling
                CommitTransaction(startNewTransaction: true); // commits each record
            });

            return result;
        }

        public bool AddOrUpdate<R>(R entity) where R : class //starts new transaction - Does not commit
        {
            bool result = false;

            ProcessTransactionableMethod(() =>  
            {
                try
                {
                    var entry = SetEntry(entity);

                    if (entry != null)
                    {
                        if (entry.State == EntityState.Detached)
                        {
                            entry.State = EntityState.Added;
                        }
                        else
                        {
                            entry.State = EntityState.Modified;
                        }
                    }
                    else
                    {
                        Model.Set<R>().Attach(entity);
                    }

                    SaveChanges();

                    result = true;
                }
                catch (Exception e)
                {
                    var entry = Model.Entry(entity);
                    entry.State = EntityState.Unchanged;

                    RollBack();
                    Detach(entity);
                    throw (e);
                }
                finally
                { }
            });

            return result;
        }

        public bool AddOrUpdateMultiple<R>(List<R> entities) where R : class // Starts nes transaction - Does not commit
        {
            bool result = false;
            entities.ForEach(e => result = AddOrUpdate<R>(e));
            return result;
        }

        public bool AddOrUpdateMultipleCommit<R>(List<R> entities) where R : class //starts new transaction. - Commits each record.
        {
            bool result = false;

            entities.ForEach(e => {
                result = AddOrUpdate<R>(e);
                CommitTransaction(startNewTransaction: true);
            });

            return result;
        }

        public void CommitTransaction(bool startNewTransaction = false)
        {
            if (_useTransaction)
            {
                switch (_transactionType)
                {
                    case TransactionTypes.DbTransaction:
                        if (_transaction != null && _transaction.Connection != null)
                        {
                            SaveChanges();
                            _transaction.Commit();
                        }
                        break;

                    case TransactionTypes.TransactionScope:
                        try
                        {
                            if(_transactionScope != null)
                                _transactionScope.Complete();
                        }
                        catch (Exception error)
                        {
                            if (_rethrowExceptions)
                            {
                                throw;
                            }
                            else
                            {
                                if (RepositoryBaseExceptionRaised != null)
                                {
                                    RepositoryBaseExceptionRaised(error);
                                }
                            }
                        }

                        break;
                }

                if (startNewTransaction)
                    StartTransaction();
            }
            else
            {
                SaveChanges();
            }
        }

        public Int32 Count<R>() where R : class
        {
            return Model.Set<R>()
                .Count();
        }

        public bool Delete<R>(R entity) where R : class //starts transaction - does not commit
        {
            bool result = false;

            ProcessTransactionableMethod(() =>
            {
                try
                {
                    var entry = SetEntry(entity);

                    if (entry != null)
                    {
                        entry.State = System.Data.Entity.EntityState.Deleted;
                    }
                    else
                    {
                        Model.Set<R>().Attach(entity);
                    }

                    Model.Set<R>().Remove(entity);

                    result = true;
                }
                catch (Exception error)
                {
                    var entry = Model.Entry(entity);
                    entry.State = EntityState.Unchanged;

                    RollBack();
                    Detach(entity);

                    if (_rethrowExceptions)
                    {
                        throw;
                    }
                    else
                    {
                        if (RepositoryBaseExceptionRaised != null)
                        {
                            RepositoryBaseExceptionRaised(error);
                        }
                    }
                }
                finally
                { }

            });

            return result;
        }

        public bool DeleteMultiple<R>(List<R> entities) where R : class
        {
            bool result = false;

            entities.ForEach(e => result = Delete<R>(e));

            return result;
        } //Does not start Transaction - Does Not commit

        public bool DeleteMultipleWithCommit<R>(List<R> entities) where R : class
        {
            bool result = false;

            entities.ForEach(e =>
            {
                result = Delete<R>(e);
                CommitTransaction(startNewTransaction: true);
            });

            return result;
        } //Starts transaction (?) - commits each record.

        public void Detach(object entity)
        {
            var objectContext = ((IObjectContextAdapter)Model).ObjectContext;
            var entry = Model.Entry(entity);

            if (entry.State != EntityState.Detached)
                objectContext.Detach(entity);
        }

        public void Detach(List<object> entities)
        {
            entities.ForEach(e => Detach(e));
        }

        public IQueryable<R> Find<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = default(IQueryable<R>);

            ProcessTransactionableMethod(() => {
                entities = SetEntities<R>().Where(where);
            });

            return entities;
        } //starts transaction

         public IQueryable<R> GetPaged<R>(Expression<Func<R, bool>> where,int Page,int PerPage) where R : class //does not start transaction
        {
            IQueryable<R> entities = default(IQueryable<R>);

            ProcessTransactionableMethod(() => {
                entities = SetEntities<R>().Where(where).Skip((Page-1)*PerPage).Take(PerPage) ;
            });

            return entities;
        }

        public IQueryable<R> Find<R>(Expression<Func<R, bool>> where, params Expression<Func<R, object>>[] includes) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            ProcessTransactionableMethod(() => 
            { 
                if (includes != null)
                {
                    entities = ApplyIncludesToQuery<R>(entities, includes);
                }

                entities = entities.Where(where);
            });

            return entities;
        }

        public R First<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity =  entities
                    .First(where);
            });

            return entity;
        }

        public R FirstOrDefault<R>(Expression<Func<R, bool>> where, params Expression<Func<R, object>>[] includes) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                if (where != null)
                    entities = entities.Where(where);

                if (includes != null)
                {
                    entities = ApplyIncludesToQuery<R>(entities, includes);
                }

                entity = entities.FirstOrDefault();
            });

            return entity;
        }

        public IQueryable<R> GetAll<R>() where R : class
        {
            IQueryable<R> entities = default(IQueryable<R>);

            ProcessTransactionableMethod(() =>
            {
                entities = SetEntities<R>().AsNoTracking();
            });

            return entities;
        }

        public IQueryable<R> GetAll<R>(params Expression<Func<R, object>>[] includes) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            if (includes != null)
            {
                entities = ApplyIncludesToQuery<R>(entities, includes);
            }

            return entities;
        }

        public DbConnection GetConnection()
        {
            return _connection;
        }

        public void SaveChanges()
        {
                Model.SaveChanges();
        }

        public void SetIdentityCommand()
        {
            List<EntitySetBase> sets;

            var container =
                   ((IObjectContextAdapter)Model).ObjectContext.MetadataWorkspace
                      .GetEntityContainer(
                            ((IObjectContextAdapter)Model).ObjectContext.DefaultContainerName,
                            DataSpace.CSpace);

            sets = container.BaseEntitySets.ToList();

            foreach (EntitySetBase set in sets)
            {
                string command = string.Format("SET IDENTITY_INSERT {0} {1}", set.Name, "ON");
                ((IObjectContextAdapter)Model).ObjectContext.ExecuteStoreCommand(command);
            }
        }

        public void SetConnectionString(string connectionString)
        {
            if (_lazyConnection == true)
            {
                _connectionString = connectionString;
                InitializeConnection();
            }
        }

        public void SetIsolationLevel(IsolationLevel isolationLevel)
        {
            _isolationLevel = isolationLevel;
        }

        public void SetRethrowExceptions(bool rehtrowExceptions)
        {
            _rethrowExceptions = rehtrowExceptions;
        }

        public void SetTransactionType(TransactionTypes transactionType)
        {
            _transactionType = transactionType;
        }

        public void SetUseTransaction(bool useTransaction)
        {
            _useTransaction = useTransaction;
        }

        public R Single<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .Single(where);
            });

            return entity;
        }

        public R SingleOrDefault<R>(Expression<Func<R, bool>> where) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .SingleOrDefault(where);
            });

            return entity;
        }

        public R SingleOrDefault<R>(Expression<Func<R, bool>> where, Expression<Func<R, object>> include) where R : class
        {
            IQueryable<R> entities = SetEntities<R>();

            R entity = default(R);

            ProcessTransactionableMethod(() =>
            {
                entity = entities
                    .Include(include)
                    .SingleOrDefault(where);
            });

            return entity;
        }

        private void AtualizarFks<R>(R dbEntidade)
        {
            var fks = Helpers.RecuperarPropriedadesPelosAtributos(new[] { typeof(ForeignKeyAttribute), typeof(FluentMappingFKAttribute) }, typeof(R));

            foreach (var fk in fks)
            {
                Object fkEntity = fk.GetValue(dbEntidade) as Object;

                if (fkEntity != null)
                {
                    var ent = Model.Set(fkEntity.GetType()).Find(8);

                    if (ent != null)
                    {
                        Model.Entry(ent).State = EntityState.Unchanged;
                        fk.SetValue(dbEntidade, ent);
                    }
                }
                else
                {
                    IEnumerable<EntidadeBase> listFks = fk.GetValue(dbEntidade) as IEnumerable<EntidadeBase>;

                    if (listFks != null && listFks.Count() > 0)
                    {
                        Type type = typeof(List<>).MakeGenericType(listFks.GetType().GetGenericArguments()[0]);
                        dynamic newListFks = Activator.CreateInstance(type);
                        //var convertedNewListFks = Convert.ChangeType(newListFks, type);

                        foreach (var listItem in listFks as IEnumerable<EntidadeBase>)
                        {
                            dynamic ent = Model.Set(listItem.GetType()).Find(listItem.ID);

                            if (ent != null)
                            {
                                Model.Entry(ent).State = EntityState.Unchanged;
                            }

                            ent = ent ?? listItem;

                            newListFks.Add(ent);
                        }

                        fk.SetValue(dbEntidade, Convert.ChangeType(newListFks, type));
                    }
                }
            }
        }

        public bool Update<R>(R entity) where R : class
        {
            bool result = false;
            //AtualizarFks(entity);

            ProcessTransactionableMethod(() =>
            {
                try
                {
                    var entry = SetEntry(entity);

                    if (entry != null)
                    {
                        
                        entry.State = EntityState.Modified;
                        
                    }
                    else
                    {
                        Model.Set<R>().Attach(entity);
                    }

                    SaveChanges();

                    result = true;
                }
                catch (Exception error)
                {
                    var entry = Model.Entry(entity);
                    entry.State = EntityState.Unchanged;

                    RollBack();
                    Detach(entity);

                    if (_rethrowExceptions)
                    {
                        throw;
                    }
                    else
                    {
                        if (RepositoryBaseExceptionRaised != null)
                        {
                            RepositoryBaseExceptionRaised(error);
                        }
                    }
                }
                finally
                { }
            });

            return result;
        }

        public bool UpdateProperty<R>(R entity, params Expression<Func<R, object>>[] properties) where R : class
        {
            bool result = false;

            ProcessTransactionableMethod(() =>
            {
                try
                {
                    var entry = SetEntry(entity);

                    if (entry != null)
                    {
                        Model.Set<R>().Attach(entity);

                        foreach (var property in properties)
                        {
                            MemberExpression body = property.Body as MemberExpression;

                            if (body == null)
                            {
                                UnaryExpression ubody = (UnaryExpression)property.Body;
                                body = ubody.Operand as MemberExpression;
                            }

                            entry.Property(body.Member.Name).IsModified = true;
                        }
                    }
                    else
                    {
                        Model.Set<R>().Attach(entity);
                    }

                    SaveChanges();

                    result = true;
                }
                catch (Exception error)
                {
                    var entry = SetEntry(entity);

                    foreach (var property in properties)
                    {
                        MemberExpression body = property.Body as MemberExpression;

                        if (body == null)
                        {
                            UnaryExpression ubody = (UnaryExpression)property.Body;
                            body = ubody.Operand as MemberExpression;
                        }

                        entry.Property(body.Member.Name).IsModified = false;
                    }

                    RollBack();
                    Detach(entity);

                    if (_rethrowExceptions)
                    {
                        throw;
                    }
                    else
                    {
                        if (RepositoryBaseExceptionRaised != null)
                        {
                            RepositoryBaseExceptionRaised(error);
                        }
                    }
                }
                finally
                { }
            });

            return result;
        }

        public bool UpdateMultiple<R>(List<R> entities) where R : class
        {
            bool result = false;

            entities.ForEach(e => result = Update<R>(e));

            return result;
        }

        public bool UpdateMultipleWithCommit<R>(List<R> entities) where R : class
        {
            bool result = false;

            entities.ForEach(e => 
            {
                result = Update<R>(e);
                CommitTransaction(startNewTransaction: true);
            });

            return result;
        }

        public static void UseLazyConnection(bool useLazyConnection)
        {
            _lazyConnection = useLazyConnection;
            _staticLazyConnectionOverrideUsed = true;
        }

        internal IQueryable<R> ApplyIncludesToQuery<R>(IQueryable<R> entities, Expression<Func<R, object>>[] includes) where R : class
        {
            if (includes != null)
                entities = includes.Aggregate(entities, (current, include) => current.Include(include));

            return entities;
        }

        internal void ProcessTransactionableMethod(Action action)
        {
            if(_saveLastExcecutedMethodInfo)
                _lastExecutedMethod = MethodInfo.GetCurrentMethod();
            
            StartTransaction();
            action();
        }

        internal IQueryable<R> SetEntities<R>() where R : class
        {
            IQueryable<R> entities = Model.Set<R>();

            return entities;
        }

        internal DbSet<R> SetEntity<R>() where R : class
        {
            DbSet<R> entity = Model.Set<R>();

            return entity;
        }

        internal DbEntityEntry SetEntry<R>(R entity) where R : class
        {

            DbEntityEntry entry = Model.Entry(entity);

            return entry;
        }

        internal IQueryable<T> GetQuery(Expression<Func<T, object>> include)
        {
            IQueryable<T> entities = SetEntities<T>()
                .Include(include);

            return entities;
        }

        internal void RollBack()
        {
            if (_useTransaction)
            {
                if (_transactionType == TransactionTypes.DbTransaction)
                {
                    if (_transaction != null && _transaction.Connection != null)
                    {
                        _transaction.Rollback();

                        if (RepositoryBaseRollBackRaised != null)
                        {
                            RepositoryBaseRollBackRaised(_lastExecutedMethod);
                        }
                    }
                }
            }
        }

        internal void StartTransaction()
        {
            if (_useTransaction)
            {
                switch(_transactionType)
                {
                    case TransactionTypes.DbTransaction:
                        if (_transaction == null || _transaction.Connection == null)
                            _transaction = _connection.BeginTransaction(_isolationLevel);
                        break;

                    case TransactionTypes.TransactionScope:
                        _transactionScope = new TransactionScope();
                        break;
                }
            }
        }
        #endregion

        protected void OnRepositoryBaseExceptionRaised(Exception e)
        {
            RepositoryBaseExceptionHandler handler = RepositoryBaseExceptionRaised;
            if (handler != null)
            {
                handler(e);
            }
        }

        public void Dispose()
        {
            CommitTransaction();

            _transaction = null;
            _transactionScope = null;

            // Check if this can be reverted. There was a problem with closing connections with MS ServiceLocator.
            /*if (_connection != null && _connection.State != ConnectionState.Closed)
            {
                _connection.Close();
                _connection = null;
            }*/

            // Check if this can be deleted safely
            if (Model.Database.Connection != null && Model.Database.Connection.State != ConnectionState.Closed)
            {
                Model.Database.Connection.Close();
                Model.Database.Connection.Dispose();
            }

            if (!Object.Equals(Model, null))
            {
                Model.Dispose();
                Model = null;
            }
        }
    }
}
