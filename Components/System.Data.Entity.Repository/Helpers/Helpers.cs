﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace System.Data.Entity.Repository
{
    public static class Helpers
    {
        public static IList<PropertyInfo> RecuperarPropriedadesPelosAtributos(Type[] tipoAtributos, Type tipoEntidade)
        {
            IList<PropertyInfo> propsAttr = new List<PropertyInfo>();

            var props = tipoEntidade.GetProperties();

            foreach (var prop in props)
            {
                foreach (var tipoAtributo in tipoAtributos)
                {
                    object attr = prop.GetCustomAttribute(tipoAtributo);

                    if (attr != null)
                    {
                        propsAttr.Add(prop);
                    }
                }
            }

            return propsAttr;
        }

        public static bool EntidadeForte(Type tipoEntidade, string propriedade)
        {
            var prop = tipoEntidade.GetProperty(propriedade);

            object attr = prop.GetCustomAttribute(typeof(ForeignKeyAttribute));

            return attr != null;
        }
    }
}
