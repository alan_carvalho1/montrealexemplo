﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace System.Data.Entity.Repository.Model
{
    
    public abstract class EntidadeBase
    {
        [Key]
        public virtual int ID { get; set; } // Id (Primary key)
        
        public virtual DateTime? DataModificacao { get; set; }
        
        public virtual string Excluido { get; set; }
    }
}
