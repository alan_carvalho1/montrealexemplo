﻿using System.Collections.Generic;
using GenericSearch.Core;
using GenericSearch.UI;

using GenericSearch.Paging;

namespace GenericSearch.UI.Models
{
    public class PagedSearchViewModel
    {
        public PagedResult<object> Data { get; set; }

        public IEnumerable<AbstractSearch> SearchCriteria { get; set; }
    }
}