﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web.Mvc;
using GenericSearch.UI;

namespace GenericSearch.Core    
{
    [ModelBinder(typeof(AbstractSearchModelBinder))]
    public abstract class AbstractSearch
    {
        string _LabelText;
        public string Property { get; set; }

        public string TargetTypeName { get; set; }
        public int    DisplayOrder = 9999;
        public int    LabelWidth=2;
        public int    OperatorWidth=2;
        public int    FieldWidth=8;
        public int    FullWidth = 12;
        public bool   DisplayOperator = true;

        public AbstractSearch SetLabel(string Newlabel)
        {
            LabelText = Newlabel;      
            return this;
        }
        public AbstractSearch SetOrder(int NewOrder)
        {
            DisplayOrder = NewOrder;      
            return this;
        }
        public AbstractSearch SetWidth(int FullWidth1,int LabelWidth1,int OperatorWidth1,int FieldWidth1)
        {
            LabelWidth=LabelWidth1;
            OperatorWidth=OperatorWidth;
            FieldWidth=FieldWidth1;
            FullWidth = FullWidth1;
            return this;
        }
        public AbstractSearch SetDisplayOperator(bool Show)
        {
            DisplayOperator = Show;
      
            return this;
        }

        public string LabelText
        {
            get
            {

                if (this.Property == null)
                {
                    return null;
                }
                if (_LabelText!=null)
                    return _LabelText;
                var arg = Expression.Parameter(Type.GetType(this.TargetTypeName), "p");
                var propertyInfo = this.GetPropertyAccess(arg).Member as PropertyInfo;

                if (propertyInfo != null)
                {
                    var displayAttribute = propertyInfo.GetCustomAttributes(true)
                        .OfType<DisplayAttribute>()
                        .Cast<DisplayAttribute>()
                        .FirstOrDefault();

                    if (displayAttribute != null)
                    {
                        return displayAttribute.Name;
                    }
                }

                string[] parts = this.Property.Split('.');

                return parts[parts.Length - 1];
            }
            set { _LabelText = value; }
        }

        internal IQueryable<T> ApplyToQuery<T>(IQueryable<T> query)
        {
            var arg = Expression.Parameter(typeof(T), "p");
            var property = this.GetPropertyAccess(arg);

            Expression searchExpression = null;

            if (property.Type.IsGenericType && property.Type.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                searchExpression = this.BuildExpression(Expression.Property(property, "Value"));
            }
            else
            {
                searchExpression = this.BuildExpression(property);
            }

            if (searchExpression == null)
            {
                return query;
            }
            else
            {
                var predicate = CreatePredicateWithNullCheck<T>(searchExpression, arg, property);
                return query.Where(predicate);
            }
        }

        protected abstract Expression BuildExpression(MemberExpression property);

        private MemberExpression GetPropertyAccess(ParameterExpression arg)
        {
            string[] parts = this.Property.Split('_');// Alan $$$ Nenhum campo deve conter underline ... somente campos que usados no join

            MemberExpression property = Expression.Property(arg, parts[0]);

            for (int i = 1; i < parts.Length; i++)
            {
                property = Expression.Property(property, parts[i]);
            }

            return property;
        }

        private Expression<Func<T, bool>> CreatePredicateWithNullCheck<T>(Expression searchExpression, ParameterExpression arg, MemberExpression targetProperty)
        {
            string[] parts = this.Property.Split('_');

            Expression nullCheckExpression = null;
            if (parts.Length > 1)
            {
                MemberExpression property = Expression.Property(arg, parts[0]);
                nullCheckExpression = Expression.NotEqual(property, Expression.Constant(null));

                for (int i = 1; i < parts.Length - 1; i++)
                {
                    property = Expression.Property(property, parts[i]);
                    Expression innerNullCheckExpression = Expression.NotEqual(property, Expression.Constant(null));

                    nullCheckExpression = Expression.AndAlso(nullCheckExpression, innerNullCheckExpression);
                }
            }

            if (!targetProperty.Type.IsValueType || (targetProperty.Type.IsGenericType && targetProperty.Type.GetGenericTypeDefinition() == typeof(Nullable<>)))
            {
                var innerNullCheckExpression = Expression.NotEqual(targetProperty, Expression.Constant(null));

                if (nullCheckExpression == null)
                {
                    nullCheckExpression = innerNullCheckExpression;
                }
                else
                {
                    nullCheckExpression = Expression.AndAlso(nullCheckExpression, innerNullCheckExpression);
                }
            }

            if (nullCheckExpression == null)
            {
                return Expression.Lambda<Func<T, bool>>(searchExpression, arg);
            }
            else
            {
                var combinedExpression = Expression.AndAlso(nullCheckExpression, searchExpression);

                var predicate = Expression.Lambda<Func<T, bool>>(combinedExpression, arg);

                return predicate;
            }
        }
    }
}
