﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace GenericSearch.Core
{
    public class NumericSearch : AbstractSearch
    {
        public int? SearchTerm { get; set; }

        public NumericComparators Comparator { get; set; }

        protected override Expression BuildExpression(MemberExpression property)
        {
            if (!this.SearchTerm.HasValue)
            {
                return null;
            }

            Expression searchExpression = this.GetFilterExpression(property);

            return searchExpression;
        }

        private Expression GetFilterExpression(MemberExpression property)
        {
            switch (this.Comparator)
            {
                case NumericComparators.Less:
                    return Expression.LessThan(property, Expression.Constant(this.SearchTerm.Value));
                case NumericComparators.LessOrEqual:
                    return Expression.LessThanOrEqual(property, Expression.Constant(this.SearchTerm.Value));
                case NumericComparators.Equal:
                    return Expression.Equal(property, Expression.Constant(this.SearchTerm.Value));
                case NumericComparators.GreaterOrEqual:
                    return Expression.GreaterThanOrEqual(property, Expression.Constant(this.SearchTerm.Value));
                case NumericComparators.Greater:
                    return Expression.GreaterThan(property, Expression.Constant(this.SearchTerm.Value));
                default:
                    throw new InvalidOperationException("Comparator not supported.");
            }
        }

        private bool _Lookup;
        public bool Lookup 
        { get {return _Lookup;}
        }
        public SelectList Items;
        public NumericSearch Setitems(SelectList  Items1)
        {
            this.Comparator = NumericComparators.Equal;
   
            if (Items1 != null)
            {
                Items = new SelectList(Items1.Items,Items1.DataValueField,Items1.DataTextField);
                _Lookup = true;
            }
            else
            {
                _Lookup = false;
                Items = null;
            }
            return this;
        }
         
    }

    public enum NumericComparators
    {
        [Display(Name = "menor que")]
        Less,

        [Display(Name = "menor ou igual à")]
        LessOrEqual,

        [Display(Name = "igual ")]
        Equal,

        [Display(Name = "maior ou igual à")]
        GreaterOrEqual,

        [Display(Name = "maior que")]
        Greater
    }
}
