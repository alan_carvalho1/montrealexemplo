﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Web.Mvc;

namespace GenericSearch.Core
{
    public class TextSearch : AbstractSearch
    {
        public string SearchTerm { get; set; }

        public TextComparators Comparator { get; set; }

        protected override Expression BuildExpression(MemberExpression property)
        {
            if (this.SearchTerm == null)
            {
                return null;
            }

            var searchExpression = Expression.Call(
                property,
                typeof(string).GetMethod(this.Comparator.ToString(), new[] { typeof(string) }),
                Expression.Constant(this.SearchTerm));

            return searchExpression;
        }

        private bool _Lookup;
        public bool Lookup 
        { get {return _Lookup;}
        }

        
        public SelectList Items;
        public TextSearch Setitems(SelectList  Items1)
        {
            //SelectList Items;
            this.Comparator = TextComparators.Equals;
   
            if (Items1 != null)
            {
                Items = new SelectList(Items1.Items,Items1.DataValueField,Items1.DataTextField);
                _Lookup = true;
            }
            else
            {
                _Lookup = false;
                Items = null;
            }
            return this;
        }
    }

    public enum TextComparators
    {
        [Display(Name = "contém")]
        Contains,

        [Display(Name = "igual à")]
        Equals,

        [Display(Name = "começa com")]
        StartsWith,
    }
}
