﻿using System.Collections.Generic;
using GenericSearch.Core;


namespace GenericSearch.UI.Models
{
    public class SearchViewModel
    {
        public IEnumerable<object> Data { get; set; }

        public ICollection<AbstractSearch> SearchCriteria { get; set; }
    }
}