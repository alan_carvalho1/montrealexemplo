﻿using AutoMapper;
using GF.Data.Model;
using GF.Web.Models;

namespace GF.Web
{

    public class AutoMapperConfig
    {
        public static void Config()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.SourceMemberNamingConvention = new LowerUnderscoreNamingConvention();
                cfg.DestinationMemberNamingConvention = new PascalCaseNamingConvention();
                cfg.CreateMissingTypeMaps = true;
                cfg.ValidateInlineMaps = false;
                cfg.ForAllMaps((c, d) => { c.PreserveReferences = true; });
                /*
                cfg.CreateMap<Pessoa, PessoaViewModel>();
                cfg.CreateMap<Funcionario, FuncionarioViewlModel>();
                cfg.CreateMap<Aluno, AlunoViewModel>();

                cfg.CreateMap<AlunoViewModel, Aluno>();
                cfg.CreateMap<FuncionarioViewlModel, Funcionario>();
                cfg.CreateMap<PessoaViewModel, Pessoa>();
                */
            });
        }
    }
}