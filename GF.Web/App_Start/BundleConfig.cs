﻿using System.Web;
using System.Web.Optimization;

namespace GF.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/animate.css",
                      "~/Content/Plugins/DatePicker/bootstrap-datepicker.css",
                      "~/Content/Plugins/DatePicker/bootstrap-datetimepicker.css",
                      "~/Content/Plugins/DatePicker/jquery.timepicker.min.css",
                      "~/Content/Plugins/ICheck/Custom.css",
                      "~/Content/Plugins/Chosen/chosen.min.css",
                      "~/Content/fullcalendar.css",
                      "~/Content/style.css",
                      "~/Content/select2.css",
                      "~/Content/jquery.steps.css"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-{version}.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/methods_pt.js"
            ));

             bundles.Add(new ScriptBundle("~/bundles/jqueryunobtrusive").Include(
                "~/Scripts/jquery.unobtrusive-ajax.js",
                "~/Scripts/jquery.validate.unobtrusive.js"
            ));


            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"
            ));

            bundles.Add(new ScriptBundle("~/plugins/datatables").Include(
               "~/Scripts/plugins/dataTables/dataTables.js",
               "~/Scripts/plugins/dataTables/dataTables.bootstrap.js",
               "~/Scripts/plugins/dataTables/dataTables.keyTable.js"

            ));
            

            bundles.Add(new StyleBundle("~/font-awesome/css").Include(
                      "~/fonts/font-awesome/css/font-awesome.css"));


          
/*
             bundles.Add(new ScriptBundle("~/plugins/editors").Include(
               //Menu
               "~/Scripts/plugins/metisMenu/jquery.metisMenu.js",
               //datepicker
               "~/Scripts/plugins/moment/moment.js",
               "~/Scripts/plugins/datapicker/bootstrap-datepicker.js",
               "~/Scripts/plugins/datapicker/bootstrap-datepicker.pt-BR.js",
               "~/Scripts/plugins/datapicker/bootstrap-datetimepicker.js",
               //Input com mascara
               "~/Scripts/plugins/jasny/jasny-bootstrap.min.js",
               //Checkbox e Radio Button
               "~/Scripts/plugins/ICheck/icheck.js",
               
               //Caxa de seleção multipla
               "~/Scripts/plugins/chosen/chosen.jquery.min.js",
               "~/Scripts/plugins/chosen/ajax-chosen.min.js"
           ));*/

            bundles.Add(new ScriptBundle("~/bundles/inspinia").Include(
                "~/Scripts/plugins/metisMenu/jquery.metisMenu.js", 
                "~/Scripts/plugins/moment/moment.js",
                "~/Scripts/jquery.mask.js",
                "~/Scripts/plugins/datapicker/bootstrap-datepicker.js",
               "~/Scripts/plugins/datapicker/bootstrap-datetimepicker.js",
               "~/Scripts/plugins/datapicker/bootstrap-datepicker.pt-BR.js",
               "~/Scripts/plugins/datapicker/jquery.timepicker.js~",
             //"~/Scripts/plugins/jasny/jasny-bootstrap.min.js",
                "~/Scripts/plugins/ICheck/icheck.js",
                "~/Scripts/plugins/PersistJS/persist-min.js",
                "~/Scripts/plugins/chosen/chosen.jquery.min.js",
                "~/Scripts/plugins/chosen/ajax-chosen.min.js",
                "~/Scripts/app/inspinia.js"
                      ));

               bundles.Add(new ScriptBundle("~/bundles/DatePicker").Include(
                      "~/Scripts/plugins/datapicker/bootstrap-datepicker.js",
                      "~/Scripts/plugins/datapicker/bootstrap-datetimepicker.js",
                      "~/Scripts/plugins/datapicker/bootstrap-datepicker.pt-BR.js",
                      "~/Scripts/plugins/datapicker/jquery.timepicker.js"

                      
            ));
           
        BundleTable.EnableOptimizations = false;    
        }
    }
}
