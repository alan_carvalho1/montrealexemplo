﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using GenericSearch.Core;
using GenericSearch.UI;
using Application.Helpers;
using System.Web.Security;
using System.Security.Principal;

namespace GF.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {


        void Application_Error1(object sender, EventArgs e)
        {
            var error = Server.GetLastError();
            var code = (error is HttpException) ? (error as HttpException).GetHttpCode() : 500;
            HttpContext httpContext = HttpContext.Current;
            string Codigo = Guid.NewGuid().ToString();
            
            
            if (code != 500)
            {
                string Message = (@"<h1>"+code+"</h1><h3 class='font-bold'>"+(error as HttpException).GetHtmlErrorMessage() + "</h3></div>");                           
                HttpContext.Current.Application[Codigo] = Message;
                httpContext.Response.Redirect(@"~/Home/Error/"+Codigo);
            }
            else
            {
                if (httpContext != null)
                {
                    RequestContext requestContext = ((MvcHandler)httpContext.CurrentHandler).RequestContext;
                    /* when the request is ajax the system can automatically handle a mistake with a JSON response. then overwrites the default response */
                    if (requestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        httpContext.Response.Clear();
                        string controllerName = requestContext.RouteData.GetRequiredString("controller");
                        IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
                        IController controller = factory.CreateController(requestContext, controllerName);
                        ControllerContext controllerContext = new ControllerContext(requestContext, (ControllerBase)controller);

                        JsonResult jsonResult = new JsonResult();
                        jsonResult.Data = new { success = false, serverError = "500" };
                        jsonResult.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                        jsonResult.ExecuteResult(controllerContext);
                        httpContext.Response.End();
                    }
                    else
                    {
                        string ExceptionMessage = error.Message;
                        if (error.InnerException != null) ExceptionMessage = ExceptionMessage + "<br/>" + error.InnerException.Message;
                        string Message= (@"<h1>"+code+"</h1><h3 class='font-bold'>Erro Interno do Servidor<br/>"+ExceptionMessage+"</h3></div>"); 
                        Server.ClearError();
                        HttpContext.Current.Application[Codigo] = Message;
                        httpContext.Response.Redirect("~/Home/Error/"+Codigo);
                    }
                }
            }
        }
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            AutoMapperConfig.Config();

            ModelBinders.Binders.Add(typeof(AbstractSearch) , new AbstractSearchModelBinder());
            ModelBinders.Binders.Add(typeof(JQueryDataTablesModel), new JQueryDataTablesModelBinder());

            
        }

        protected void Application_PreRequestHandlerExecute()
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR");
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            var authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
            {
                var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                var roles = authTicket.UserData.Split(new Char[] { ',' });
                var userPrincipal = new GenericPrincipal(new GenericIdentity(authTicket.Name), roles);
                Context.User = userPrincipal;
            }
        }
    }
}
