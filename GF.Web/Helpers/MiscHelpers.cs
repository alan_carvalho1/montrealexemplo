﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Application.Helpers
{


  
    public class MiscHelpers
    {


        public static SelectList GetStatusList()
        {
                SelectList sl = new SelectList( new[]{
                new SelectListItem{ Text="Ativo", Value="A"},
                new SelectListItem{ Text="Inativo", Value="I"},
                }, "Value", "Text" );
            return sl;
        }
    }
}