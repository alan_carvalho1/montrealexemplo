﻿using System;

public static class DateTimeExtensions
{
    public static int GetTotalMonths(this DateTime Source)
    {
        int month = 0;
        DateTime current = Source;
        while (current.Ticks > new DateTime(1, 1, 31).Ticks)
        {
            current = current.AddMonths(-1);
            month += 1;
        }
        return month;
    }
    public static int GetTotalDays(this DateTime Source)
    {
        return Convert.ToInt32(Math.Floor(new TimeSpan(Source.Ticks).TotalDays));
    }
}