﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using GenericSearch.UI.Models;
using GenericSearch.Core;
using Newtonsoft.Json;
using GenericSearch.UI;

namespace Application.Helpers
/*
 * Esta classe permite que a ordenação de um ienumerable possa ser feita usndo um paramentro
 * tipo string contendo o nome do campo a ser usado para ordenar.
 * http://blog.cincura.net/229310-sorting-in-iqueryable-using-string-as-column-name/
 * */
{
    public static class SortByFieldNameHelper
    {
        private static IOrderedQueryable<T> zOrderingHelper<T>(IQueryable<T> source, string propertyName, bool descending, bool anotherLevel)
        {
            ParameterExpression param = Expression.Parameter(typeof(T), string.Empty); // I don't care about some naming
            MemberExpression property = Expression.PropertyOrField(param, propertyName);
            LambdaExpression sort = Expression.Lambda(property, param);
            MethodCallExpression call = Expression.Call(
                typeof(Queryable),
                (!anotherLevel ? "OrderBy" : "ThenBy") + (descending ? "Descending" : string.Empty),
                new[] { typeof(T), property.Type },
                source.Expression,
                Expression.Quote(sort));
            return (IOrderedQueryable<T>)source.Provider.CreateQuery<T>(call);
        }

        public static IOrderedQueryable<T> OrderingHelper<T>(IQueryable<T> source, string propertyName, bool descending, bool anotherLevel)
        {
            ParameterExpression param = Expression.Parameter(typeof(T), string.Empty); // I don't care about some naming

            Expression body = param;
            foreach (var member in propertyName.Split('.'))
            {
                body = Expression.PropertyOrField(body, member);
            }

            LambdaExpression sort = Expression.Lambda(body, param);
            MethodCallExpression call = Expression.Call(
                typeof(Queryable),
                (!anotherLevel ? "OrderBy" : "ThenBy") + (descending ? "Descending" : string.Empty),
                new[] { typeof(T), body.Type },
                source.Expression,
                Expression.Quote(sort));

            return (IOrderedQueryable<T>)source.Provider.CreateQuery<T>(call);
        }

        public static IOrderedQueryable<T> sOrder<T>(this IQueryable<T> source, string propertyName,bool Asc)
        {
            try
            {
                if (Asc)
                    return OrderingHelper(source, propertyName, true, false);
                else
                    return OrderingHelper(source, propertyName, false, false);
            }
            catch 
            { 
                return (IOrderedQueryable<T>)source;
            }
        }

        public static IOrderedQueryable<T> sOrderBy<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, false);
        }

        public static IOrderedQueryable<T> sOrderByDescending<T>(this IQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, true, false);
        }

        public static IOrderedQueryable<T> sThenBy<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, false, true);
        }

        public static IOrderedQueryable<T> sThenByDescending<T>(this IOrderedQueryable<T> source, string propertyName)
        {
            return OrderingHelper(source, propertyName, true, true);
        }
    }

    [Serializable()]
    public class GridViewModel
    {
        public ICollection<AbstractSearch> _SearchCriteria {get;set;}
        public GridViewModel() { }
        public GridViewModel(ICollection<AbstractSearch> SearchCriteria1)
        {
            _SearchCriteria = SearchCriteria1 ;                 
        }
        public ICollection<AbstractSearch> GetSearchCriteria()
        {
            try
            {
                return _SearchCriteria;
            }
            catch
            {
                return null;
            }
        }
    }
}
