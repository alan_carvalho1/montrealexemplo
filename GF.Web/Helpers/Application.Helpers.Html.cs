﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Application.Helpers.Html
{

    public static class HMTLHelperExtensions
    {
        //esta helper é usado pelo menu para macar o menu ativo
        public static string IsSelected(this HtmlHelper html, string controller = null, string action = null)
        {
            string cssClass = "active";
            string currentAction = (string)html.ViewContext.RouteData.Values["action"];
            string currentController = (string)html.ViewContext.RouteData.Values["controller"];

            if (String.IsNullOrEmpty(controller))
                controller = currentController;

            if (String.IsNullOrEmpty(action))
                action = currentAction;

            return (controller.ToUpper().Contains(currentController.ToUpper()) && (action.ToUpper().Contains(currentAction.ToUpper())) ? cssClass : String.Empty);
        }

    }
}