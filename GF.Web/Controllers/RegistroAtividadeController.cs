// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using AutoMapper;
using System.Data.Entity.Repository.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using static GF.Data.Model.ConstantesModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace GF.Controllers
{

    //[Authorize(Roles = "Admin")]
    [Authorize]
    public partial class RegistroAtividadeController : Controller
    {
        const string GridInfoKey = "RegistroAtividade" + "GridData";
        private UnitOfWork db = new UnitOfWork();

        ILog log = LogManager.GetLogger<RegistroAtividadeController>();

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult NoFilter()
        {
            Session[GridInfoKey] = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ICollection<AbstractSearch> SearchCriteria1;
                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                if (GridState != null && GridState.GetSearchCriteria() != null)
                {
                    SearchCriteria1 = GridState.GetSearchCriteria();
                }
                else
                {
                    SearchCriteria1 = CustomizeSearch(typeof(RegistroAtividade).GetDefaultSearchCriterias());
                }

                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = SearchCriteria1
                };
                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }

            return View();

        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Index(ICollection<AbstractSearch> searchCriteria)
        {
            try
            {
                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = CustomizeSearch(searchCriteria)
                };
                GridViewModel GridConfig = new GridViewModel(searchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            return View();

        }


        // GET: /RegistroAtividade/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create(int Funcionario_ID, DateTime DataRegistro)
        {
            try
            {
                RegistroAtividade registroAtividade = new RegistroAtividade();
                InicializarCombos(Funcionario_ID);
                registroAtividade.Funcionario_ID = Funcionario_ID;
                registroAtividade.DataRegistro = DataRegistro;
                registroAtividade.DataRegistroTexto = DataRegistro.ToString("dd/MM/yyyy");

                TempData["titulo"] = "Novo Lançamento";
                
                return View(registroAtividade);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }

            return View();
        }

        private void InicializarCombos(int Professor_ID)
        {
            var funcionarios = db.FuncionarioRepository.GetAllFuncioanrios(k => k.Pessoa).OrderBy(y => y.Pessoa.Nome).ToList();
            if (Professor_ID != 0)
            {
                funcionarios = funcionarios.Where(x => x.ID == Professor_ID).ToList();
            }
            var fun = funcionarios.Select(x => new
            {
                ID = x.ID,
                Nome = x.Pessoa.CPF + " - " + x.Pessoa.Nome
            });
            ViewBag._Funcionarios = new SelectList(fun.ToList(), "ID", "Nome");


            //var contratos = db.RegistroAtividadeRepository.GetAllRegistroAtividades().Where(x => x.Excluido == ConstantesModel.Excluido.NAO).Select(x => x.Contrato_ID);

            var contratos1 = db.ContratoRepository.GetAllContratos(y => y.Aluno, y => y.Aluno.Pessoa).Where(x => x.Excluido == ConstantesModel.Excluido.NAO)
            //    .Where(k => !contratos.Contains(k.ID))
                .OrderBy(y => y.Aluno.Pessoa.Nome);
            var A = contratos1.Select(x => new
            {
                ID = x.ID,
                Nome = x.Aluno.Pessoa.Nome
            });
            ViewBag._Alunos = new SelectList(A, "ID", "Nome");
        }

        [Authorize(Roles = "Administrador")]
        public ActionResult Encaminhar(RegistroAtividade registroAtividade)
        {
            InicializarCombos(0);
            registroAtividade.DataRegistro = DateTime.Now;
            return View("Encaminhar", "_LayoutModal", registroAtividade);


        }


        // POST: /RegistroAtividade/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RegistroAtividade transacoes)
        {
            try
            {
                
                Validar(transacoes, ModelState);
                if (ModelState.IsValid)
                {
                    //transacoes.DataEntrada = DateTime.Now;
                    db.RegistroAtividadeRepository.AddRegistroAtividade(transacoes);

                    return this.Json(new { Msg = ConstantesModel.Mensagens.ADD_SUCESS, ID = transacoes.ID }, JsonRequestBehavior.AllowGet);

                }
                

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                InicializarCombos(0);
                return this.Json(new { Msg = ConstantesModel.Mensagens.ERRO, ID = 0 }, JsonRequestBehavior.AllowGet);
            }
            InicializarCombos(0);
            return PartialView("Create", transacoes);
        }

        // POST: /RegistroAtividade/Delete/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                RegistroAtividade carteira;
                carteira = db.RegistroAtividadeRepository.FindRegistroAtividade(y => y.ID == id).FirstOrDefault();

                db.RegistroAtividadeRepository.DeleteRegistroAtividade(carteira);

                return this.Json(new { Msg = Mensagens.DELETE_SUCESS, ID = carteira.ID }, JsonRequestBehavior.AllowGet);
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex1)
            {
                //Console.Write(ex1.Message);
                {
                    var sqlex = ex1.InnerException.InnerException as SqlException;

                    if (sqlex != null)
                    {
                        switch (sqlex.Number)
                        {
                            case 547:
                                TempData["popup-alert"] = "Esse registro possui relaciomento e não pode ser apagado!";
                                break; //FK exception
                            case 2627:
                            case 2601:
                                TempData["popup-alert"] = "Registro Duplicado!";
                                break; //primary key exception

                            default: throw sqlex; //otra excepcion que no controlo.


                        }
                    }

                    //throw ex1;
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);


                return View();
            }
        }
        

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;
            try
            {
                //int[] arrayStatus = new int[] { ConstantesModel.ConstTipoTransacao.RECEITA, ConstantesModel.ConstTipoTransacao.MULTA, ConstantesModel.ConstTipoTransacao.MENSALIDADE, ConstTipoTransacao.DESPESA, ConstantesModel.ConstTipoTransacao.SALARIO };
                IQueryable<RegistroAtividade> Result = db.RegistroAtividadeRepository.GetAllRegistroAtividades(y => y.Funcionario, y => y.Funcionario.Pessoa);


                if (jQueryDataTablesModel.iSortingCols > 0 && jQueryDataTablesModel.iSortCol_ != null)
                {
                    SortBy = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_[0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                IQueryable<RegistroAtividade> ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<RegistroAtividade> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<RegistroAtividade>)(ResultFiltered.sOrder(SortBy, SortOrder == "asc").Skip(page).Take(limit)));
                

                var result2 = ResultOrdered
                    .ToList()
                    .GroupBy(k => new { k.Funcionario.Pessoa.Nome, k.Funcionario_ID, k.DataRegistro })
                    .Select(item => new
                    {
                        Funcionario_ID = item.Key.Funcionario_ID,
                        Nome = item.Key.Nome,
                        DataRegistro = item.Key.DataRegistro,
                        parametros = "Funcionario_ID=" + item.Key.Funcionario_ID + "&DataRegistro=" + item.Key.DataRegistro.Value.ToString("MM/dd/yyyy"),
                    });

                int totalRecordCount = result2.Count();
                int searchRecordCount = result2.Count();

                var A = result2.Select(x => new
                {
                    Funcionario = new
                    {
                        Pessoa = new
                        {
                            Nome = x.Nome,
                        },
                    },
                    Funcionario_ID = x.Funcionario_ID,
                    Nome = x.Nome,
                    DataRegistro = x.DataRegistro,
                    parametros = x.parametros,
                }).ToList();

               
                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }


        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxDataCarteira(JQueryDataTablesModel jQueryDataTablesModel, int Funcionario_ID, string DataRegistro)
        {
            string SortBy;
            string SortOrder;
            try
            {
                string[] dataSplit = DataRegistro.Split('/');
                // int dia = Convert.ToInt32(dataSplit[2]);
                DateTime dataFinal = new DateTime(Convert.ToInt32(dataSplit[2]), Convert.ToInt32(dataSplit[0]), Convert.ToInt32(dataSplit[1]));

                IQueryable<RegistroAtividade> Result = db.RegistroAtividadeRepository.GetAllRegistroAtividades(y => y.Funcionario, y => y.Funcionario.Pessoa, k => k.Contrato, k => k.Contrato.Aluno.Pessoa)
                    .Where(x => x.Funcionario_ID == Funcionario_ID)
                    .Where(x => x.DataRegistro == dataFinal);

                if (jQueryDataTablesModel.iSortingCols > 0 && jQueryDataTablesModel.iSortCol_ != null)
                {

                    SortBy = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_[0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                IQueryable<RegistroAtividade> ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<RegistroAtividade> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<RegistroAtividade>)(ResultFiltered.sOrder(SortBy, SortOrder == "asc").Skip(page).Take(limit)));

                int totalRecordCount = Result.Count();
                int searchRecordCount = ResultFiltered.Count();

                var A = ResultOrdered.Select(x => new
                {
                    ID = x.ID,

                    Funcionario = new
                    {
                        Pessoa = new
                        {
                            Nome = x.Funcionario.Pessoa.Nome,
                        }

                    },

                    Contrato = new
                    {
                        Excluido = (x.Contrato.Excluido == "E") ? "Encerrado" : (x.Contrato.Excluido == "S") ? "Excluído" : "Ativo",
                        Aluno = new
                        {
                            Pessoa = new
                            {
                                Nome = x.Contrato.Aluno.Pessoa.Nome,
                            }
                        }

                    },

                    DataRegistro = x.DataRegistro,



                    

                });

                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RegistroAtividade>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }


        }


        //Esta funcao deve ser movida paro o outro arquivo da classe parcial
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
            searchCriteria.Remove("ID");
            searchCriteria.Remove("Contrato_ID");
            searchCriteria.Remove("DataRegistroTexto");
            searchCriteria.GetSearchCriteria("DataRegistro").SetLabel("Data de Registro");
            var funcionarios = db.FuncionarioRepository.GetAllFuncioanrios(k => k.Pessoa).OrderBy(y => y.Pessoa.Nome);
            var fun = funcionarios.Select(x => new
            {
                ID = x.ID,
                Nome = x.Pessoa.Nome
            });
            ((NumericSearch)searchCriteria.GetSearchCriteria("Funcionario_ID"))
                .Setitems(new SelectList(fun
                .ToList(), "ID", "Nome"))
                .SetLabel("Professor")
                .SetOrder(0);


            searchCriteria.Remove("DataEntrada");
            searchCriteria.Remove("DataSaida");


            searchCriteria.Remove("DataModificacao");
            searchCriteria.Remove("Excluido");


            return searchCriteria.Order();
        }

        
        [Authorize(Roles = "Administrador")]
        public bool Validar(RegistroAtividade carteira, ModelStateDictionary ModelState)
        {
            if (carteira.Funcionario_ID == null || carteira.Funcionario_ID == 0)
            {
                ModelState.AddModelError("Funcionario_ID", "Favor escolher um Funcionário.");
            }

            if (carteira.Contrato_ID == null || carteira.Contrato_ID == 0)
            {
                ModelState.AddModelError("Funcionario_ID", "Favor escolher um aluno.");
            }
            return ModelState.Count() == 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
