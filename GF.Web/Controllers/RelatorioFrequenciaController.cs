// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using AutoMapper;
using System.Data.Entity.Repository.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using static GF.Data.Model.ConstantesModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using GF.Web.Models;

namespace GF.Controllers
{

    //[Authorize(Roles = "Admin")]
    [Authorize]
    public partial class RelatorioFrequenciaController : Controller
    {
        const string GridInfoKey = "RelatorioFrequencia" + "GridData";
        private UnitOfWork db = new UnitOfWork();

        ILog log = LogManager.GetLogger<RelatorioFrequenciaController>();

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult NoFilter()
        {
            Session[GridInfoKey] = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()
        {
            try
            {
                ICollection<AbstractSearch> SearchCriteria1;
                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                if (GridState != null && GridState.GetSearchCriteria() != null)
                {
                    SearchCriteria1 = GridState.GetSearchCriteria();
                }
                else
                {
                    SearchCriteria1 = CustomizeSearch(typeof(RelatorioFrequenciaDataViewModel).GetDefaultSearchCriterias());
                }

                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = SearchCriteria1
                };

                var teste = SearchCriteria1.ToList()[0];
                if (((GenericSearch.Core.DateSearch)teste).SearchTerm == null)
                {
                    ((GenericSearch.Core.DateSearch)teste).SearchTerm = DateTime.Now.AddDays(-30);
                    ((GenericSearch.Core.DateSearch)teste).SearchTerm2 = DateTime.Now;
                    ((GenericSearch.Core.DateSearch)teste).Comparator = GenericSearch.Core.DateComparators.InRange;
                }


                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RelatorioFrequenciaDataViewModel>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }

            return View();

        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Index(ICollection<AbstractSearch> searchCriteria)
        {
            try
            {
                


                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = CustomizeSearch(searchCriteria)
                };

                var teste = model.SearchCriteria.ToList()[0];
                if (((GenericSearch.Core.DateSearch)teste).SearchTerm2 == null)
                {
                    
                    if (((GenericSearch.Core.DateSearch)teste).SearchTerm == null)
                    {
                        ((GenericSearch.Core.DateSearch)teste).SearchTerm2 = DateTime.Now;
                        ((GenericSearch.Core.DateSearch)teste).SearchTerm = DateTime.Now.AddDays(-30);
                    } else
                    {
                        ((GenericSearch.Core.DateSearch)teste).SearchTerm2 = ((GenericSearch.Core.DateSearch)teste).SearchTerm.Value.AddDays(30);
                    }


                    
                }
                else
                {
                    if (((GenericSearch.Core.DateSearch)teste).SearchTerm == null)
                    {
                        ((GenericSearch.Core.DateSearch)teste).SearchTerm = ((GenericSearch.Core.DateSearch)teste).SearchTerm2.Value.AddDays(-30);
                    }
                }

                ((GenericSearch.Core.DateSearch)teste).Comparator = GenericSearch.Core.DateComparators.InRange;


                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);

                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<RelatorioFrequenciaDataViewModel>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            return View();

        }

        


        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;
            try
            {
                

                if (jQueryDataTablesModel.iSortingCols > 0 && jQueryDataTablesModel.iSortCol_ != null)
                {

                    SortBy = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_[0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();

                //var abc = GridState._SearchCriteria.ToList();
                AbstractSearch firstDate = GridState._SearchCriteria.ToList().Find(e => e.Property == "DataRegistro");
//                AbstractSearch first = abc.Find(e => e.Property == "DataRegistro");
                    

                RelatorioFrequenciaDataViewModel filtro = new RelatorioFrequenciaDataViewModel();
                filtro.DataInicio = ((GenericSearch.Core.DateSearch)firstDate).SearchTerm ?? DateTime.Now;
                filtro.DataFim = ((GenericSearch.Core.DateSearch)firstDate).SearchTerm2 ?? DateTime.Now;
                IQueryable<RelatorioFrequenciaDataViewModel> Result = db.RegistroAtividadeRepository.RelatorioFrequencia(filtro);



                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                SearchCriteria.Remove("DataRegistro");

                IQueryable<RelatorioFrequenciaDataViewModel> ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<RelatorioFrequenciaDataViewModel> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<RelatorioFrequenciaDataViewModel>)(ResultFiltered.sOrder(SortBy, SortOrder == "asc").Skip(page).Take(limit)));

                //adicionando novametne o abstract search
                SearchCriteria.Add(firstDate);

                int totalRecordCount = Result.Count();
                int searchRecordCount = ResultFiltered.Count();

                var A = ResultOrdered.Select(x => new
                {
                    ID = x.ID,
                    Frequencia = x.Frequencia,
                    AulasPrevistas = x.AulasPrevistas,
                    AulasRealizadas = x.AulasRealizadas,
                    FuncionarioTop = x.FuncionarioTop,
                    qtdFuncionarioTop = x.qtdFuncionarioTop,
                    ProfessorResponsavel = x.ProfessorResponsavel,
                    Aluno = x.Aluno,
                });

                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }


        }

        //Esta funcao deve ser movida paro o outro arquivo da classe parcial
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
            searchCriteria.Remove("ID");


            searchCriteria.Remove("DataInicio");
            searchCriteria.Remove("DataFim");
            searchCriteria.Remove("Domingo");
            searchCriteria.Remove("Segunda");
            searchCriteria.Remove("Terca");
            searchCriteria.Remove("Quarta");
            searchCriteria.Remove("Quinta");
            searchCriteria.Remove("Sexta");
            searchCriteria.Remove("Sabado");

            

            //searchCriteria.Remove("Frequencia");
            searchCriteria.Remove("AulasRealizadas");
            searchCriteria.Remove("AulasPrevistas");
            searchCriteria.Remove("Porcentagem");

            searchCriteria.GetSearchCriteria("DataRegistro").SetLabel("Período").SetOrder(0);
            //searchCriteria.GetSearchCriteria("ProfessorResponsavel").SetLabel("Responsável").SetOrder(1);
            searchCriteria.GetSearchCriteria("Aluno").SetLabel("Aluno").SetOrder(2);
            searchCriteria.Remove("FuncionarioTop");
            searchCriteria.Remove("qtdFuncionarioTop");
            


            return searchCriteria.Order();
        }

        
        [Authorize(Roles = "Administrador")]
        public bool Validar(RelatorioFrequenciaDataViewModel carteira, ModelStateDictionary ModelState)
        {
            
            return ModelState.Count() == 0;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
