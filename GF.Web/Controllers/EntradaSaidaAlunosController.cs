// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning




using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Web.Constantes;
using System.Data.SqlClient;
using GF.Web.Models;
using System.Web.Services;
using Microsoft.Ajax.Utilities;
using static GF.Web.Constantes.Constantes;

namespace GF.Controllers
{
    [Authorize]
    public partial class EntradaSaidaAlunosController : Controller
    {
        const string GridInfoKey = "EntradaSaidaAlunos" + "GridData";
        private UnitOfWork db = new UnitOfWork();

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult NoFilter()
        {
            Session[GridInfoKey] = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()
        {
            RelatorioResultModel result = new RelatorioResultModel();
            result.Ano = DateTime.Now.Year;
            result.StatusVeiculo = "Ativo";
            

            return View(result);
        }

       

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult Index(RelatorioResultModel Relatorio)
        {
            
            return View(Relatorio);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult GetDados(RelatorioResultModel Relatorio)
        {
            RelatorioModel relatorio = GerarRelatorio(Relatorio);


            //JsonResult resul = Json(relatorio, JsonRequestBehavior.AllowGet);

            return this.Json(new { Result = relatorio, ABR = Relatorio }, JsonRequestBehavior.AllowGet);
        }

        private RelatorioModel GerarRelatorio(RelatorioResultModel Relatorio)
        {
            RelatorioModel relatorio = new RelatorioModel();
            relatorio.type = "line";

            DataModel dataModel = new DataModel();
            dataModel.labels = new string[] { "Janeiro", "Fevereiro", "Mar�o", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

            //cada linha existente no gr�fico esta aqui
            IQueryable<Contrato> Result = db.ContratoRepository.GetAllContratos();

            var resultEncerrado = Result
            .OrderBy(k => new { k.Excluido })
            .Where(k => k.Excluido == ConstantesModel.Excluido.CONTRATO_ENCERRADO)
            .Where(k => k.Encerramento.Value.Year == Relatorio.Ano)
            .GroupBy(t => new { t.Excluido, t.Encerramento.Value.Month, t.Encerramento.Value.Year })
            .Select(item => new
            {
                Mes = item.Key.Month,
                Ano = item.Key.Year,
                QtdAlunos = item.Count(),
                Excluido = item.Key.Excluido
            });
            //para contratos ativos
            var ResultAtivo = Result
            .OrderBy(k => new { k.Excluido })
            .Where(k => k.Excluido == ConstantesModel.Excluido.NAO)
            .Where(k => k.DataInicio.Value.Year == Relatorio.Ano)
            .GroupBy(t => new { t.Excluido, t.DataInicio.Value.Month, t.DataInicio.Value.Year })
            .Select(item => new
            {
                Mes = item.Key.Month,
                Ano = item.Key.Year,
                QtdAlunos = item.Count(),
                Excluido = item.Key.Excluido
            });

            var result2 = ResultAtivo.Union(resultEncerrado);


            /************************************************************************************/


            var listaVeiculo = result2
                .Select(item => new
                {
                    Excluido = item.Excluido
                })
                .Distinct();

            DataSetModel[] listDataSet = new DataSetModel[listaVeiculo.Count()];
            int i = 0;
            var listaMedias = result2.ToList();
            Random rdn = new Random();

            listaVeiculo.ForEach(lista =>
            {
                DataSetModel dataSet = new DataSetModel();
                dataSet.label = (lista.Excluido == ConstantesModel.Excluido.CONTRATO_ENCERRADO) ? "Encerrado" : (lista.Excluido == ConstantesModel.Excluido.NAO) ? "Novo" : "Outros";


                if (lista.Excluido == ConstantesModel.Excluido.CONTRATO_ENCERRADO)
                {
                    dataSet.backgroundColor = "rgba(255, 0, 0, 0.8)";
                    dataSet.borderColor = "rgba(255, 0, 0, 0.8)";
                }
                if (lista.Excluido == ConstantesModel.Excluido.NAO)
                {
                    dataSet.backgroundColor = "rgba(28, 18, 171, 1)";
                    dataSet.borderColor = "rgba(28, 18, 171, 1)";
                }


                var Janeiro = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.JANEIRO).FirstOrDefault();
                var Fevereiro = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.FEVEREIRO).FirstOrDefault();
                var Marco = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.MARCO).FirstOrDefault();
                var Abril = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.ABRIL).FirstOrDefault();
                var Maio = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.MAIO).FirstOrDefault();
                var Junho = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.JUNHO).FirstOrDefault();
                var Julho = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.JULHO).FirstOrDefault();
                var Agosto = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.AGOSTO).FirstOrDefault();
                var Setembro = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.SETEMBRO).FirstOrDefault();
                var Outubro = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.OUTUBRO).FirstOrDefault();
                var Novembro = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.NOVEMBRO).FirstOrDefault();
                var Dezembro = listaMedias.Where(a => a.Excluido == lista.Excluido).Where(a => a.Mes == Mes.DEZEMBRO).FirstOrDefault();


                dataSet.data = new decimal[]
                {
                    (Janeiro == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Janeiro.QtdAlunos)),
                    (Fevereiro == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Fevereiro.QtdAlunos)),
                    (Marco == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Marco.QtdAlunos)),
                    (Abril == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Abril.QtdAlunos)),
                    (Maio == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Maio.QtdAlunos)),
                    (Junho == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Junho.QtdAlunos)),
                    (Julho == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Julho.QtdAlunos)),
                    (Agosto == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Agosto.QtdAlunos)),
                    (Setembro == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Setembro.QtdAlunos)),
                    (Outubro == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Outubro.QtdAlunos)),
                    (Novembro == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Novembro.QtdAlunos)),
                    (Dezembro == null) ? 0 : decimal.Parse(String.Format("{0:0.00}", Dezembro.QtdAlunos))

                };

                dataSet.fill = false;

                listDataSet[i] = dataSet;
                i++;
            });


            dataModel.datasets = listDataSet;
            relatorio.data = dataModel;

            //op��es do gr�fico
            OptionsModel optionModel = new OptionsModel();
            optionModel.responsive = true;
            //titulo
            TitleModel title = new TitleModel();
            title.text = "Contratos por M�s";
            title.display = true;
            optionModel.title = title;
            //tooltipsModel
            TooltipsModel tooltipsModel = new TooltipsModel();
            tooltipsModel.intersect = false;
            tooltipsModel.mode = "index";
            optionModel.tooltips = tooltipsModel;
            //hover
            HoverModel hoverModel = new HoverModel();
            hoverModel.intersect = true;
            hoverModel.mode = "nearest";
            optionModel.hover = hoverModel;
            //scales

            ScalesModel scalesModel = new ScalesModel();
            AxesModel x = new AxesModel();
            x.display = true;
            ScaleLabelModel scaleLabel = new ScaleLabelModel();
            scaleLabel.display = true;
            scaleLabel.labelString = "M�s";
            x.scaleLabel = scaleLabel;
            AxesModel y = new AxesModel();
            y.display = true;
            ScaleLabelModel scaleLabel2 = new ScaleLabelModel();
            scaleLabel2.display = true;
            scaleLabel2.labelString = "Valor";
            y.scaleLabel = scaleLabel2;
            ///////////////
            scalesModel.xAxes = x;
            scalesModel.yAxes = y;
            optionModel.scales = scalesModel;

            relatorio.options = optionModel;
            return relatorio;
        }



        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
        
            searchCriteria.GetSearchCriteria("Nome").SetLabel("Nome");

            searchCriteria.Remove("Id");
        

            return searchCriteria.Order();
        }
        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
