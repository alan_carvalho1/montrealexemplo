// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning




using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;

namespace GF.Controllers
{
    [Authorize]
    public partial class GrupoController : Controller
    {
		const string GridInfoKey = "Grupo"+"GridData";
        private UnitOfWork db = new UnitOfWork();

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public  ActionResult NoFilter()
        {
            Session[GridInfoKey]  = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()	
        {

			ICollection<AbstractSearch> SearchCriteria1;
			 GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
			 if (GridState!=null && GridState.GetSearchCriteria()!=null)
			 {
				SearchCriteria1 = GridState.GetSearchCriteria();
			 }
			 else
			 {
				SearchCriteria1 = CustomizeSearch(typeof(Grupo).GetDefaultSearchCriterias());
			 }

            var model = new SearchViewModel()
            {
                Data = null,
                SearchCriteria = SearchCriteria1
            };
            GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
			Session[GridInfoKey] = GridConfig;
            return View(model);
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult  Index(ICollection<AbstractSearch>searchCriteria )	
        {
             var model = new SearchViewModel()
            {
                Data = null,
                SearchCriteria = CustomizeSearch(searchCriteria)
            };
            GridViewModel GridConfig = new GridViewModel(searchCriteria);
			Session[GridInfoKey] = GridConfig;            
            return View(model);
        }


        // GET: /Grupo/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Grupo/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Grupo grupo)
        {
            if (ModelState.IsValid)
            {
				db.GrupoRepository.AddGrupo(grupo);
                db.SaveChanges();
				db.GrupoRepository.CommitTransaction();
                return RedirectToAction("Index");
            }

            return View(grupo);
        }

        // GET: /Grupo/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
			Grupo grupo =  db.GrupoRepository.FindGrupoByGrupoId((int)id);
            
            if (grupo == null)
            {
                return HttpNotFound();
            }
            return View(grupo);
        }

        // POST: /Grupo/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Grupo grupo)
        {
            if (ModelState.IsValid)
            {
                db.GrupoRepository.UpdateGrupo(grupo);
                db.SaveChanges();
				db.GrupoRepository.CommitTransaction();
                return RedirectToAction("Index");
            }
            return View(grupo);
        }

        // GET: /Grupo/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Grupo grupo =  db.GrupoRepository.FindGrupoByGrupoId((int)id);
            if (grupo == null)
            {
                return HttpNotFound();
            }
            return View(grupo);
        }

        // POST: /Grupo/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Grupo grupo =  db.GrupoRepository.FindGrupoByGrupoId((int)id);
            db.GrupoRepository.DeleteGrupo(grupo);
            db.SaveChanges();
			db.GrupoRepository.CommitTransaction();
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public JsonResult GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;

            IQueryable<Grupo> Result = db.GrupoRepository.GetAllGrupos();

            if (jQueryDataTablesModel.iSortingCols>0)
            {
                SortBy    = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                SortOrder = jQueryDataTablesModel.sSortDir_ [0];
            }
            else
            {
                SortBy = "Id";
                SortOrder = "asc";
            }

            GridViewModel GridState= (GridViewModel)Session[GridInfoKey];
            ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
            int page = jQueryDataTablesModel.iDisplayStart;
            int limit = jQueryDataTablesModel.iDisplayLength;

            IQueryable <Grupo>ResultFiltered;
            if (SearchCriteria != null)
                ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
            else
                ResultFiltered = Result;

            IOrderedQueryable<Grupo> ResultOrdered;
            ResultOrdered = ((IOrderedQueryable<Grupo>)(ResultFiltered.sOrder(SortBy, SortOrder=="asc").Skip(page).Take(limit)));

            int totalRecordCount=Result.Count();
            int searchRecordCount = ResultFiltered.Count();

            return this.DataTablesJsonGenericSearch(
                items: ResultOrdered, 
                totalRecords: totalRecordCount,
                totalDisplayRecords: searchRecordCount,
                sEcho: jQueryDataTablesModel.sEcho,
                SearchCriteria:SearchCriteria);
        }


        
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
			searchCriteria.GetSearchCriteria("Id").SetLabel("Id");
			searchCriteria.GetSearchCriteria("Nome").SetLabel("Nome");
			//searchCriteria.GetSearchCriteria("Status").SetLabel("Status");
            searchCriteria.Remove("DataModificacao");
            searchCriteria.Remove("Excluido");
            searchCriteria.Remove("Id");


            return searchCriteria.Order();
        }
        
		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
