// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using AutoMapper;
using System.Data.Entity.Repository.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using static GF.Data.Model.ConstantesModel;

namespace GF.Controllers {

    //[Authorize(Roles = "Admin")]
    [Authorize]
    public partial class PlanoController : Controller
    {
		const string GridInfoKey = "Plano_Servico"+"GridData";
        private UnitOfWork db = new UnitOfWork();
        
        ILog log = LogManager.GetLogger<Plano_Servico>();
        
        [Authorize(Roles = "Administrador")]
		[HttpGet]
        public  ActionResult NoFilter()
        {
            Session[GridInfoKey]  = null;
            return RedirectToAction("Index");
        }

        private void InicializarCombos()
        {
            ViewBag._Periodo = new SelectList(db.PeriodoRepository.GetAllPeriodos().ToList(), "ID", "Descricao");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()	
        {
            try
            {
                ICollection<AbstractSearch> SearchCriteria1;
                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                if (GridState != null && GridState.GetSearchCriteria() != null)
                {
                    SearchCriteria1 = GridState.GetSearchCriteria();
                }
                else
                {
                    SearchCriteria1 = CustomizeSearch(typeof(Plano_Servico).GetDefaultSearchCriterias());
                }

                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = SearchCriteria1
                };
                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);                
            }

            return View();

        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult  Index(ICollection<AbstractSearch>searchCriteria )	
        {
            try
            {
                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = CustomizeSearch(searchCriteria)
                };
                GridViewModel GridConfig = new GridViewModel(searchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            return View();

        }


        // GET: /Plano_Servico/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                InicializarCombos();
               return View();
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            
            return View();
        }

        // POST: /Plano_Servico/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Plano_Servico plano)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    //valida se o registro existe com o mesmo nome escrito
                    InicializarCombos();
                    Plano_Servico validaPlano_Servico;
                    validaPlano_Servico = db.Plano_ServicoRepository.GetAllPlano_Servicos(y => y.Periodo).Where(y => y.Nome == plano.Nome && y.Periodo_ID == plano.Periodo_ID).FirstOrDefault();
                    if (validaPlano_Servico != null)
                    {
                        @TempData["popup-alert"] = Mensagens.DUPLICATE;
                        return View();
                    }
                    
                    //insere o novo registro
                    db.Plano_ServicoRepository.AddPlano_Servico(plano);
                    
                    @TempData["popup-sucess"] = Mensagens.ADD_SUCESS;
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    log = LogManager.GetLogger<Plano_Servico>();
                    log.Error(e);
                    ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                }
                
            }

            return View(plano);
        }

        
        // GET: /Plano_Servico/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //para lembrar
                Plano_Servico plano = db.Plano_ServicoRepository.FindPlano_Servico(x => x.ID == id).FirstOrDefault();
                
                if (plano == null)
                {
                    return HttpNotFound();
                }
                InicializarCombos();
                return View(plano);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }
        
        // POST: /Plano_Servico/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Plano_Servico plano)
        {
            try
            {
                InicializarCombos();
                if (ModelState.IsValid)
                {
                    Plano_Servico validaPlano_Servico = db.Plano_ServicoRepository.FindPlano_Servico(x => x.Nome == plano.Nome).FirstOrDefault();
                    if (validaPlano_Servico != null)
                    {
                        if (validaPlano_Servico.ID != plano.ID)
                        {
                            //@TempData["popup-alert"] = "Registro j� cadastrado.";
                            ModelState.AddModelError("Nome", "Nome j� cadastrado na base de dados.");
                            throw new GFException("");
                        }
                    }

                    validaPlano_Servico = db.Plano_ServicoRepository.FindPlano_Servico(x => x.ID == plano.ID).FirstOrDefault();
                    plano.CopyProperties(validaPlano_Servico);

                    db.Plano_ServicoRepository.UpdatePlano_Servico(validaPlano_Servico);
                    

                    @TempData["popup-sucess"] = Mensagens.UPDATE_SUCESS;
                    return RedirectToAction("Index");

                }
            }
            catch (GFException)
            {

            }                
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

            }

            return View(plano);
        }

        // GET: /Plano_Servico/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id)
        {
            try
            {
                InicializarCombos();
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                //IQueryable<Funcionario> Result = db.FuncionarioRepository.GetAllFuncioanrios(y => y.Pessoa);
                Plano_Servico plano = db.Plano_ServicoRepository.GetAllPlano_Servicos(y => y.Periodo).Where(x => x.ID == id).FirstOrDefault();
                
                if (plano == null)
                {
                    return HttpNotFound();
                }
                return View(plano);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // POST: /Plano_Servico/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                InicializarCombos();
                Plano_Servico plano;
                plano = db.Plano_ServicoRepository.FindPlano_Servico(x => x.ID == id).FirstOrDefault();
                
                db.Plano_ServicoRepository.DeletePlano_Servico(plano);
                
                @TempData["popup-sucess"] = Mensagens.DELETE_SUCESS;
                return RedirectToAction("Index");
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex1)
            {
                //Console.Write(ex1.Message);
                {
                    var sqlex = ex1.InnerException.InnerException as SqlException;

                    if (sqlex != null)
                    {
                        switch (sqlex.Number)
                        {
                            case 547:
                                TempData["popup-alert"] = "Esse registro possui relaciomento e n�o pode ser apagado!";
                                break; //FK exception
                            case 2627:
                            case 2601:
                                TempData["popup-alert"] = "Registro Duplicado!";
                                break; //primary key exception

                            default: throw sqlex; //otra excepcion que no controlo.


                        }
                    }

                    //throw ex1;
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                
                return View();
            }
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;
            try
            {
                IQueryable<Plano_Servico> Result = db.Plano_ServicoRepository.GetAllPlano_Servicos(x => x.Periodo);

                if (jQueryDataTablesModel.iSortingCols>0 && jQueryDataTablesModel.iSortCol_ !=null)
                {
               
                    SortBy    = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_ [0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState= (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                IQueryable <Plano_Servico>ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<Plano_Servico> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<Plano_Servico>)(ResultFiltered.sOrder(SortBy, SortOrder=="asc").Skip(page).Take(limit)));

                int totalRecordCount=Result.Count();
                int searchRecordCount = ResultFiltered.Count();

                var A = ResultOrdered.Select(x => new
                {
                    ID = x.ID,
                    Periodo = new { Descricao = x.Periodo.Descricao, Duracao = x.Periodo.Duracao },
                    Nome = x.Nome + " - " + x.Periodo.Descricao,
                    Valor = x.Valor,
                    Descricao = x.Descricao,
                    Duracao = x.Duracao,
                    DataModificacao = x.DataModificacao,
                    Excluido = x.Excluido,
                });

                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Plano_Servico>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }

            
        }


        //Esta funcao deve ser movida paro o outro arquivo da classe parcial
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
            
            ((NumericSearch)searchCriteria.GetSearchCriteria("Periodo_ID"))
                .Setitems(new SelectList(db.PeriodoRepository.GetAllPeriodos().ToList(), "ID", "Descricao"))
                .SetLabel("Per�odo");

            searchCriteria.Remove("Valor");
            
            searchCriteria.Remove("DataModificacao");
            searchCriteria.Remove("Excluido");
            searchCriteria.Remove("ID");
            


            return searchCriteria.Order();
        }

        
        [Authorize(Roles = "Administrador")]
        public bool Validar(Plano_Servico Plano_Servico, ModelStateDictionary ModelState )
		{
			
			return ModelState.Count() == 0;
		}
		
		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
