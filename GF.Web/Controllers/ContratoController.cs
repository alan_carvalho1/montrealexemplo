// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using AutoMapper;
using System.Data.Entity.Repository.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using static GF.Data.Model.ConstantesModel;
using GF.Web.Constantes;
using System.IO;

namespace GF.Controllers {

    //[Authorize(Roles = "Admin")]
    [Authorize]
    public partial class ContratoController : Controller
    {
		const string GridInfoKey = "Contrato"+"GridData";
        private UnitOfWork db = new UnitOfWork();
        
        ILog log = LogManager.GetLogger<Contrato>();
        
        [Authorize(Roles = "Administrador")]
		[HttpGet]
        public  ActionResult NoFilter()
        {
            Session[GridInfoKey]  = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()	
        {
            try
            {
                ICollection<AbstractSearch> SearchCriteria1;
                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                
                    SearchCriteria1 = CustomizeSearch(typeof(Contrato).GetDefaultSearchCriterias());
                

                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = SearchCriteria1
                };
                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);                
            }

            return View();

        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult  Index(ICollection<AbstractSearch>searchCriteria )	
        {
            try
            {
                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = CustomizeSearch(searchCriteria)
                };
                GridViewModel GridConfig = new GridViewModel(searchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            return View();

        }


        // GET: /Contrato/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                InicializarCombos(0);
                return View();
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            
            return View();
        }

        private void InicializarCombos(int id)
        {
            var contratos1 = db.ContratoRepository.GetAllContratos().Where(x => x.Excluido == ConstantesModel.Excluido.NAO).Select(x => x.Aluno_ID);

            var alunos = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(k => !contratos1.Contains(k.ID) && k.Excluido != ConstantesModel.Excluido.INATIVO)
                .Union(
                    db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(k => k.ID == id && k.Excluido != ConstantesModel.Excluido.INATIVO)
                );


            
            var A = alunos.Select(x => new
            {
                ID = x.ID,
                Nome = x.Pessoa.Nome
            });
            ViewBag._Alunos = new SelectList(A, "ID", "Nome");

            var planos = db.Plano_ServicoRepository.GetAllPlano_Servicos(x => x.Periodo).OrderBy(a => a.Nome);
            var L = planos.Select(x => new
            {
                ID = x.ID,
                Nome = x.Nome + " - " + x.Periodo.Descricao
            });
            ViewBag._Planos = new SelectList(L.ToList(), "ID", "Nome");

        }

        // POST: /Contrato/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contrato contrato)
        {

            if (ModelState.IsValid)
            {
                try
                {
                    
                    //insere o novo registro
                    db.ContratoRepository.AddContrato(contrato);
                    
                    @TempData["popup-sucess"] = Mensagens.ADD_SUCESS;
                    return RedirectToAction("Edit", new { id = contrato.ID });
                }
                catch (Exception e)
                {
                    log = LogManager.GetLogger<Contrato>();
                    log.Error(e);
                    ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                }
                
            }
            InicializarCombos(0);
            return View(contrato);
        }

        
        // GET: /Contrato/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //para lembrar
                Contrato contrato = db.ContratoRepository.FindContrato(x => x.ID == id, y => y.Aluno).FirstOrDefault();
                if (contrato.Excluido == ConstantesModel.Excluido.CONTRATO_ENCERRADO)
                {
                    @TempData["popup-alert"] = "Contrato Encerrado. N�o � poss�vel realizar altera��es";
                }
                InicializarCombos(contrato.Aluno_ID ?? 0);
                if (contrato == null)
                {
                    return HttpNotFound();
                }
                
                return View(contrato);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // GET: /Contrato/Edit/5
        [Authorize(Roles = "Administrador")]
        public FileResult GerarContrato(int id)
        {
            try
            {
                Contrato contrato = db.ContratoRepository.FindContrato(x => x.ID == id, y => y.Aluno, y => y.Aluno.Pessoa, y => y.PlanoServico.Periodo).FirstOrDefault();
                if (contrato.Excluido == ConstantesModel.Excluido.CONTRATO_ENCERRADO)
                {
                    @TempData["popup-alert"] = "Contrato Encerrado. N�o � poss�vel realizar altera��es";
                }
                InicializarCombos(contrato.Aluno_ID ?? 0);

                string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                //
                string path2 = path.Remove(path.LastIndexOf("\\"));
                path = path2.Remove(path2.LastIndexOf("\\") + 1);
                path += "academiafoco\\Contratos";

                System.IO.Directory.CreateDirectory(path);


                

                var document = new PdfSharp.Pdf.PdfDocument();
                var page = document.AddPage();
                var graphics = PdfSharp.Drawing.XGraphics.FromPdfPage(page);
                var textFormatter = new PdfSharp.Drawing.Layout.XTextFormatter(graphics);
                var font = new PdfSharp.Drawing.XFont("Calibri", 8);
                var titulo = new PdfSharp.Drawing.XFont("Calibri", 9, PdfSharp.Drawing.XFontStyle.Bold | PdfSharp.Drawing.XFontStyle.Underline);
                var negrito = new PdfSharp.Drawing.XFont("Calibri", 8, PdfSharp.Drawing.XFontStyle.Bold);
                var rodape = new PdfSharp.Drawing.XFont("Calibri", 5);
                // Textos.

                string nome = contrato.Aluno.Pessoa.Nome;
                nome += contrato.Aluno.Pessoa.Rua != null ? ", " + contrato.Aluno.Pessoa.Rua : "";
                nome += contrato.Aluno.Pessoa.Numero != null ? ", " + contrato.Aluno.Pessoa.Numero : "";
                nome += contrato.Aluno.Pessoa.Cidade != null ? ", " + contrato.Aluno.Pessoa.Cidade : "";
                nome += contrato.Aluno.Pessoa.Estado != null ? " - " + contrato.Aluno.Pessoa.Estado : "";
                nome += contrato.Aluno.Pessoa.CEP != null ? " (" + contrato.Aluno.Pessoa.CEP + ")" : "";


                

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Center;
                textFormatter.DrawString("CONTRATO DE PRESTA��O DE SERVI�OS DE ACADEMIA", titulo, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 30, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("CONTRATANTE: ",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 45, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString(nome,
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 55, page.Width - 60, page.Height - 60));


                
                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("CONTRATADA: FOCO ATIVIDADE E CONDICIONAMENTO F�SICO LTDA.",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 85, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Empresa detentora do CNPJ n�: 26.451.593 / 0001 - 04, com endere�o situado na Rua Mios�tis, 1115, J�quei, cidade de Teresina / PI, CEP: 64.000 - 000",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 95, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("As partes acima identificadas t�m, entre si, justo e acertado o presente Contrato de Presta��o de Servi�os de Academia de Gin�stica, que se reger� pelas cl�usulas seguintes e pelas condi��es descritas no presente.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 115, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("DO OBJETO DO CONTRATO",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 135, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 1�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 145, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Este contrato tem como OBJETO o uso de academia, de propriedade da CONTRATADA, pelo CONTRATANTE para desenvolver a pr�tica de atividades f�sicas.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 155, page.Width - 60, page.Height - 60));



                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("DO FUNCIONAMENTO",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 185, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 2�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 195, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("A academia de gin�stica funcionar� de segunda a sexta - feira das 06:00h �s 12:00h, retornando das 15:00h �s 21:00h e aos s�bados das 09:00h �s 12:00h, e das 15:00h �s 18:00h.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 205, page.Width - 60, page.Height - 60));


                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 3�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 235, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Estes hor�rios poder�o sofrer altera��es, de acordo com as necessidades da CONTRATADA.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 245, page.Width - 60, page.Height - 60));


                /****************************************************************/
                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("DA MATR�CULA E DO PAGAMENTO DAS MENSALIDADES",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 275, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 4�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 285, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("O pagamento das matr�culas, avalia��es f�sicas e mensalidades, dever� ser feito na secretaria da CONTRATADA.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 295, page.Width - 60, page.Height - 60));


                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 5�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 315, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Quando o CONTRATANTE for menor de idade, o respons�vel dever� assinar o contrato.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 325, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 6�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 345, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("O CONTRATANTE escolher� a melhor forma de pagamento dentre as disponibilizadas pela CONTRATADA, devendo efetuar os pagamentos peri�dicos mensais de acordo com seu plano, salvo em caso de pagamentos mediante cart�o de cr�dito ou cheques pr� - datados, que ser�o descontados automaticamente.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 355, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 7�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 385, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Em caso de desist�ncia dos planos dever� ser observado o seguinte:",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 395, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("- Nos planos mensais � N�O SER� DEVOLVIDO NENHUM VALOR.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 405, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("- Nos planos trimestrais, semestrais ou anuais � em caso de desist�ncia dos aludidos planos o CONTRANTANTE dever� pagar pela diferen�a dos planos, tendo em vista que quanto maior o plano, maior o desconto no valor da presta��o mensal.Deve ainda ser deduzido dentro do valor a ser restitu�do ao CONTRATANTE, os tributos j� pagos pela CONTRATADA.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 415, page.Width - 60, page.Height - 60));


                /**************/
                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("DO VALOR DA MENSALIDADES",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 455, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 8�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 465, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("O CONTRATANTE pagar� o valor de R$ " + contrato.ValorMensalidade + " , no dia " + contrato.DiaBasePagamento + " no plano " + contrato.PlanoServico.Periodo.Descricao + ".",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 475, page.Width - 60, page.Height - 60));


                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 9�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 495, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Este valor de mensalidade poder� tamb�m ser alterado em fun��o de:",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 505, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("a) Reajustes financeiros anuais.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 515, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("b) Retirada de conv�nio.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 525, page.Width - 60, page.Height - 60));

                /****************/
                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("FREQU�NCIA �S ATIVIDADES",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 545, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 10�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 555, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Somente poder� frequentar as atividades nas instala��es da CONTRATADA, o CONTRATANTE que estiver regularmente matriculado, com todos os exames obrigat�rios realizados e sem nenhum d�bito de mensalidade",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 565, page.Width - 60, page.Height - 60));


                /***********************/
                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("DA RESCIS�O",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 595, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 11�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 605, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Poder� ser rescindido automaticamente, por qualquer das partes, desde que a parte interessada comunique � outra, sendo que o CONTRATANTE somente poder� rescindir este contrato se estiver em dia com o pagamento das mensalidades ou outros d�bitos existentes para com a CONTRATADA.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 615, page.Width - 60, page.Height - 60));

                /******************************/

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("DO FORO",
                    negrito, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 645, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Cl�usula 12�.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 655, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Para dirimir quaisquer controv�rsias oriundas do CONTRATO, as partes elegem o foro da comarca de Teresina / PI",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 665, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Justify;
                textFormatter.DrawString("Por estarem assim justos e contratados, firmam o presente instrumento, em duas vias de igual teor.",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 675, page.Width - 60, page.Height - 60));


                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Left;
                textFormatter.DrawString("Teresina/PI, " + DateTime.Now.Day + " de " + Constantes.DicionarioMes.NomeMes[DateTime.Now.Month] + " de " + DateTime.Now.Year, 
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 745, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Center;
                textFormatter.DrawString("________________________________________",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 745, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Right;
                textFormatter.DrawString("________________________________________",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 745, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Center;
                textFormatter.DrawString("Contratada",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 755, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Center;
                textFormatter.DrawString("Contratante",
                    font, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(220, 755, page.Width - 60, page.Height - 60));

                textFormatter.Alignment = PdfSharp.Drawing.Layout.XParagraphAlignment.Right;
                textFormatter.DrawString("Gerado em: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"),
                    rodape, PdfSharp.Drawing.XBrushes.Black, new PdfSharp.Drawing.XRect(30, 785, page.Width - 60, page.Height - 60));

                
                Random random = new Random();
                int num = random.Next(0, 1000);
                string nomeArquivo = contrato.Aluno.Pessoa.CPF + "-" + contrato.ID + ".pdf";
                string pathFinal = (path + "\\" + nomeArquivo);

                document.Save(pathFinal);


                string root = Server.MapPath("~");
                string parent = Path.GetDirectoryName(root);
                string grandParent = Path.GetDirectoryName(parent);

                
                log = LogManager.GetLogger<Contrato>();
                
                return File(root + "\\Contratos\\" + nomeArquivo, "application /pdf");
                
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return null;
            }

        }

        // POST: /Contrato/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Contrato contrato)
        {
            try
            {
                Contrato validaContrato = db.ContratoRepository.FindContrato(x => x.ID == contrato.ID, y => y.Aluno).FirstOrDefault();
                contrato.DataInicio = validaContrato.DataInicio;
                ModelState.Remove("DataInicio");
                if (ModelState.IsValid)
                {
                    
                    
                    contrato.CopyProperties(validaContrato);

                    db.ContratoRepository.UpdateContrato(validaContrato);
                    

                    @TempData["popup-sucess"] = Mensagens.UPDATE_SUCESS;
                    return RedirectToAction("Index");

                }
            }
            catch (GFException)
            {

            }                
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

            }

            return View(contrato);
        }

        // GET: /Contrato/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult EncerrarContrato(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                
                Contrato contrato = db.ContratoRepository.FindContrato(x => x.ID == id, y => y.Aluno).FirstOrDefault();
                contrato.Encerramento = DateTime.Now;

                if (contrato.Excluido == ConstantesModel.Excluido.CONTRATO_ENCERRADO)
                {
                    @TempData["popup-alert"] = "Contrato Encerrado. N�o � poss�vel realizar altera��es";
                }

                InicializarCombos(contrato.Aluno_ID ?? 0);
                if (contrato == null)
                {
                    return HttpNotFound();
                }

                return View(contrato);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }

        }

        // POST: /Contrato/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EncerrarContrato(Contrato contrato)
        {
            try
            {
                Validar(contrato, ModelState);
                if (ModelState.IsValid)
                {

                    Contrato inserirContrato = db.ContratoRepository.FindContrato(x => x.ID == contrato.ID, y => y.Aluno, y => y.Aluno.Pessoa).FirstOrDefault();
                    //contrato.CopyProperties(validaContrato);
                    inserirContrato.Multa = contrato.Multa;
                    inserirContrato.Encerramento = contrato.Encerramento;

                    inserirContrato.Excluido = ConstantesModel.Excluido.CONTRATO_ENCERRADO;
                    db.ContratoRepository.UpdateContrato(inserirContrato);

                    
                    @TempData["popup-sucess"] = Mensagens.CONTRATO_ENCERRADO;
                    return RedirectToAction("Index");

                }
                InicializarCombos(contrato.Aluno_ID ?? 0);
            }
            catch (GFException)
            {

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

            }

            return View(contrato);
        }

        // GET: /Contrato/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Contrato contrato = db.ContratoRepository.FindContrato(x => x.ID == id, y => y.Aluno, y => y.PlanoServico, y => y.Aluno.Pessoa).FirstOrDefault();
                
                if (contrato == null)
                {
                    return HttpNotFound();
                }
                return View(contrato);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // POST: /Contrato/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                Contrato validaContrato = db.ContratoRepository.FindContrato(x => x.ID == id, y => y.Aluno).FirstOrDefault();
                
                
                db.ContratoRepository.DeleteContrato(validaContrato);
                

                @TempData["popup-sucess"] = Mensagens.DELETE_SUCESS;
                return RedirectToAction("Index");
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex1)
            {
                //Console.Write(ex1.Message);
                {
                    var sqlex = ex1.InnerException.InnerException as SqlException;

                    if (sqlex != null)
                    {
                        switch (sqlex.Number)
                        {
                            case 547:
                                TempData["popup-alert"] = "Esse registro possui relaciomento e n�o pode ser apagado!";
                                break; //FK exception
                            case 2627:
                            case 2601:
                                TempData["popup-alert"] = "Registro Duplicado!";
                                break; //primary key exception

                            default: throw sqlex; //otra excepcion que no controlo.


                        }
                    }

                    //throw ex1;
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                
                return View();
            }
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;
            try
            {
                IQueryable<Contrato> Result = db.ContratoRepository.GetAllContratos(y => y.Aluno, y => y.Aluno.Pessoa, y => y.PlanoServico.Periodo);

                if (jQueryDataTablesModel.iSortingCols>0 && jQueryDataTablesModel.iSortCol_ !=null)
                {
               
                    SortBy    = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_ [0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState= (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                IQueryable <Contrato>ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<Contrato> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<Contrato>)(ResultFiltered.sOrder(SortBy, SortOrder=="asc").Skip(page).Take(limit)));

                int totalRecordCount=Result.Count();
                int searchRecordCount = ResultFiltered.Count();

                var A = ResultOrdered.Select(x => new
                {
                    ID = x.ID,
                    DiaBasePagamento = x.DiaBasePagamento,
                    DataInicio = x.DataInicio,
                    Multa = x.Multa,
                    Encerramento = x.Encerramento,
                    Aluno = new { Pessoa = new { Nome = x.Aluno.Pessoa.Nome }, Matricula = x.Aluno.Matricula },
                    PlanoServico = new { Nome = x.PlanoServico.Nome + " - " + x.PlanoServico.Periodo.Descricao + " - R$" + x.ValorMensalidade},
                    DataModificacao = x.DataModificacao,
                    ValorMensalidade = x.ValorMensalidade,
                    Excluido = x.Excluido,
                });

                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Contrato>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }

            
        }


        
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {

            var planos = db.Plano_ServicoRepository.GetAllPlano_Servicos(x => x.Periodo).OrderBy(a => a.Nome);
            var L = planos.Select(x => new
            {
                ID = x.ID,
                Nome = x.Nome + " - " + x.Periodo.Descricao
            });
            ((NumericSearch)searchCriteria.GetSearchCriteria("PlanoServico_ID"))
                .Setitems(new SelectList(L
                .ToList(), "ID", "Nome"))
                .SetLabel("Plano")
                .SetOrder(0);
          

            searchCriteria.Remove("Ano");
        
            searchCriteria.Remove("Aluno_ID");
            searchCriteria.Remove("Multa");
            searchCriteria.Remove("ValorMensalidade");

            searchCriteria.Remove("DataModificacao");
            searchCriteria.Remove("Excluido");
            searchCriteria.Remove("ID");
            


            return searchCriteria.Order();
        }

        
        [Authorize(Roles = "Administrador")]
        public bool Validar(Contrato Contrato, ModelStateDictionary ModelState )
		{
            if (Contrato.Multa == null)
            {
                ModelState.AddModelError("Multa", "O campo Multa deve ser preenchido para encerrar o contrato.");
                
            }

            if (Contrato.Encerramento == null)
            {
                ModelState.AddModelError("Encerramento", "O campo Data de Encerramento deve ser preenchido para encerrar o contrato.");
            }
			//ModelState.AddModelError("","O camplo XYZ bla bla bla");
			//ModelState.AddModelError("Name","O campo Name bla bla bla");
			return ModelState.Count() == 0;
		}

        public string BuscaValorPlano(string IdPlano)
        {
            int valor = int.Parse(IdPlano);
            var detalhes = db.Plano_ServicoRepository.FindPlano_ServicoByPlano_ServicoId(valor);
            var result = detalhes.Valor.ToString().Replace(".",",");

            return result;
        }

        [HttpGet]
        public ActionResult BuscaSugestaoValor(string categoria)
        {
            var abc = new JsonResult()
            {
                Data = BuscaValorPlano(categoria),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
            return abc;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
