
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using AutoMapper;
using System.Data.Entity.Repository.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using static GF.Data.Model.ConstantesModel;

namespace GF.Controllers {

    
    [Authorize]
    public partial class AlunoController : Controller
    {
		const string GridInfoKey = "Aluno"+"GridData";
        private UnitOfWork db = new UnitOfWork();
        
        ILog log = LogManager.GetLogger<Aluno>();
        
        [Authorize(Roles = "Administrador")]
		[HttpGet]
        public  ActionResult NoFilter()
        {
            Session[GridInfoKey]  = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()	
        {
            try
            {
                ICollection<AbstractSearch> SearchCriteria1;
                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                if (GridState != null && GridState.GetSearchCriteria() != null)
                {
                    SearchCriteria1 = GridState.GetSearchCriteria();
                }
                else
                {
                    SearchCriteria1 = CustomizeSearch(typeof(Aluno).GetDefaultSearchCriterias());
                }

                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = SearchCriteria1
                };
                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);                
            }

            return View();

        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult  Index(ICollection<AbstractSearch>searchCriteria )	
        {
            try
            {
                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = CustomizeSearch(searchCriteria)
                };
                GridViewModel GridConfig = new GridViewModel(searchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            return View();

        }


        // GET: /Aluno/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
               return View();
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            
            return View();
        }

        // POST: /Aluno/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Aluno aluno)
        {

            try
            {
                Aluno validaAluno;
                aluno.Pessoa.CPF = aluno.Pessoa.CPF.Replace(".", "").Replace("-", "");
                validaAluno = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(y => y.Pessoa.CPF == aluno.Pessoa.CPF).FirstOrDefault();
                if (validaAluno != null)
                {
                    @TempData["popup-alert"] = Mensagens.DUPLICATE;
                    return View();
                }

                validaAluno = db.AlunoRepository.GerarMatricula(aluno);

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

            }

            if (ModelState.IsValid)
            {
                try
                {
                    


                    aluno.Pessoa.CPF = aluno.Pessoa.CPF.Replace(".", "").Replace("-", "");
                    //insere o novo registro
                    aluno.Pessoa.DataModificacao = DateTime.Now;
                    aluno.Pessoa.Excluido = "N";
                    db.AlunoRepository.AddAluno(aluno);
                    db.SaveChanges();
                    db.AlunoRepository.CommitTransaction();
                    
                    @TempData["popup-sucess"] = Mensagens.ADD_SUCESS;
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    log = LogManager.GetLogger<Aluno>();
                    log.Error(e);
                    ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                }
                
            }

            return View(aluno);
        }

        


        // GET: /Aluno/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //para lembrar
                ///IQueryable<Aluno> aluno = db.AlunoRepository.FindAluno(x => x.ID == id && x.Login == "alan", y => y.Pessoa, y => y.Pessoa);
                IQueryable<Aluno> listAluno = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(x => x.ID == id);
                Aluno aluno = listAluno.FirstOrDefault();

                if (aluno == null)
                {
                    return HttpNotFound();
                }
                
                return View(aluno);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }
        
        // POST: /Aluno/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Aluno aluno)
        {
            try
            {
                Validar(aluno, ModelState);
               

                if (ModelState.IsValid)
                {
                    //db.AlunoRepository.GetAllFuncioanrios(y => y.Pessoa);
                    aluno.Pessoa.CPF = aluno.Pessoa.CPF.Replace(".", "").Replace("-", "");
                    Aluno validaAluno = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(u => u.Pessoa.CPF == aluno.Pessoa.CPF).FirstOrDefault();
                    if (validaAluno != null)
                    {
                        if (validaAluno.ID != aluno.ID)
                        {
                            //@TempData["popup-alert"] = "Registro j� cadastrado.";
                            ModelState.AddModelError("CPF", "CPF j� cadastrado na base de dados.");
                            throw new GFException("");
                        }
                    }

                    validaAluno = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(u => u.ID == aluno.ID).FirstOrDefault();

                    aluno.Pessoa.CopyProperties(validaAluno.Pessoa);
                    aluno.CopyProperties(validaAluno);

                    validaAluno.Pessoa.Excluido = "N";
                    validaAluno.Pessoa.DataModificacao = DateTime.Now;

                    db.AlunoRepository.UpdateAluno(validaAluno);
                    
                    
                   
                    db.SaveChanges();
                    db.AlunoRepository.CommitTransaction();
                    @TempData["popup-sucess"] = Mensagens.UPDATE_SUCESS;
                    return RedirectToAction("Index");

                }
            }
            catch (GFException)
            {

            }                
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

            }

            return View(aluno);
        }

        // GET: /Aluno/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Aluno aluno = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(x => x.ID == id).FirstOrDefault();
                if (aluno == null)
                {
                    return HttpNotFound();
                }
                return View(aluno);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // POST: /Aluno/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { 
                Aluno aluno;
                aluno = db.AlunoRepository.GetAllAlunos(y => y.Pessoa).Where(x => x.ID == id).FirstOrDefault();

                aluno.Pessoa.Excluido = "S";
                aluno.Pessoa.DataModificacao = DateTime.Now;

                db.AlunoRepository.DeleteAluno(aluno);
                db.SaveChanges();
                db.AlunoRepository.CommitTransaction();
                @TempData["popup-sucess"] = Mensagens.DELETE_SUCESS;
                return RedirectToAction("Index");
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex1)
            {
                //Console.Write(ex1.Message);
                {
                    var sqlex = ex1.InnerException.InnerException as SqlException;

                    if (sqlex != null)
                    {
                        switch (sqlex.Number)
                        {
                            case 547:
                                TempData["popup-alert"] = "Esse registro possui relaciomento e n�o pode ser apagado!";
                                break; //FK exception
                            case 2627:
                            case 2601:
                                TempData["popup-alert"] = "Registro Duplicado!";
                                break; //primary key exception

                            default: throw sqlex; //otra excepcion que no controlo.


                        }
                    }

                    //throw ex1;
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                
                return View();
            }
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;
            try
            {
                IQueryable<Aluno> Result = db.AlunoRepository.GetAllAlunos(y => y.Pessoa);

                if (jQueryDataTablesModel.iSortingCols>0 && jQueryDataTablesModel.iSortCol_ !=null)
                {
               
                    SortBy    = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_ [0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState= (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                IQueryable <Aluno>ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<Aluno> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<Aluno>)(ResultFiltered.sOrder(SortBy, SortOrder=="asc").Skip(page).Take(limit)));

                int totalRecordCount=Result.Count();
                int searchRecordCount = ResultFiltered.Count();

                var A = ResultOrdered.Select(x => new
                {
                    ID = x.ID,
                    Matricula = x.Matricula,
                    Email = x.Email,
                    Excluido = x.Excluido,
                    Pessoa = new { Nome = x.Pessoa.Nome,                    
                        DataNascimento = x.Pessoa.DataNascimento,
                        Telefone_Fixo = x.Pessoa.Telefone_Fixo,
                        CPF = x.Pessoa.CPF
                    },
                });

                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Aluno>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }

            
        }


        
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
            
            searchCriteria.Remove("Sexo");
            searchCriteria.Remove("Profissao");
            searchCriteria.Remove("LocalTreino");
            searchCriteria.Remove("EstadoCivil");
            searchCriteria.Remove("DataModificacao");
            searchCriteria.Remove("Excluido");
            searchCriteria.Remove("ID");
            searchCriteria.Remove("Pessoa_ID");


            return searchCriteria.Order();
        }

        
        [Authorize(Roles = "Administrador")]
        public bool Validar(Aluno aluno, ModelStateDictionary ModelState )
		{
            if (aluno.Excluido == ConstantesModel.Excluido.INATIVO)
            {
                var contratoAluno = db.ContratoRepository.FindContrato(y => y.Aluno_ID == aluno.ID).Where(x => x.Excluido == ConstantesModel.Excluido.NAO).FirstOrDefault();
                if (contratoAluno != null)
                {
                    ModelState.AddModelError("", "Este aluno n�o pode ser inativado porque possui contrato ativo.");
                }
            }

            //ModelState.AddModelError("","O camplo XYZ bla bla bla");
            //ModelState.AddModelError("Name","O campo Name bla bla bla");
            return ModelState.Count() == 0;
		}
		
		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
