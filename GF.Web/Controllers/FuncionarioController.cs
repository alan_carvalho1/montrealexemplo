// TargetFrameworkVersion = 4.51
#pragma warning disable 1591//  Ignore "Missing XML Comment" warning


using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Repository.Attributes;

namespace GF.Controllers {

    
    [Authorize]
    public partial class FuncionarioController : Controller
    {
		const string GridInfoKey = "Funcionario"+"GridData";
        private UnitOfWork db = new UnitOfWork();
        private GFRoleProvider Provider;
        ILog log = LogManager.GetLogger<Funcionario>();
        


        public FuncionarioController()
        {
            Provider = new GFRoleProvider(db);
        }

        [Authorize(Roles = "Administrador")]
		[HttpGet]
        public  ActionResult NoFilter()
        {
            Session[GridInfoKey]  = null;
            return RedirectToAction("Index");
        }

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()	
        {
            try
            {
                ICollection<AbstractSearch> SearchCriteria1;
                GridViewModel GridState = (GridViewModel)Session[GridInfoKey];
                if (GridState != null && GridState.GetSearchCriteria() != null)
                {
                    SearchCriteria1 = GridState.GetSearchCriteria();
                }
                else
                {
                    SearchCriteria1 = CustomizeSearch(typeof(Funcionario).GetDefaultSearchCriterias());
                }

                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = SearchCriteria1
                };
                GridViewModel GridConfig = new GridViewModel(model.SearchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);                
            }

            return View();

        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public ActionResult  Index(ICollection<AbstractSearch>searchCriteria )	
        {
            try
            {
                var model = new SearchViewModel()
                {
                    Data = null,
                    SearchCriteria = CustomizeSearch(searchCriteria)
                };
                GridViewModel GridConfig = new GridViewModel(searchCriteria);
                Session[GridInfoKey] = GridConfig;
                return View(model);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            return View();

        }


        // GET: /Funcionario/Create
        [Authorize(Roles = "Administrador")]
        public ActionResult Create()
        {
            try
            {
                
                string[] userRoles = new string[] { "" };
                
                var a = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });
                ViewBag.RoleId = a;

                
                

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
            }
            
            return View();
        }

        // POST: /Funcionario/Create
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Funcionario funcionario,params string[] selectedRoles)
        {
            string[] userRoles = new string[] { "" };
            IEnumerable<SelectListItem> a;
            
            if (ModelState.IsValid)
            {
                try
                {
                    funcionario.Pessoa.CPF = funcionario.Pessoa.CPF.Replace(".", "").Replace("-", "");
                    Funcionario validaMotorista = db.FuncionarioRepository.GetAllFuncionarios().Where(u => u.Login == funcionario.Login).FirstOrDefault();
                    if (validaMotorista != null)
                    {
                        //@TempData["popup-alert"] = "Registro j� cadastrado.";
                        ModelState.AddModelError("Login", "Login j� cadastrado na base de dados.");
                        userRoles = new string[] { "" };
                        a = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });
                        ViewBag.RoleId = a;
                        return View();
                    }

                    validaMotorista = db.FuncionarioRepository.GetAllFuncionarios().Where(u => u.Email == funcionario.Email).FirstOrDefault();
                    if (validaMotorista != null)
                    {
                        //@TempData["popup-alert"] = "Registro j� cadastrado.";
                        ModelState.AddModelError("Email", "E-mail j� cadastrado na base de dados.");
                        userRoles = new string[] { "" };
                        a = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });
                        ViewBag.RoleId = a;
                        return View();
                    }

                    funcionario.Senha = HashValue(funcionario.Senha);
                
                    
                    funcionario.Pessoa.Excluido = ConstantesModel.Excluido.NAO;
                    funcionario.Pessoa.DataModificacao = DateTime.Now;
                    
                    db.FuncionarioRepository.AddFuncionario(funcionario);
                
                    if (selectedRoles != null)
                    {
                        
                        Provider.AddUsersToRoles(funcionario.Login.Split('\n'),selectedRoles);
                    }
                    db.SaveChanges();
                    db.FuncionarioRepository.CommitTransaction();
                    @TempData["popup-sucess"] = ConstantesModel.Mensagens.ADD_SUCESS;
                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    log = LogManager.GetLogger<Funcionario>();
                    log.Error(e);
                    ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                }
                
            }

            //ViewBag.RoleId = new SelectList(Provider.GetAllRoles().ToList()).OrderBy(p => p.Text);
            userRoles = new string[] { "" };
            a = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });
            ViewBag.RoleId = a;

            return View(funcionario);
        }

        // GET: /Funcionario/Create]
        [AllowAnonymous]
        public ActionResult Login()
        {

            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            //authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie != null)
                return RedirectToAction("AcessoNegado");

            return View("Login", "_LayoutLogin");
        }

        // POST: /Funcionario/Create
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Funcionario funcionario)
        {
            try
            {
                //verifica se existe login
                Funcionario usrLogando = db.FuncionarioRepository.GetAllFuncionarios().Where(l => l.Login == funcionario.Login && l.Excluido == ConstantesModel.Excluido.NAO).FirstOrDefault();
                if (usrLogando == null)
                {
                    //verifica se existe email
                    usrLogando = db.FuncionarioRepository.GetAllFuncionarios().Where(l => l.Email == funcionario.Login).FirstOrDefault();
                    if (usrLogando == null)
                    {
                        ModelState.AddModelError("Login", "E-mail / Login n�o cadastrado na base de dados.");
                        return View("Login", "_LayoutLogin", funcionario);
                    }
                }

                string senhaLogando = HashValue(funcionario.Senha);
                if (senhaLogando != usrLogando.Senha)
                {
                    ModelState.AddModelError("Senha", "Senha Incorreta.");
                    return View("Login", "_LayoutLogin", funcionario);
                }


                string[] userRoles = Provider.GetRolesForUser(usrLogando.Login);
                string roles = "";
                userRoles.ToList().ForEach(lista =>
                {
                    roles += lista + ",";
                });
                roles = roles.Substring(0, roles.Length - 1);
                    

                //roles = "Administrador";
                var authTicket = new FormsAuthenticationTicket(
                        1,
                        funcionario.Login,
                        DateTime.Now,
                        DateTime.Now.AddMinutes(60),
                        false,
                        roles,
                        "/"
                );
                
                //var cookie = new HttpCookie("GF", FormsAuthentication.Encrypt(authTicket));
                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                cookie.Expires = DateTime.Now.AddMinutes(60);
                Response.Cookies.Add(cookie);

                //HttpCookie authCookie = Request.Cookies["GF"];
                HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];


                return RedirectToAction("Index", "Home");
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View("Login", "_LayoutLogin", funcionario);

            }



        }

        [Authorize(Roles = "Administrador")]
        public static string HashValue(string value)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes;
            using (HashAlgorithm hash = SHA1.Create())
                hashBytes = hash.ComputeHash(encoding.GetBytes(value));

            StringBuilder hashValue = new StringBuilder(hashBytes.Length * 2);
            foreach (byte b in hashBytes)
            {
                hashValue.AppendFormat(CultureInfo.InvariantCulture, "{0:X2}", b);
            }

            return hashValue.ToString();
        }

        public ActionResult AcessoNegado()
        {
            return View();
        }

        public ActionResult Sair()
        {
            FormsAuthentication.SignOut();

            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            return RedirectToAction("Login", "Funcionario");
        }

        // GET: /Funcionario/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult AlterarSenha(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Funcionario funcionario = db.FuncionarioRepository.FindFuncionario(x => x.ID == id, y => y.Pessoa).FirstOrDefault();

                if (funcionario == null)
                {
                    return HttpNotFound();
                }
                string[] userRoles = Provider.GetRolesForUser(funcionario.Login);
                ViewBag.RoleId = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });
                return View(funcionario);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // POST: /Funcionario/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AlterarSenha(Funcionario funcionario, params string[] selectedRoles)
        {
            try
            {
                if (funcionario.Senha != "" && funcionario.Senha != null)
                {
                    Funcionario funcionario2 = db.FuncionarioRepository.FindFuncionarioByFuncionarioId((int)funcionario.ID);
                    funcionario2.Senha = HashValue(funcionario.Senha);
                    //funcionario = funcionario2;

                    db.FuncionarioRepository.UpdateFuncionario(funcionario2);
                    
                    db.SaveChanges();
                    db.FuncionarioRepository.CommitTransaction();
                    @TempData["popup-sucess"] = ConstantesModel.Mensagens.UPDATE_SUCESS;
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError("Senha", "O campo Senha � Obrigat�rio");

                    funcionario = db.FuncionarioRepository.FindFuncionarioByFuncionarioId((int)funcionario.ID);
                    string[] userRoles1 = Provider.GetRolesForUser(funcionario.Login);
                    ViewBag.RoleId = Provider.GetAllRoles().ToList().Select(x => new SelectListItem()
                    {
                        Selected = userRoles1.Contains(x),
                        Text = x,
                        Value = x
                    }
                    );
                }


                
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                
            }
            return View(funcionario);

        }

        // GET: /Funcionario/Edit/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Edit(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                //para lembrar
                ///IQueryable<Funcionario> funcionario = db.FuncionarioRepository.FindFuncionario(x => x.ID == id && x.Login == "alan", y => y.Pessoa, y => y.Pessoa);
                IQueryable<Funcionario> listFuncionario = db.FuncionarioRepository.FindFuncionario(x => x.ID == id, y => y.Pessoa);
                Funcionario funcionario = listFuncionario.FirstOrDefault();

                if (funcionario == null)
                {
                    return HttpNotFound();
                }
                string[] userRoles = Provider.GetRolesForUser(funcionario.Login);
                ViewBag.RoleId = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });
                return View(funcionario);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // POST: /Funcionario/Edit/5
        [Authorize(Roles = "Administrador")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Funcionario funcionario, string loginOld, params string[] selectedRoles)
        {
            string[] userRoles;
            try
            {
                
                if (ModelState.IsValid)
                {
                    funcionario.Pessoa.CPF = funcionario.Pessoa.CPF.Replace(".", "").Replace("-", "");
                    //db.FuncionarioRepository.GetAllFuncioanrios(y => y.Pessoa);
                    Funcionario validaFuncionario = db.FuncionarioRepository.GetAllFuncioanrios(y => y.Pessoa).Where(u => u.Login == funcionario.Login).FirstOrDefault();
                    if (validaFuncionario != null)
                    {
                        if (validaFuncionario.ID != funcionario.ID)
                        {
                            //@TempData["popup-alert"] = "Registro j� cadastrado.";
                            ModelState.AddModelError("Login", "Login j� cadastrado na base de dados.");
                            throw new GFException("");
                        }
                    }

                    validaFuncionario = db.FuncionarioRepository.GetAllFuncionarios().Where(u => u.Email == funcionario.Email).FirstOrDefault();
                    if (validaFuncionario != null)
                    {
                        if (validaFuncionario.ID != funcionario.ID)
                        {
                            //@TempData["popup-alert"] = "Registro j� cadastrado.";
                            ModelState.AddModelError("Email", "E-mail j� cadastrado na base de dados.");
                            throw new GFException("");
                        }
                    }
                    
                    validaFuncionario = db.FuncionarioRepository.GetAllFuncioanrios(y => y.Pessoa).Where(u => u.ID == funcionario.ID).FirstOrDefault();
                    funcionario.Pessoa.CopyProperties(validaFuncionario.Pessoa);
                    funcionario.CopyProperties(validaFuncionario);
                    
                    
                    validaFuncionario.Pessoa.DataModificacao = DateTime.Now;
                    validaFuncionario.Pessoa.Excluido = "N";
                    
                    db.FuncionarioRepository.UpdateFuncionario(validaFuncionario);

                    

                    
                    userRoles = Provider.GetRolesForUser(funcionario.Login);
                    if (selectedRoles != null)
                    {

                        Provider.AddUsersToRoles(funcionario.Login.Split('\n'), selectedRoles);
                    }

                    Provider.RemoveUsersFromRoles(funcionario.Login.Split('\n'), selectedRoles == null ? userRoles : userRoles.Except(selectedRoles).ToArray());

                    
                    db.SaveChanges();
                    db.FuncionarioRepository.CommitTransaction();
                    @TempData["popup-sucess"] = ConstantesModel.Mensagens.UPDATE_SUCESS;
                    return RedirectToAction("Index");

                }
            }
            catch (GFException)
            {

            }                
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

            }

            userRoles = Provider.GetRolesForUser(loginOld);
            ViewBag.RoleId = Provider.GetAllRoles().ToList().Select(x => new SelectListItem() { Selected = userRoles.Contains(x), Text = x, Value = x });


            return View(funcionario);
        }

        // GET: /Funcionario/Delete/5
        [Authorize(Roles = "Administrador")]
        public ActionResult Delete(int id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Funcionario funcionario = db.FuncionarioRepository.FindFuncionario(x => x.ID == id, y => y.Pessoa).FirstOrDefault();
                if (funcionario == null)
                {
                    return HttpNotFound();
                }
                return View(funcionario);
            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                return View();
            }
            
        }

        // POST: /Funcionario/Delete/5
        [Authorize(Roles = "Administrador")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            { 
                Funcionario funcionario;
                funcionario = db.FuncionarioRepository.FindFuncionario(x => x.ID == id, y => y.Pessoa).FirstOrDefault();


                funcionario.Pessoa.Excluido = "S";
                funcionario.Pessoa.DataModificacao = DateTime.Now;
                db.FuncionarioRepository.DeleteFuncionario(funcionario);



                db.SaveChanges();
                db.FuncionarioRepository.CommitTransaction();
                @TempData["popup-sucess"] = ConstantesModel.Mensagens.DELETE_SUCESS;
                return RedirectToAction("Index");
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateException ex1)
            {
                //Console.Write(ex1.Message);
                {
                    var sqlex = ex1.InnerException.InnerException as SqlException;

                    if (sqlex != null)
                    {
                        switch (sqlex.Number)
                        {
                            case 547:
                                TempData["popup-alert"] = "Esse registro possui relaciomento e n�o pode ser apagado!";
                                break; //FK exception
                            case 2627:
                            case 2601:
                                TempData["popup-alert"] = "Registro Duplicado!";
                                break; //primary key exception

                            default: throw sqlex; //otra excepcion que no controlo.


                        }
                    }

                    //throw ex1;
                    return RedirectToAction("Index");
                }

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);

                
                return View();
            }
        }

        [Authorize(Roles = "Administrador")]
        [HttpPost]
        public string GetAjaxData(JQueryDataTablesModel jQueryDataTablesModel)
        {
            string SortBy;
            string SortOrder;
            try
            {
                IQueryable<Funcionario> Result = db.FuncionarioRepository.GetAllFuncioanrios(y => y.Pessoa);
            
            

                if (jQueryDataTablesModel.iSortingCols>0 && jQueryDataTablesModel.iSortCol_ !=null)
                {
               
                    SortBy    = jQueryDataTablesModel.mDataProp_[jQueryDataTablesModel.iSortCol_[0]];
                    SortOrder = jQueryDataTablesModel.sSortDir_ [0];
                }
                else
                {
                    SortBy = "Id";
                    SortOrder = "asc";
                }

                GridViewModel GridState= (GridViewModel)Session[GridInfoKey];
                ICollection<AbstractSearch> SearchCriteria = GridState.GetSearchCriteria();
                int page = jQueryDataTablesModel.iDisplayStart;
                int limit = jQueryDataTablesModel.iDisplayLength;

                IQueryable <Funcionario>ResultFiltered;
                if (SearchCriteria != null)
                    ResultFiltered = Result.ApplySearchCriterias(SearchCriteria);
                else
                    ResultFiltered = Result;

                IOrderedQueryable<Funcionario> ResultOrdered;
                ResultOrdered = ((IOrderedQueryable<Funcionario>)(ResultFiltered.sOrder(SortBy, SortOrder=="asc").Skip(page).Take(limit)));

                int totalRecordCount=Result.Count();
                int searchRecordCount = ResultFiltered.Count();

                var A = ResultOrdered.Select(x => new
                {
                    ID = x.ID,
                    Login = x.Login,
                    Excluido = x.Excluido,
                    Pessoa = new { Nome = x.Pessoa.Nome, CPF = x.Pessoa.CPF },
                    NomeCurto = x.NomeCurto,
                    Email = x.Email,
                });

                //return

                var x1 = this.DataTablesJson(
                    items: A,//ResultOrdered, 
                    totalRecords: totalRecordCount,
                    totalDisplayRecords: searchRecordCount,
                    sEcho: jQueryDataTablesModel.sEcho
                );

                string output = JsonConvert.SerializeObject(x1.Data);

                return output;

            }
            catch (Exception e)
            {
                log = LogManager.GetLogger<Funcionario>();
                log.Error(e);
                ModelState.AddModelError("", ConstantesModel.Mensagens.ERRO);
                throw;
            }

            
        }


        
        [Authorize(Roles = "Administrador")]
        private ICollection<AbstractSearch> CustomizeSearch(ICollection<AbstractSearch> searchCriteria)
        {
            
            searchCriteria.Remove("CTPS");
            searchCriteria.Remove("Cargo");
            searchCriteria.Remove("DataModificacao");
            searchCriteria.Remove("Excluido");
            searchCriteria.Remove("Salario");
            searchCriteria.Remove("ID");
            searchCriteria.Remove("Senha");
            searchCriteria.Remove("Pessoa_ID");


            return searchCriteria.Order();
        }

        
        
		
		protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
