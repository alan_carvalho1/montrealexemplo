﻿using GF.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GenericSearch.Core;
using GenericSearch.UI.Models;
using Newtonsoft.Json;

using System.Data.Entity.Repository;
using Application.Helpers;
using GF.Data.Model;
using GF.Data.Security;
using System.Web.Security;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Data.SqlClient;
using Common.Logging;
using AutoMapper;
using System.Data.Entity.Repository.Attributes;
using System.ComponentModel.DataAnnotations.Schema;
using static GF.Data.Model.ConstantesModel;
using GF.Web.Models;

namespace GF.Controllers
{
    public class HomeController : Controller
    {
        const string GridInfoKey = "Aluno" + "GridData";
        private UnitOfWork db = new UnitOfWork();

        ILog log = LogManager.GetLogger<Aluno>();

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult NoFilter()
        {
            Session[GridInfoKey] = null;
            return RedirectToAction("Index");
        }

        

        [Authorize(Roles = "Administrador")]
        [HttpGet]
        public ActionResult Index()
        {

            return View();

        }



    }
}
