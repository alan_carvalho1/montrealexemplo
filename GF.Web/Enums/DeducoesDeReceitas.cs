
using System.ComponentModel;

public enum DeducoesDeReceita
{
    [Description("ICMS")]
    ICMS,
    [Description("Cofins Sobre Receita Bruta")]
    Confins,
    [Description("PIS/Pasep Sobre Receita Bruta")]
    PIS,
    [Description("Demais Impostos e Contrib Incidentes ")]
    DemaisImpostos,
    
}
