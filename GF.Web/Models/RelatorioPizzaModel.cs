﻿using GF.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GF.Web.Models
{

    public class DataModelPie
    {
        public DataSetModelPie[] datasets { get; set; }
        public string[] labels { get; set; }
    }

    public class DataSetModelPie
    {
        public decimal[] data { get; set; }
        public string[] backgroundColor { get; set; }
        public string label { get; set; }
    }

    public class TitleModelPie
    {
        public bool display { get; set; }
        public string text { get; set; }
    }

    public class OptionsModelPie
    {
        public bool responsive { get; set; }
        public TitleModelPie title { get; set; }
    }

    public class RelatorioModelPie
    {

        public string type { get; set; }
        public DataModelPie data { get; set; }
        public OptionsModelPie options { get; set; }
    
    }
}