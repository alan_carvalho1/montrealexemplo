

using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GF.Web.Models
{
    public class RelFaturamentoLoteModel
    {
        [DataMember(Order = 1, IsRequired = false)]
        [Display(Name = "Data de In�cio")]
        public DateTime DataInicio { get; set; }

        [DataMember(Order = 2, IsRequired = false)]
        [Display(Name = "Data Final")]
        public DateTime DataFinal { get; set; }

        [DataMember(Order = 3, IsRequired = false)]
        [Display(Name = "Compras")]
        public int? IdLancamentoCompra { get; set; }

        [DataMember(Order = 4, IsRequired = false)]
        public string HiddenLancamentoCompra { get; set; }

        [DataMember(Order = 5, IsRequired = false)]
        [Display(Name = "M�s de Refer�ncia")]
        public string DataString { get; set; }
        /*
        [DataMember(Order = 12, IsRequired = false)]
        [Display(Name = "Quantidade de Ve�culos")]
        public int? QtdVeiculo { get; set; } // CentroCusto
        */
    }
}