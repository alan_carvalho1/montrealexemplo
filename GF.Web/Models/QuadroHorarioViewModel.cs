
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using System.Data.Entity.Repository.Model;
using GF.Data.Model;

namespace GF.Web.Models
{
    [DataContract]
    public partial class QuadroHorarioViewModel
    {
        public string HTML { get; set; }
        public string ModalHTML { get; set; }

        [DataMember(Order = 2, IsRequired = true)]
        [Display(Name = "Contrato")]
        public int? Contrato_ID { get; set; }

        [DataMember(Order = 3, IsRequired = true)]
        [Display(Name = "Hor�rio")]
        public string Horario { get; set; }

        [DataMember(Order = 4, IsRequired = true)]
        [Display(Name = "Dia")]
        public string Dia { get; set; }

        

    }

}

