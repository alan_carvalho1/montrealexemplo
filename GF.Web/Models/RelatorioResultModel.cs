﻿

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace GF.Web.Models
{
    public class RelatorioResultModel
    {
        [DataMember(Order = 1, IsRequired = false)]
        [Display(Name = "Ano")]
        public int? Ano { get; set; }

        [DataMember(Order = 2, IsRequired = false)]
        [Display(Name = "Veículo")]
        public int?[] IdVeiculo { get; set; }

        [DataMember(Order = 3, IsRequired = false)]
        [Display(Name = "Status do Veículo")]
        public string StatusVeiculo { get; set; }

        public MultiSelectList Veiculos { get; set; }

        /*
        [DataMember(Order = 12, IsRequired = false)]
        [Display(Name = "Quantidade de Veículos")]
        public int? QtdVeiculo { get; set; } // CentroCusto
        */
    }
}