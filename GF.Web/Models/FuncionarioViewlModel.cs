// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using System.Data.Entity.Repository.Model;

namespace GF.Web.Models
{
//[Template: POCO CLASS][4]
    // Funcionario 
       [DataContract]
    
    public partial class FuncionarioViewlModel : EntidadeBase
    {
		[DataMember(Order = 2, IsRequired = true)]
		[Required] 
		[MaxLength(15, ErrorMessage = "Tamanho máximo do campo: 15 caracteres. ")]
        [Display(Name = "Nome Curto")]
        public string NomeCurto { get; set; } // NomeCurto

        [DataMember(Order = 4, IsRequired = true)]
        [Required]
        [MaxLength(200, ErrorMessage = "Tamanho máximo do campo: 200 caracteres. ")]
        [Display(Name = "Login")]
        public string Login { get; set; } // NomeCurto


        

		[DataMember(Order = 5, IsRequired = true)]
		[MaxLength(200, ErrorMessage = "Tamanho máximo do campo: 200 caracteres. ")]
        [Required]
        public string Email { get; set; } // Email

        [DataMember(Order = 6, IsRequired = true)]
        [Required]
        [MaxLength(50, ErrorMessage = "Tamanho máximo do campo: 50 caracteres. ")]
        public string Senha { get; set; } // Senha

        [DataMember(Order = 7, IsRequired = true)]
        [Display(Name = "Pessoa")]
        public int? Pessoa_ID { get; set; }

        [NotMapped]
        public string Pessoa_Nome { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        public virtual PessoaViewModel Pessoa { get; set; }

        // Reverse navigation
        //public virtual ICollection<FuncionarioGrupo> FuncionariosGrupos { get; set; } // Funcionario_Grupo.FK_dbo.Funcionario_Grupo_dbo.Funcionario_IdFuncionario
        
        public FuncionarioViewlModel()
        {
            //FuncionariosGrupos = new List<FuncionarioGrupo>();
            
        }

        
    }

}
// </auto-generated>
