
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using System.Data.Entity.Repository.Model;

namespace GF.Web.Models
{
    [DataContract]
    public partial class PessoaViewModel : EntidadeBase
    {
        
        
        [MaxLength(50, ErrorMessage = "Tamanho m�ximo do campo: 50 caracteres. ")]
        [Required]
        [Display(Name = "Nome")]
        [DataMember(Order = 2, IsRequired = true)]
        public string Nome { get; set; }
                
        [Display(Name = "Data de Nascimento")]
        [DataMember(Order = 3, IsRequired = true)]
        public DateTime? DataNascimento { get; set; }
        
        [DataMember(Order = 4, IsRequired = false)]
        public string Telefone_Fixo { get; set; }
        
        [DataMember(Order = 5, IsRequired = false)]
        public string Telefone_Celular { get; set; }
        
        [DataMember(Order = 6, IsRequired = false)]
        public string Rua { get; set; }
        
        [DataMember(Order = 7, IsRequired = false)]
        public string Numero { get; set; }
        
        [DataMember(Order = 8, IsRequired = false)]
        public string Bairro { get; set; }
        
        [DataMember(Order = 9, IsRequired = false)]
        public string Cidade { get; set; }
        
        [DataMember(Order = 10, IsRequired = false)]
        public string Estado { get; set; }
        
        [DataMember(Order = 11, IsRequired = false)]
        public string CEP { get; set; }

        [DataMember(Order = 12, IsRequired = false)]
        public string Identidade { get; set; }

        [DataMember(Order = 13, IsRequired = false)]
        public string CPF { get; set; }



        //relacionamentos entre tabelas
        public virtual ICollection<FuncionarioViewlModel> Funcionarios { get; set; }
        public virtual ICollection<AlunoViewModel> Alunos { get; set; }

        public PessoaViewModel()
        {
            Funcionarios = new List<FuncionarioViewlModel>();
            Alunos = new List<AlunoViewModel>();
            
        }

        
    }

}

