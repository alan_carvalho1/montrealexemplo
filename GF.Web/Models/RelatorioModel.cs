﻿using GF.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GF.Web.Models
{

    public class DataModel
    {
        public string[] labels { get; set; }
        public DataSetModel[] datasets { get; set; }
    }

    public class DataSetModel
    {
        public string label { get; set; }
        public string backgroundColor { get; set; }
        public string borderColor { get; set; }
        public decimal[] data { get; set; }
        public bool fill { get; set; }
    }

    public class TitleModel
    {
        public bool display { get; set; }
        public string text { get; set; }
    }

    public class TooltipsModel
    {
        public bool intersect { get; set; }
        public string mode { get; set; }
    }

    public class HoverModel
    {
        public bool intersect { get; set; }
        public string mode { get; set; }
    }

    public class AxesModel
    {
        public bool display { get; set; }
        public ScaleLabelModel scaleLabel { get; set; }

    }

    public class ScaleLabelModel
    {

        public bool display { get; set; }
        public string labelString { get; set; }
    
    }

    public class ScalesModel
    {
        public AxesModel xAxes { get; set; }
        public AxesModel yAxes { get; set; }
    }

    public class OptionsModel
    {
        public bool responsive { get; set; }
        public TitleModel title { get; set; }
        public TooltipsModel tooltips { get; set; }
        public HoverModel hover { get; set; }
        public ScalesModel scales { get; set; }
    }

    public class RelatorioModel
    {

        public string type { get; set; }
        public DataModel data { get; set; }
        public OptionsModel options { get; set; }
    
    }
}