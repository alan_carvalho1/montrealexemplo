﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GF.Models
{
    public class Utils : IUtils
    {
        const string GridInfoKey = "AppointmentDiary" + "GridData";
        private UnitOfWork db = new UnitOfWork();

        public bool InitialiseDiary() {
            // init connection to database
            // DiaryContainer ent = new DiaryContainer();
            return false;
        }

        public static int GetRandomValue(int LowerBound, int UpperBound) {
            Random rnd = new Random();
            return rnd.Next(LowerBound, UpperBound); 
        }

        /// <summary>
        /// sends back a date/time +/- 15 days from todays date
        /// </summary>
        public static DateTime GetRandomAppointmentTime(bool GoBackwards, bool Today) {
            Random rnd = new Random(Environment.TickCount); // set a new random seed each call
            var baseDate = DateTime.Today;
            if (Today)
                return new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, rnd.Next(9, 18), rnd.Next(1, 6)*5, 0);
            else
            {
                int rndDays = rnd.Next(1, 16);
                if (GoBackwards)
                    rndDays = rndDays * -1; // make into negative number
                return new DateTime(baseDate.Year, baseDate.Month, baseDate.Day, rnd.Next(9, 18), rnd.Next(1, 6)*5, 0).AddDays(rndDays);             
            }
        }

    }
}