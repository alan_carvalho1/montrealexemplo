﻿using GF.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GF.Web.Models
{

    public class DataModelBar
    {
        public DataSetModelBar[] datasets { get; set; }
        public string[] labels { get; set; }
    }

    public class DataSetModelBar
    {
        public decimal[] data { get; set; }
        public string[] backgroundColor { get; set; }
        public string[] borderColor { get; set; }
        public int borderWidth { get; set; }
        public string label { get; set; }
    }

    public class TitleModelBar
    {
        public bool display { get; set; }
        public string text { get; set; }
    }

    public class LegendModelBar
    {
        public string position { get; set; }
    }

    public class RectangleModelBar
    {
        public int borderWidth { get; set; }
    }

    public class ElementsModelBar
    {
        public RectangleModelBar rectangle { get; set; }
    }

    public class AxesModelBar
    {
        
        public TicksModelBar ticks { get; set; }

    }

    public class TicksModelBar
    {

        public bool beginAtZero { get; set; }
        public int min { get; set; }

    }

    public class ScalesModelBar
    {
        public AxesModelBar xAxes { get; set; }
        public AxesModelBar yAxes { get; set; }
    }

    public class OptionsModelBar
    {
        public bool responsive { get; set; }
        public TitleModelBar title { get; set; }
        public LegendModelBar legend { get; set; }
        public ElementsModelBar elements { get; set; }
        public ScalesModelBar scales { get; set; }
    }

    public class RelatorioModelBar
    {

        public string type { get; set; }
        public DataModelBar data { get; set; }
        public OptionsModelBar options { get; set; }
    
    }
}