﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GF.Web.Constantes
{
    public class Constantes
    {
        public class ListaStatusMotorista
        {
            public static readonly SelectList lista = new SelectList(new[]
                    {
                    new { Valor = "A", Texto = "Ativo" },
                    new { Valor = "I", Texto = "Inativo" },
                    new { Valor = "D", Texto = "Demitido" }
                }, "Valor", "Texto", "Ativo");
        }

        public static class Mes
        {
            public const int JANEIRO = 1;
            public const int FEVEREIRO = 2;
            public const int MARCO = 3;
            public const int ABRIL = 4;
            public const int MAIO = 5;
            public const int JUNHO = 6;
            public const int JULHO = 7;
            public const int AGOSTO = 8;
            public const int SETEMBRO = 9;
            public const int OUTUBRO = 10;
            public const int NOVEMBRO = 11;
            public const int DEZEMBRO = 12;
        }

        public static class Dias
        {
            public static readonly string[] Semana = { "Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado" };
            public static readonly string[] Hora = { "1am", "2am", "3am", "4am", "5am", "6am", "7am", "8am", "9am", "10am", "11am", "12am", "1pm", "2pm", "3pm", "4pm", "5pm", "6pm", "7pm", "8pm", "9pm", "10pm", "11pm", "12pm" };
        }

        public static class FaixaAlunosPorHorario
        {
            public const int PRIMEIRA_FAIXA = 5;
            public const int SEGUNDA_FAIXA = 10;
            public const int TERCEIRA_FAIXA = 15;
        }

        public static class DicionarioMes
        {
            public static Dictionary<int, string> NomeMes =
                new Dictionary<int, string>() {
                    { Mes.JANEIRO, "Janeiro" },
                    { Mes.FEVEREIRO, "Fevereiro" },
                    { Mes.MARCO, "Março" },
                    { Mes.ABRIL, "Abril" },
                    { Mes.MAIO, "Maio" },
                    { Mes.JUNHO, "Junho" },
                    { Mes.JULHO, "Julho" },
                    { Mes.AGOSTO, "Agosto" },
                    { Mes.SETEMBRO, "Setembro" },
                    { Mes.OUTUBRO, "Outubro" },
                    { Mes.NOVEMBRO, "Novembro" },
                    { Mes.DEZEMBRO, "Dezembro" }};
        }

       
    }
}