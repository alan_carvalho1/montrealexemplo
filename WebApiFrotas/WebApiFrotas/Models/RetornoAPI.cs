﻿using System.Net;

namespace WebApi2_Produtos.Models
{
    public class RetornoAPI<T>
    {
        public bool Sucesso { get; set; }
        public string Mensagem { get; set; }
        public HttpStatusCode CodigoErro { get; set; }
        public T Retorno { get; set; }
    }
}