﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi2_Produtos.Models;

namespace WebApiFrotas.Controllers
{
    public class TipoCombustivelController : ApiController
    {
        private UnitOfWork db = new UnitOfWork();

        public IEnumerable<TipoCombustivel> GetAllTipoCombustivel()
        {
            return db.TipoCombustivelRepository.GetAllTipoCombustivels();
        }

        [Route("api/TipoCombustivel/{id}")]
        public TipoCombustivel GetTipoCombustivel(int id)
        {
            TipoCombustivel motorista = db.TipoCombustivelRepository.FindTipoCombustivelByTipoCombustivelId((int)id);
            
            return motorista;
        }

        [Route("api/TipoCombustivel/Atualizados/{dataAtualizacao}")]
        public IEnumerable<TipoCombustivel> GetTipoCombustivel(DateTime dataAtualizacao)
        {
            return db.TipoCombustivelRepository.GetAllTipoCombustivelsAPI().Where(x => x.DataModificacao >= dataAtualizacao);
        }
    }
}
