﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi2_Produtos.Models;

namespace WebApiFrotas.Controllers
{
    public class LancamentoCombustivelController : ApiController
    {
        private UnitOfWork db = new UnitOfWork();

        public IEnumerable<LancamentoCombustivel> GetAllLancamentoCombustivel()
        {
            return db.LancamentoCombustivelRepository.GetAllLancamentoCombustivels();
        }

        [Route("api/LancamentoCombustivel/{id}")]
        public LancamentoCombustivel GetLancamentoCombustivel(int id)
        {
            LancamentoCombustivel lancamentoCombustivel = db.LancamentoCombustivelRepository.FindLancamentoCombustivelByLancamentoCombustivelId((int)id);
            
            return lancamentoCombustivel;
        }

        [Route("api/LancamentoCombustivel/Atualizados/{dataAtualizacao}")]
        public IEnumerable<LancamentoCombustivel> GetLancamentoCombustivel(DateTime dataAtualizacao)
        {
            return db.LancamentoCombustivelRepository.GetAllLancamentoCombustivelsAPI().Where(x => x.DataModificacao >= dataAtualizacao);
        }

        public RetornoAPI<LancamentoCombustivel> PostLancamentoCombustivel(LancamentoCombustivel item)
        {
            RetornoAPI<LancamentoCombustivel> resposta = new RetornoAPI<LancamentoCombustivel>();
            try
            {
                item = db.LancamentoCombustivelRepository.CreateValidation(item);

                resposta.CodigoErro = HttpStatusCode.Created;
                resposta.Sucesso = true;
                resposta.Mensagem = "Registro Inserido com Sucesso!";
                resposta.Retorno = item;
            }
            catch (Exception e)
            {
                resposta.CodigoErro = HttpStatusCode.InternalServerError;
                resposta.Sucesso = false;
                resposta.Mensagem = "Erro: " + e.Message;
                resposta.Retorno = null;
            }

            

            
            return resposta;
        }

        public RetornoAPI<LancamentoCombustivel> PutLancamentoCombustivel(int id, LancamentoCombustivel lancamentoCombustivel)
        {
            RetornoAPI<LancamentoCombustivel> resposta = new RetornoAPI<LancamentoCombustivel>();
            try
            {
                lancamentoCombustivel.Id = id;

                string msg = db.LancamentoCombustivelRepository.Atualizar(lancamentoCombustivel);

                if (msg != "")
                {
                    resposta.CodigoErro = HttpStatusCode.OK;
                    resposta.Sucesso = false;
                    resposta.Mensagem = msg;
                    resposta.Retorno = lancamentoCombustivel;
                }
                else
                {
                    resposta.CodigoErro = HttpStatusCode.OK;
                    resposta.Sucesso = true;
                    resposta.Mensagem = "Registro Atualizado com Sucesso!";
                    resposta.Retorno = lancamentoCombustivel;
                }
            }
            catch (Exception e)
            {
                resposta.CodigoErro = HttpStatusCode.InternalServerError;
                resposta.Sucesso = false;
                resposta.Mensagem = "Erro: " + e.Message;
                resposta.Retorno = lancamentoCombustivel;
            }

            return resposta;



        }
        
        public RetornoAPI<LancamentoCombustivel> DeleteLancamentoCombustivel(int id)
        {
            RetornoAPI<LancamentoCombustivel> resposta = new RetornoAPI<LancamentoCombustivel>();

            try
            {
                LancamentoCombustivel lancamentoCombustivel = db.LancamentoCombustivelRepository.FindLancamentoCombustivelByLancamentoCombustivelId((int)id);

                if (lancamentoCombustivel == null)
                {
                    resposta.CodigoErro = HttpStatusCode.Found;
                    resposta.Sucesso = false;
                    resposta.Mensagem = "Registro Não Existe!";
                    resposta.Retorno = lancamentoCombustivel;
                }
                else
                {
                    LancamentoCombustivel lancamentoCombustivelDelete = db.LancamentoCombustivelRepository.DeleteLancamentoRefatorado(id);

                    resposta.CodigoErro = HttpStatusCode.OK;
                    resposta.Sucesso = true;
                    resposta.Mensagem = "Registro removido com sucesso!";
                    resposta.Retorno = lancamentoCombustivelDelete;
                }
            }
            catch (Exception e)
            {
                resposta.CodigoErro = HttpStatusCode.InternalServerError;
                resposta.Sucesso = false;
                resposta.Mensagem = "Erro: " + e.Message;
                resposta.Retorno = null;
            }

            return resposta;




        }

        /*
        private void ThrowResponseException(HttpStatusCode statusCode, string message)
        {
            var errorResponse = Request.CreateErrorResponse(statusCode, message);
            throw new HttpResponseException(errorResponse);
        }*/
    }
}
