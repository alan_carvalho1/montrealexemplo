﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi2_Produtos.Models;

namespace WebApiFrotas.Controllers
{
    public class PostoController : ApiController
    {
        private UnitOfWork db = new UnitOfWork();

        public IEnumerable<Posto> GetAllPosto()
        {
            return db.PostoRepository.GetAllPostos();
        }

        [Route("api/Posto/{id}")]
        public Posto GetPosto(int id)
        {
            Posto posto = db.PostoRepository.FindPostoByPostoId((int)id);
            
            return posto;
        }

        [Route("api/Posto/Atualizados/{dataAtualizacao}")]
        public IEnumerable<Posto> GetPosto(DateTime dataAtualizacao)
        {
            return db.PostoRepository.GetAllPostosAPI().Where(x => x.DataModificacao >= dataAtualizacao);
        }
    }
}
