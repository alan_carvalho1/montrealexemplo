﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi2_Produtos.Models;

namespace WebApiFrotas.Controllers
{
    public class VeiculoController : ApiController
    {
        private UnitOfWork db = new UnitOfWork();

        public IEnumerable<Veiculo> GetAllVeiculo()
        {
            return db.VeiculoRepository.GetAllVeiculos();
        }

        [Route("api/Veiculo/{id}")]
        public Veiculo GetVeiculo(int id)
        {
            Veiculo Veiculo = db.VeiculoRepository.FindVeiculoByVeiculoId((int)id);
            
            return Veiculo;
        }

        [Route("api/Veiculo/Atualizados/{dataAtualizacao}")]
        public IEnumerable<Veiculo> GetVeiculo(DateTime dataAtualizacao)
        {
            return db.VeiculoRepository.GetAllVeiculosAPI().Where(x => x.DataModificacao >= dataAtualizacao);
        }
    }
}
