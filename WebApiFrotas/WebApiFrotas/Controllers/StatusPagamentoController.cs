﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi2_Produtos.Models;

namespace WebApiFrotas.Controllers
{
    public class StatusPagamentoController : ApiController
    {
        private UnitOfWork db = new UnitOfWork();

        public IEnumerable<StatusPagamento> GetAllStatusPagamento()
        {
            return db.StatusPagamentoRepository.GetAllStatusPagamentos();
        }

        [Route("api/StatusPagamento/{id}")]
        public StatusPagamento GetStatusPagamento(int id)
        {
            StatusPagamento statusPagamento = db.StatusPagamentoRepository.FindStatusPagamentoByStatusPagamentoId((int)id);
            
            return statusPagamento;
        }

        [Route("api/StatusPagamento/Atualizados/{dataAtualizacao}")]
        public IEnumerable<StatusPagamento> GetStatusPagamento(DateTime dataAtualizacao)
        {
            return db.StatusPagamentoRepository.GetAllStatusPagamentosAPI().Where(x => x.DataModificacao >= dataAtualizacao);
        }
    }
}
