﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi2_Produtos.Models;

namespace WebApi2_Produtos.Controllers
{
    public class ProdutosController : ApiController
    {
        static readonly IProdutoRepositorio repositorio = new ProdutoRepositorio();

        public IEnumerable<Produto> GetAllProdutos()
        {
            return repositorio.GetAll();
        }

        public Produto GetProduto(int id)
        {
            Produto item = repositorio.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return item;
        }
        
        public IEnumerable<Produto> GetProdutosPorCategoria(string categoria)
        {
            return repositorio.GetAll().Where(
                p => string.Equals(p.Categoria, categoria, StringComparison.OrdinalIgnoreCase));
        }
        
        public RetornoAPI<Produto> PostProduto(Produto item)
        {
            RetornoAPI<Produto> resposta = new RetornoAPI<Produto>();
            try
            {
                item = repositorio.Add(item);

                resposta.CodigoErro = HttpStatusCode.Created;
                resposta.Sucesso = true;
                resposta.Mensagem = "Registro Inserido com Sucesso!";
                resposta.Retorno = item;
            } catch (Exception e)
            {
                resposta.CodigoErro = HttpStatusCode.InternalServerError;
                resposta.Sucesso = false;
                resposta.Mensagem = "Erro: " + e.Message;
                resposta.Retorno = item;
            }
            

            /*var response = Request.CreateResponse<Produto>(HttpStatusCode.Created, item);

            string uri = Url.Link("DefaultApi", new { id = item.Id });
            response.Headers.Location = new Uri(uri);
            */

            return resposta;
        }
        
        public RetornoAPI<Produto> PutProduto(int id, Produto produto)
        {
            RetornoAPI<Produto> resposta = new RetornoAPI<Produto>();
            try
            {
                produto.Id = id;
                repositorio.Update(produto);

                resposta.CodigoErro = HttpStatusCode.OK;
                resposta.Sucesso = true;
                resposta.Mensagem = "Registro Atualizado com Sucesso!";
                resposta.Retorno = produto;

            }
            catch (Exception e)
            {
                resposta.CodigoErro = HttpStatusCode.InternalServerError;
                resposta.Sucesso = false;
                resposta.Mensagem = "Erro: " + e.Message;
                resposta.Retorno = produto;
            }

            return resposta;
            
        }
        
        public RetornoAPI<Produto> DeleteProduto(int id)
        {
            RetornoAPI<Produto> resposta = new RetornoAPI<Produto>();

            try
            {
                Produto item = repositorio.Get(id);

                if (item == null)
                {
                    resposta.CodigoErro = HttpStatusCode.Found;
                    resposta.Sucesso = true;
                    resposta.Mensagem = "Registro Não Existe!";
                    resposta.Retorno = item;
                } else { 

                    repositorio.Remove(id);

                    resposta.CodigoErro = HttpStatusCode.OK;
                    resposta.Sucesso = true;
                    resposta.Mensagem = "Registro removido com sucesso!";
                    resposta.Retorno = item;
                }


            }
            catch (Exception e)
            {
                resposta.CodigoErro = HttpStatusCode.InternalServerError;
                resposta.Sucesso = false;
                resposta.Mensagem = "Erro: " + e.Message;
                resposta.Retorno = null;
            }

            return resposta;

            

            

            
        }
    }
}
