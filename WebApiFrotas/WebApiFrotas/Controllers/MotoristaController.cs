﻿using GF.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi2_Produtos.Models;

namespace WebApiFrotas.Controllers
{
    public class MotoristaController : ApiController
    {
        private UnitOfWork db = new UnitOfWork();

        public IEnumerable<Motorista> GetAllMotorista()
        {
            var a = db.MotoristaRepository.GetAllMotoristas().Where(x => x.Status_Funcionario == "Ativo").ToList();
            return a;
        }

        [Route("api/Motorista/{id}")]
        public Motorista GetMotorista(int id)
        {
            Motorista motorista = db.MotoristaRepository.FindMotoristaByMotoristaId((int)id);
            if (motorista == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return motorista;
        }

        [Route("api/Motorista/Atualizados/{dataAtualizacao}")]
        public IEnumerable<Motorista> GetMotorista(DateTime dataAtualizacao)
        {
            return db.MotoristaRepository.GetAllMotoristasAPI().Where(x => x.DataModificacao >= dataAtualizacao);
        }
    }
}
