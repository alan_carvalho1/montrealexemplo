// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
//[Template: Concrete Repository][9]
    public partial  class EmpresaRepository : RepositoryBase<GFContext>, IEmpresaRepository
    {
        public EmpresaRepository(IGFContext Model): base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += EmpresaRepository_RepositoryBaseExceptionRaised;
        }

		public EmpresaRepository()
        {
            base.RepositoryBaseExceptionRaised += EmpresaRepository_RepositoryBaseExceptionRaised;
        }
        void EmpresaRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public bool AddEmpresa(Empresa empresa)
        {
            bool result = false;
            result = Add<Empresa>(empresa);
            return result;
        }
        public bool AddEmpresaWithoutTransaction(Empresa empresa)
        {
            SetUseTransaction(false);
            bool result = false;
            result = Add<Empresa>(empresa);
            return result;
        }
        public bool AddMultipleEmpresas(List<Empresa> empresas)
        {
            bool result = false;
            foreach(Empresa empresa in empresas)
            {
                result = Add<Empresa>(empresa);
            }
            return result;
        }

        public bool AddMultipleEmpresasWithCommit(List<Empresa> empresas)
        {
            bool result = false;

            foreach(Empresa empresa in empresas)
            {
                result = Add<Empresa>(empresa);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool AddOrUpdateEmpresa(Empresa empresa)
        {
            bool result = false;
            result = AddOrUpdate<Empresa>(empresa);
            return result;
        }
        public bool AddOrUpdateMultipleEmpresas(List<Empresa> empresas)
        {
            bool result = false;
            foreach (Empresa empresa in empresas)
            {
                result = AddOrUpdateEmpresa(empresa);
            }
            return result;
        }

        public bool AddOrUpdateMultipleEmpresasWithCommit(List<Empresa> empresas)
        {
            bool result = false;
            foreach (Empresa empresa in empresas)
            {
                result = AddOrUpdateEmpresa(empresa);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeleteEmpresa(Empresa empresa)
        {
            bool result = false;
            result = Delete<Empresa>(empresa);
            return result;
        }
        public bool UpdateEmpresa(Empresa empresa)
        {
            bool result = false;
            result = Update<Empresa>(empresa);
            return result;
        }

        public bool UpdateMultipleEmpresas(List<Empresa> empresas)
        {
            bool result = false;
            foreach (Empresa empresa in empresas)
            {
                result = UpdateEmpresa(empresa);
            }
            return result;
        }
        public bool UpdateMultipleEmpresasWithCommit(List<Empresa> empresas)
        {
            bool result = false;
            foreach (Empresa empresa in empresas)
            {
                result = UpdateEmpresa(empresa);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
		public IQueryable<Empresa> GetAllEmpresas()
        {
            IQueryable<Empresa> result = GetAll<Empresa>();
            return result;
        }

		public IQueryable<Empresa> GetAllEmpresas(params Expression<Func<Empresa, object>>[] includes) 
        {
            IQueryable<Empresa> result = GetAll<Empresa>(includes);
            return result;
        }
		 public Empresa FindEmpresaByEmpresaId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            Empresa result = Find<Empresa>(x => x.Id == Id1).FirstOrDefault();
            return result;
        }
		public IQueryable<Empresa> FindEmpresa(Expression<Func<Empresa, bool>> where1)
        {
            IQueryable<Empresa> result = Find<Empresa>(where1);
			return result;
		}
    }


//******************************************************************************************************************************************************

}
// </auto-generated>
