// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Usuario_Grupo
    public partial class UsuarioGrupoConfiguration : EntityTypeConfiguration<UsuarioGrupo>
    {
        public UsuarioGrupoConfiguration()
            : this("dbo")
        {
        }
 
        public UsuarioGrupoConfiguration(string schema)
        {
            ToTable(schema + ".Usuario_Grupo");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.IdUsuario).HasColumnName("IdUsuario").IsRequired().HasColumnType("int");
            Property(x => x.IdGrupo).HasColumnName("IdGrupo").IsRequired().HasColumnType("int");

            // Foreign keys
            HasRequired(a => a.Grupo).WithMany(b => b.UsuariosGrupos).HasForeignKey(c => c.IdGrupo); // FK_dbo.Usuario_Grupo_dbo.Grupo_IdGrupo
            HasRequired(a => a.Usuario).WithMany(b => b.UsuariosGrupos).HasForeignKey(c => c.IdUsuario); // FK_dbo.Usuario_Grupo_dbo.Usuario_IdUsuario
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
