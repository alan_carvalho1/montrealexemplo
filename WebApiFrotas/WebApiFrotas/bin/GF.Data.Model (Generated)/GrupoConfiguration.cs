// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Grupo
    public partial class GrupoConfiguration : EntityTypeConfiguration<Grupo>
    {
        public GrupoConfiguration()
            : this("dbo")
        {
        }
 
        public GrupoConfiguration(string schema)
        {
            ToTable(schema + ".Grupo");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Nome).HasColumnName("Nome").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            Property(x => x.Status).HasColumnName("Status").IsRequired().IsUnicode(false).HasColumnType("varchar").HasMaxLength(1);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
