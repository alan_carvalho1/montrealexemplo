﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace GF.Data.Model
{
    [DataContract]
    public abstract class EntidadeBase
    {
        [Key]
        [DataMember(Order = 1, IsRequired = true)]
        [Required(ErrorMessage = "Campo obrigatório")]
        public virtual int ID { get; set; } // Id (Primary key)

        [DataMember(IsRequired = false)]
        public virtual DateTime? DataModificacao { get; set; }
        [DataMember(IsRequired = false)]
        [Display(Name = "Status")]
        public virtual string Excluido { get; set; }
    }
}
