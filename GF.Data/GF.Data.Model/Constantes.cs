﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GF.Data.Model
{
    public class ConstantesModel
    {
        
        public static class Excluido
        {
            public const string NAO = "N";
            public const string SIM = "S";
            public const string INATIVO = "I";
            public const string CONTRATO_ENCERRADO = "E";
            public const string REMOVIDO_CARTEIRA = "R";
        }

        public static class StatusPagamento
        {
            public const string PAGO = "Pago";
            public const string PENDENTE = "Pendente";
            public const string VENCIDO = "Vencido";
        }

        public static class ConstTipoTransacao
        {
            public const int RECEITA_DIVERSOS = 1;
            public const int DESPESA = 2;
            public const int MULTA = 3;
            public const int MENSALIDADE = 4;
            public const int DEDUCOES_RECEITAS = 5;
            public const int DESPESAS_OPERACIONAIS = 6;
            public const int DESPESAS_TRIBUTARIAS = 7;
            public const int RECEITAS_FINANCEIRAS = 8;
            public const int DESPESAS_FINANCEIRAS = 9;
            public const int DESPESAS_NAO_OPERACIONAIS = 10;
            public const int IMPOSTOS = 11;
            public const int SALARIO = 12;
            public const int RECEITAS = 13;




        }

        public static class TipoPlanejamento
        {
            public const int SALARIO = 8;
        }

        public static class Mensagens
        {
            public const string ERRO = "Ocorreu um erro ao tentar executar a ação.";
            public const string ADD_SUCESS = "Registro Cadastrado com Sucesso.";
            public const string DELETE_SUCESS = "Registro Removido com Sucesso.";
            public const string CONTRATO_ENCERRADO = "Contrato Encerrado com Sucesso.";
            public const string UPDATE_SUCESS = "Registro atualizado com sucesso.";
            public const string DUPLICATE = "Registro já cadastrado.";
            public const string TIPO_TRANSACAO_NAO_CADASTRADA = "Transação não existe para edição.";
        }
    }
}