
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    
    public partial class Plano_ServicoRepository : RepositoryBase<GFContext>, IPlano_ServicoRepository
    {
        public Plano_ServicoRepository(IGFContext Model) : base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += Plano_ServicoRepository_RepositoryBaseExceptionRaised;
        }

        public Plano_ServicoRepository()
        {
            base.RepositoryBaseExceptionRaised += Plano_ServicoRepository_RepositoryBaseExceptionRaised;
        }
        void Plano_ServicoRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public Plano_Servico AddPlano_Servico(Plano_Servico vPlano_Servico)
        {
            vPlano_Servico.DataModificacao = DateTime.Now;
            vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
            
            Plano_Servico result = Add<Plano_Servico>(vPlano_Servico);
            return result;
        }
        public Plano_Servico AddPlano_ServicoWithoutTransaction(Plano_Servico vPlano_Servico)
        {
            vPlano_Servico.DataModificacao = DateTime.Now;
            vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
            SetUseTransaction(false);
            
            Plano_Servico result = Add<Plano_Servico>(vPlano_Servico);
            return result;
        }
        public bool AddMultiplePlano_Servicos(List<Plano_Servico> vPlano_Servicos)
        {
            bool result = false;
            foreach (Plano_Servico vPlano_Servico in vPlano_Servicos)
            {
                vPlano_Servico.DataModificacao = DateTime.Now;
                vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
                Plano_Servico result2 = Add<Plano_Servico>(vPlano_Servico);
                result = true;
            }
            return result;
        }

        public bool AddMultiplePlano_ServicosWithCommit(List<Plano_Servico> vPlano_Servicos)
        {
            bool result = false;

            foreach (Plano_Servico vPlano_Servico in vPlano_Servicos)
            {
                vPlano_Servico.DataModificacao = DateTime.Now;
                vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
                Plano_Servico result2 = Add<Plano_Servico>(vPlano_Servico);
                CommitTransaction(startNewTransaction: true);
                result = true;
            }
            return result;
        }
        public bool AddOrUpdatePlano_Servico(Plano_Servico vPlano_Servico)
        {
            vPlano_Servico.DataModificacao = DateTime.Now;
            vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = AddOrUpdate<Plano_Servico>(vPlano_Servico);
            return result;
        }
        public bool AddOrUpdateMultiplePlano_Servicos(List<Plano_Servico> vPlano_Servicos)
        {
            bool result = false;
            foreach (Plano_Servico vPlano_Servico in vPlano_Servicos)
            {
                vPlano_Servico.DataModificacao = DateTime.Now;
                vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdatePlano_Servico(vPlano_Servico);
            }
            return result;
        }

        public bool AddOrUpdateMultiplePlano_ServicosWithCommit(List<Plano_Servico> vPlano_Servicos)
        {
            bool result = false;
            foreach (Plano_Servico vPlano_Servico in vPlano_Servicos)
            {
                vPlano_Servico.DataModificacao = DateTime.Now;
                vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdatePlano_Servico(vPlano_Servico);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeletePlano_Servico(Plano_Servico vPlano_Servico)
        {
            vPlano_Servico.DataModificacao = DateTime.Now;
            vPlano_Servico.Excluido = ConstantesModel.Excluido.SIM;
            bool result = false;
            result = Update<Plano_Servico>(vPlano_Servico);
            return result;
        }
        public bool UpdatePlano_Servico(Plano_Servico vPlano_Servico)
        {
            vPlano_Servico.DataModificacao = DateTime.Now;
            vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = Update<Plano_Servico>(vPlano_Servico);
            return result;
        }

        public bool UpdateMultiplePlano_Servicos(List<Plano_Servico> vPlano_Servicos)
        {
            bool result = false;
            foreach (Plano_Servico vPlano_Servico in vPlano_Servicos)
            {
                vPlano_Servico.DataModificacao = DateTime.Now;
                vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdatePlano_Servico(vPlano_Servico);
            }
            return result;
        }
        public bool UpdateMultiplePlano_ServicosWithCommit(List<Plano_Servico> vPlano_Servicos)
        {
            bool result = false;
            foreach (Plano_Servico vPlano_Servico in vPlano_Servicos)
            {
                vPlano_Servico.DataModificacao = DateTime.Now;
                vPlano_Servico.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdatePlano_Servico(vPlano_Servico);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public IQueryable<Plano_Servico> GetAllPlano_Servicos()
        {
            IQueryable<Plano_Servico> result = GetAll<Plano_Servico>().Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }

        public IQueryable<Plano_Servico> GetAllPlano_Servicos(params Expression<Func<Plano_Servico, object>>[] includes)
        {
            IQueryable<Plano_Servico> result = GetAll<Plano_Servico>(includes).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
        public Plano_Servico FindPlano_ServicoByPlano_ServicoId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            Plano_Servico result = Find<Plano_Servico>(x => x.ID == Id1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO).FirstOrDefault();
            return result;
        }
        /*
        public Plano_Servico FindPlano_ServicoByPlano_ServicoCodigo(string codigo)
        {
            Plano_Servico result = Find<Plano_Servico>(x => x.Codigo == codigo).FirstOrDefault();
            return result;
        }*/
        public IQueryable<Plano_Servico> FindPlano_Servico(Expression<Func<Plano_Servico, bool>> where1)
        {
            IQueryable<Plano_Servico> result = Find<Plano_Servico>(where1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
    }


    //******************************************************************************************************************************************************

}




