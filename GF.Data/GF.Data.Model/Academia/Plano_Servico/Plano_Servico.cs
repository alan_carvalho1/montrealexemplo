
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;


namespace GF.Data.Model
{
    [DataContract]
    public partial class Plano_Servico : EntidadeBase
    {        
        [DataMember(Order = 2, IsRequired = true)]
        [Display(Name = "Nome")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo Nome: 255 caracteres. ")]
        [Required]
        public string Nome { get; set; }
        
        [DataMember(Order = 3, IsRequired = true)]
        [Display(Name = "Valor")]
        [Required]
        public decimal Valor { get; set; }
        
        [DataMember(Order = 4, IsRequired = false)]
        [Display(Name = "Descri��o")]
        [MaxLength(2000, ErrorMessage = "Tamanho m�ximo do campo Descri��o: 2000 caracteres. ")]
        public string Descricao { get; set; }
        
        [DataMember(Order = 5, IsRequired = false)]
        [Display(Name = "Dura��o (meses)")]
        public int Duracao { get; set; }

        [DataMember(Order = 6, IsRequired = false)]
        [Display(Name = "Periodo")]
        public int? Periodo_ID { get; set; }

        //relacionamentos entre tabelas
        //public virtual ICollection<LancamentoCombustivel> LancamentoCombustivels { get; set; }

        [ForeignKey("Aluno_contrato")]
        public virtual ICollection<Contrato> Contratos { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Periodo")]
        public virtual Periodo Periodo { get; set; }

        public Plano_Servico()
        {
            Contratos = new List<Contrato>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}

