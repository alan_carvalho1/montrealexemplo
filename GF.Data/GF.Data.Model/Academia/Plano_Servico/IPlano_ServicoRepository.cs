
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    public partial interface IPlano_ServicoRepository : IRepositoryBase
    {
        Plano_Servico AddPlano_Servico(Plano_Servico vPlano_Servico);
        Plano_Servico AddPlano_ServicoWithoutTransaction(Plano_Servico vPlano_Servico);
        bool AddMultiplePlano_Servicos(List<Plano_Servico> vPlano_Servicos);
        bool AddMultiplePlano_ServicosWithCommit(List<Plano_Servico> vPlano_Servicos);
        bool AddOrUpdatePlano_Servico(Plano_Servico vPlano_Servico);
        bool AddOrUpdateMultiplePlano_Servicos(List<Plano_Servico> vPlano_Servicos);
        bool AddOrUpdateMultiplePlano_ServicosWithCommit(List<Plano_Servico> vPlano_Servicos);
        bool DeletePlano_Servico(Plano_Servico vPlano_Servico);
        bool UpdatePlano_Servico(Plano_Servico vPlano_Servico);
        bool UpdateMultiplePlano_Servicos(List<Plano_Servico> vPlano_Servicos);
        IQueryable<Plano_Servico> GetAllPlano_Servicos();
        IQueryable<Plano_Servico> GetAllPlano_Servicos(params Expression<Func<Plano_Servico, object>>[] includes);
        Plano_Servico FindPlano_ServicoByPlano_ServicoId(int Id1);
        IQueryable<Plano_Servico> FindPlano_Servico(Expression<Func<Plano_Servico, bool>> where1);
    }

}


