
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Plano_Servico
    public partial class Plano_ServicoConfiguration : EntityTypeConfiguration<Plano_Servico>
    {
        public Plano_ServicoConfiguration()
            : this("dbo")
        {
        }

        public Plano_ServicoConfiguration(string schema)
        {
            ToTable(schema + ".Plano_Servico");
            HasKey(x => x.ID);
			Property(x => x.ID).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			
        	Property(x => x.ID).HasColumnName("ID").IsRequired().HasColumnType("int");
            Property(x => x.Periodo_ID).HasColumnName("Periodo_ID").HasColumnType("int");

            Property(x => x.Nome).HasColumnName("Nome").IsOptional().HasColumnType("varchar");
        	
        	Property(x => x.Valor).HasColumnName("Valor").IsOptional().HasColumnType("decimal");
        	
        	Property(x => x.Descricao).HasColumnName("Descricao").IsOptional().HasColumnType("varchar");
        	
        	Property(x => x.Duracao).HasColumnName("Duracao").IsOptional().HasColumnType("int");
        	
        	Property(x => x.DataModificacao).HasColumnName("DataModificacao").IsRequired().HasColumnType("datetime");
        	
        	Property(x => x.Excluido).HasColumnName("Excluido").IsRequired().HasColumnType("char");

            HasOptional(a => a.Periodo).WithMany(b => b.Plano_Servicos).HasForeignKey(c => c.Periodo_ID);
            
            InitializePartial();
        }
        partial void InitializePartial();
    }

}





