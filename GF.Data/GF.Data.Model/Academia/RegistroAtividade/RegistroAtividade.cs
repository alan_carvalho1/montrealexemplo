
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;


namespace GF.Data.Model
{
    [DataContract]
    public partial class RegistroAtividade : EntidadeBase
    {
        
        [DataMember(Order = 1, IsRequired = true)]
        [Display(Name = "Aluno")]
        public int? Contrato_ID { get; set; }

        [DataMember(Order = 2, IsRequired = true)]
        [Display(Name = "Professor")]
        public int? Funcionario_ID { get; set; }

        [DataMember(Order = 3, IsRequired = true)]
        [Display(Name = "Data de Registro")]
        public DateTime? DataRegistro { get; set; }
        
        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Contrato_ID")]
        public virtual Contrato Contrato { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Funcionario_ID")]
        public virtual Funcionario Funcionario { get; set; }

        [NotMapped]
        [Display(Name = "Data de Registro")]
        public string DataRegistroTexto { get; set; }


        //relacionamentos entre tabelas
        //public virtual ICollection<LancamentoCombustivel> LancamentoCombustivels { get; set; }

        public RegistroAtividade()
        {
            //LancamentoCombustivels = new List<LancamentoCombustivel>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}

