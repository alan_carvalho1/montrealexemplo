
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using System.Data.Entity.Repository.Model;
using GF.Data.Model;

namespace GF.Data.Model
{
    [DataContract]
    public partial class RelatorioFrequenciaDataViewModel
    {
        public int ID { get; set; }
        
        public int AulasRealizadas { get; set; }
        public int AulasPrevistas { get; set; }
        //public int Porcentagem { get; set; }
        [Display(Name = "Frequência")]
        public int Frequencia { get; set; }

        public int Segunda { get; set; }
        public int Terca { get; set; }
        public int Quarta { get; set; }
        public int Quinta { get; set; }
        public int Sexta { get; set; }
        public int Sabado { get; set; }
        public int Domingo { get; set; }

        public DateTime DataRegistro { get; set; }

        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }

        public string FuncionarioTop { get; set; }
        public int? qtdFuncionarioTop { get; set; }

        [Display(Name = "Professor Responsável")]
        public string ProfessorResponsavel { get; set; }
        public string Aluno { get; set; }



        /*
        [DataMember(Order = 2, IsRequired = true)]
        [Display(Name = "Frequência")]
        public int Frequencia { get; set; }
        
        public int QtdAulas { get; set; }
        
        public int Previsao { get; set; }

        public DateTime? DataRegistro { get; set; }

        [Display(Name = "Aluno")]
        public string Contrato_Aluno_Pessoa_Nome { get; set; }

        [Display(Name = "Professor")]
        public string Funcionario_Pessoa_Nome { get; set; }

        

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Contrato_ID")]
        public virtual Contrato Contrato { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Funcionario_ID")]
        public virtual Funcionario Funcionario { get; set; }*/

    }

}

