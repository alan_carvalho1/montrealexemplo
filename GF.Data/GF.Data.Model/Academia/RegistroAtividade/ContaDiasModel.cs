
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using System.Data.Entity.Repository.Model;
using GF.Data.Model;

namespace GF.Data.Model
{
    public partial class ContaDiasModel
    {
        public int qtdDomingo { get; set; }
        public int qtdSegunda { get; set; }
        public int qtdTerca { get; set; }
        public int qtdQuarta { get; set; }
        public int qtdQuinta { get; set; }
        public int qtdSexta { get; set; }
        public int qtdSabado { get; set; }
    }

}

