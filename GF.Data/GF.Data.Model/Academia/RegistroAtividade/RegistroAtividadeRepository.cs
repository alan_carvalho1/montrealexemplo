
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    
    public partial class RegistroAtividadeRepository : RepositoryBase<GFContext>, IRegistroAtividadeRepository
    {
        public RegistroAtividadeRepository(IGFContext Model) : base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += RegistroAtividadeRepository_RepositoryBaseExceptionRaised;
        }

        public ContaDiasModel GetDiffDays(DateTime initialDate, DateTime finalDate)
        {
            ContaDiasModel data = new ContaDiasModel();

            int days = 0;
            int daysCount = 0;
            days = initialDate.Subtract(finalDate).Days;

            //Módulo 
            if (days < 0)
                days = days * -1;

            for (int i = 1; i <= days; i++)
            {
                initialDate = initialDate.AddDays(1);
                //Conta apenas dias da semana.
                if (initialDate.DayOfWeek == DayOfWeek.Sunday)
                    data.qtdDomingo++;

                if (initialDate.DayOfWeek == DayOfWeek.Saturday)
                    data.qtdSabado++;

                if (initialDate.DayOfWeek == DayOfWeek.Monday)
                    data.qtdSegunda++;

                if (initialDate.DayOfWeek == DayOfWeek.Tuesday)
                    data.qtdTerca++;

                if (initialDate.DayOfWeek == DayOfWeek.Wednesday)
                    data.qtdQuarta++;

                if (initialDate.DayOfWeek == DayOfWeek.Thursday)
                    data.qtdQuinta++;

                if (initialDate.DayOfWeek == DayOfWeek.Friday)
                    data.qtdSexta++;

            }
            return data;
        }

        public IQueryable<RelatorioFrequenciaDataViewModel> RelatorioFrequencia(RelatorioFrequenciaDataViewModel rel)
        {
            /* Model.Database.SqlQuery<string>()
             var studentList = Model.Students
                     .SqlQuery("Select * from Students")
                     .ToList<Student>();*/

            //var studentName = Model.Database.SqlQuery<int>("select aluno_id as alan from contrato").ToList();
            string inicio = rel.DataInicio.ToString("yyyy-dd-MM");
            string fim = rel.DataFim.ToString("yyyy-dd-MM");

            string query = "SET DATEFORMAT dmy; " +
                "select C.ID as ID,  P.nome as Aluno, " +
                    " (select count(ID) from RegistroAtividade where C.ID = Contrato_ID and Excluido != 'S' " +
                        " and DataRegistro >= '" + inicio + "' " +
                        " and DataRegistro <= '" + fim + "') " +
                        " as AulasRealizadas, " +
                        " (select top 1 td.Nome from (select p.Nome, count(p.nome) as val from RegistroAtividade RA inner join Funcionario F on RA.Funcionario_ID = F.ID inner join pessoa P on F.Pessoa_ID = P.ID where RA.Contrato_ID = C.ID and RA.Excluido != 'S' and ra.DataRegistro >= '" + inicio + "' " +
                        " and ra.DataRegistro <= '" + fim + "' " + " group by p.Nome) as td order by val desc) as FuncionarioTop, " +
                        " (select top 1 td.val from (select p.Nome, count(p.nome) as val from RegistroAtividade RA inner join Funcionario F on RA.Funcionario_ID = F.ID inner join pessoa P on F.Pessoa_ID = P.ID where RA.Contrato_ID = C.ID and RA.Excluido != 'S' and ra.DataRegistro >= '" + inicio + "' " +
                        " and ra.DataRegistro <= '" + fim + "' " + " group by p.Nome) as td order by val desc) as qtdFuncionarioTop,  " +
                        " 'Teste' as ProfessorResponsavel, " +
                    " (0) as Domingo, " +
                    " (2) as Segunda, " +
                    " (0) as Terca, " +
                    " (1) as Quarta, " +
                    " (1) as Quinta, " +
                    " (0) as Sexta, " +
                    " (0) as Sabado " +
                    " from Contrato C " +
                    " inner join aluno A on C.Aluno_ID = A.ID " +
                    " inner join Pessoa P on A.Pessoa_ID = P.ID " +
                    " where c.Excluido = 'N'";

            IQueryable<RelatorioFrequenciaDataViewModel> data =
                Model.Database.SqlQuery<RelatorioFrequenciaDataViewModel>
                (query, "USA").AsQueryable();

            var result = data.ToList();

            var Resultado = result.Select(x => new RelatorioFrequenciaDataViewModel
            {
                ID = x.ID,
                Frequencia = 0,
                AulasRealizadas = x.AulasRealizadas,
                AulasPrevistas = 0,
                Domingo = x.Domingo,
                Segunda = x.Segunda,
                Terca = x.Terca,
                Quarta = x.Quarta,
                Quinta = x.Quinta,
                Sexta = x.Sexta,
                Sabado = x.Sabado,

                FuncionarioTop = x.FuncionarioTop,
                qtdFuncionarioTop = x.qtdFuncionarioTop,
                ProfessorResponsavel = x.ProfessorResponsavel,
                Aluno = x.Aluno


            }).ToList();


            /*****************************aqui faço as contas ************************/
            ContaDiasModel resultCont = GetDiffDays(rel.DataInicio, rel.DataFim);
            int i = 0;
            Resultado.ForEach(res =>
            {
                int aulasPrevistas = (Resultado[i].Domingo * resultCont.qtdDomingo) +
                            (Resultado[i].Segunda * resultCont.qtdSegunda) +
                            (Resultado[i].Terca * resultCont.qtdTerca) +
                            (Resultado[i].Quarta * resultCont.qtdQuarta) +
                            (Resultado[i].Quinta * resultCont.qtdQuinta) +
                            (Resultado[i].Sexta * resultCont.qtdSexta) +
                            (Resultado[i].Sabado * resultCont.qtdSabado);


                Resultado[i].AulasPrevistas = aulasPrevistas;

                Resultado[i].AulasRealizadas = res.AulasRealizadas;

                Resultado[i].Frequencia = (res.AulasRealizadas * 100) / ((aulasPrevistas == 0) ? 1 : aulasPrevistas);

                i++;
            });
            /**************************************************************************/
            data = Resultado.AsQueryable();
            return data;
        }

        public RegistroAtividadeRepository()
        {
            base.RepositoryBaseExceptionRaised += RegistroAtividadeRepository_RepositoryBaseExceptionRaised;
        }
        void RegistroAtividadeRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public RegistroAtividade AddRegistroAtividade(RegistroAtividade vRegistroAtividade)
        {
            vRegistroAtividade.DataModificacao = DateTime.Now;
            vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
            
            RegistroAtividade result = Add<RegistroAtividade>(vRegistroAtividade);
            return result;
        }
        public RegistroAtividade AddRegistroAtividadeWithoutTransaction(RegistroAtividade vRegistroAtividade)
        {
            vRegistroAtividade.DataModificacao = DateTime.Now;
            vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
            SetUseTransaction(false);

            RegistroAtividade result = Add<RegistroAtividade>(vRegistroAtividade);
            return result;
        }
        public bool AddMultipleRegistroAtividades(List<RegistroAtividade> vRegistroAtividades)
        {
            bool result = false;
            foreach (RegistroAtividade vRegistroAtividade in vRegistroAtividades)
            {
                vRegistroAtividade.DataModificacao = DateTime.Now;
                vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
                RegistroAtividade result2 = Add<RegistroAtividade>(vRegistroAtividade);
                result = true;
            }
            return result;
        }

        public bool AddMultipleRegistroAtividadesWithCommit(List<RegistroAtividade> vRegistroAtividades)
        {
            bool result = false;

            foreach (RegistroAtividade vRegistroAtividade in vRegistroAtividades)
            {
                vRegistroAtividade.DataModificacao = DateTime.Now;
                vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
                RegistroAtividade result2 = Add<RegistroAtividade>(vRegistroAtividade);
                CommitTransaction(startNewTransaction: true);
                result = true;
            }
            return result;
        }
        public bool AddOrUpdateRegistroAtividade(RegistroAtividade vRegistroAtividade)
        {
            vRegistroAtividade.DataModificacao = DateTime.Now;
            vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = AddOrUpdate<RegistroAtividade>(vRegistroAtividade);
            return result;
        }
        public bool AddOrUpdateMultipleRegistroAtividades(List<RegistroAtividade> vRegistroAtividades)
        {
            bool result = false;
            foreach (RegistroAtividade vRegistroAtividade in vRegistroAtividades)
            {
                vRegistroAtividade.DataModificacao = DateTime.Now;
                vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdateRegistroAtividade(vRegistroAtividade);
            }
            return result;
        }

        public bool AddOrUpdateMultipleRegistroAtividadesWithCommit(List<RegistroAtividade> vRegistroAtividades)
        {
            bool result = false;
            foreach (RegistroAtividade vRegistroAtividade in vRegistroAtividades)
            {
                vRegistroAtividade.DataModificacao = DateTime.Now;
                vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdateRegistroAtividade(vRegistroAtividade);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeleteRegistroAtividade(RegistroAtividade vRegistroAtividade)
        {
            vRegistroAtividade.DataModificacao = DateTime.Now;
            vRegistroAtividade.Excluido = ConstantesModel.Excluido.SIM;
            bool result = false;
            result = Update<RegistroAtividade>(vRegistroAtividade);
            return result;
        }
        public bool UpdateRegistroAtividade(RegistroAtividade vRegistroAtividade)
        {
            vRegistroAtividade.DataModificacao = DateTime.Now;
            vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = Update<RegistroAtividade>(vRegistroAtividade);
            return result;
        }

        public bool RemoverRegistroAtividade(RegistroAtividade vRegistroAtividade)
        {
            vRegistroAtividade.DataModificacao = DateTime.Now;
            vRegistroAtividade.Excluido = ConstantesModel.Excluido.REMOVIDO_CARTEIRA;
            bool result = false;
            result = Update<RegistroAtividade>(vRegistroAtividade);
            return result;
        }

        public bool UpdateMultipleRegistroAtividades(List<RegistroAtividade> vRegistroAtividades)
        {
            bool result = false;
            foreach (RegistroAtividade vRegistroAtividade in vRegistroAtividades)
            {
                vRegistroAtividade.DataModificacao = DateTime.Now;
                vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdateRegistroAtividade(vRegistroAtividade);
            }
            return result;
        }
        public bool UpdateMultipleRegistroAtividadesWithCommit(List<RegistroAtividade> vRegistroAtividades)
        {
            bool result = false;
            foreach (RegistroAtividade vRegistroAtividade in vRegistroAtividades)
            {
                vRegistroAtividade.DataModificacao = DateTime.Now;
                vRegistroAtividade.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdateRegistroAtividade(vRegistroAtividade);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public IQueryable<RegistroAtividade> GetAllRegistroAtividades()
        {
            IQueryable<RegistroAtividade> result = GetAll<RegistroAtividade>().Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }

        public IQueryable<RegistroAtividade> GetAllRegistroAtividades(params Expression<Func<RegistroAtividade, object>>[] includes)
        {
            IQueryable<RegistroAtividade> result = GetAll<RegistroAtividade>(includes).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
        public RegistroAtividade FindRegistroAtividadeByRegistroAtividadeId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            RegistroAtividade result = Find<RegistroAtividade>(x => x.ID == Id1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO).FirstOrDefault();
            return result;
        }
        /*
        public RegistroAtividade FindRegistroAtividadeByRegistroAtividadeCodigo(string codigo)
        {
            RegistroAtividade result = Find<RegistroAtividade>(x => x.Codigo == codigo).FirstOrDefault();
            return result;
        }*/
        public IQueryable<RegistroAtividade> FindRegistroAtividade(Expression<Func<RegistroAtividade, bool>> where1)
        {
            IQueryable<RegistroAtividade> result = Find<RegistroAtividade>(where1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
    }


    //******************************************************************************************************************************************************

}




