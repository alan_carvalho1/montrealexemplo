




using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // RegistroAtividade
    public partial class RegistroAtividadeConfiguration : EntityTypeConfiguration<RegistroAtividade>
    {
        public RegistroAtividadeConfiguration()
            : this("dbo")
        {
        }

        public RegistroAtividadeConfiguration(string schema)
        {
            ToTable(schema + ".RegistroAtividade");
            HasKey(x => x.ID);
			Property(x => x.ID).HasColumnName("ID").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

        
            Property(x => x.Contrato_ID).HasColumnName("Contrato_ID").IsRequired().HasColumnType("int");
            Property(x => x.Funcionario_ID).HasColumnName("Funcionario_ID").IsRequired().HasColumnType("int");
        
            Property(x => x.DataRegistro).HasColumnName("DataRegistro").IsRequired().HasColumnType("datetime");
        
            Property(x => x.DataModificacao).HasColumnName("DataModificacao").IsRequired().HasColumnType("datetime");
        
            Property(x => x.Excluido).HasColumnName("Excluido").IsRequired().HasColumnType("char");

            HasRequired(a => a.Contrato).WithMany(b => b.RegistroAtividades).HasForeignKey(c => c.Contrato_ID);
            HasRequired(a => a.Funcionario).WithMany(b => b.RegistroAtividades).HasForeignKey(c => c.Funcionario_ID);

            InitializePartial();
        }
        partial void InitializePartial();
    }

}





