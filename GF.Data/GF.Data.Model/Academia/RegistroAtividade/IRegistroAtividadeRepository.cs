
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    public partial interface IRegistroAtividadeRepository : IRepositoryBase
    {
        RegistroAtividade AddRegistroAtividade(RegistroAtividade vRegistroAtividade);
        RegistroAtividade AddRegistroAtividadeWithoutTransaction(RegistroAtividade vRegistroAtividade);
        bool AddMultipleRegistroAtividades(List<RegistroAtividade> vRegistroAtividades);
        bool AddMultipleRegistroAtividadesWithCommit(List<RegistroAtividade> vRegistroAtividades);
        bool AddOrUpdateRegistroAtividade(RegistroAtividade vRegistroAtividade);
        bool AddOrUpdateMultipleRegistroAtividades(List<RegistroAtividade> vRegistroAtividades);
        bool AddOrUpdateMultipleRegistroAtividadesWithCommit(List<RegistroAtividade> vRegistroAtividades);
        bool DeleteRegistroAtividade(RegistroAtividade vRegistroAtividade);
        bool UpdateRegistroAtividade(RegistroAtividade vRegistroAtividade);
        bool UpdateMultipleRegistroAtividades(List<RegistroAtividade> vRegistroAtividades);
        IQueryable<RegistroAtividade> GetAllRegistroAtividades();
        IQueryable<RegistroAtividade> GetAllRegistroAtividades(params Expression<Func<RegistroAtividade, object>>[] includes);
        RegistroAtividade FindRegistroAtividadeByRegistroAtividadeId(int Id1);
        IQueryable<RegistroAtividade> FindRegistroAtividade(Expression<Func<RegistroAtividade, bool>> where1);
        bool RemoverRegistroAtividade(RegistroAtividade vRegistroAtividade);
        IQueryable<RelatorioFrequenciaDataViewModel> RelatorioFrequencia(RelatorioFrequenciaDataViewModel rel);
    }

}


