
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    public partial interface IAlunoRepository : IRepositoryBase
    {
        Aluno AddAluno(Aluno vAluno);
        Aluno AddAlunoWithoutTransaction(Aluno vAluno);
        bool AddMultipleAlunos(List<Aluno> vAlunos);
        bool AddMultipleAlunosWithCommit(List<Aluno> vAlunos);
        bool AddOrUpdateAluno(Aluno vAluno);
        bool AddOrUpdateMultipleAlunos(List<Aluno> vAlunos);
        bool AddOrUpdateMultipleAlunosWithCommit(List<Aluno> vAlunos);
        bool DeleteAluno(Aluno vAluno);
        bool UpdateAluno(Aluno vAluno);
        bool UpdateMultipleAlunos(List<Aluno> vAlunos);
        IQueryable<Aluno> GetAllAlunos();
        IQueryable<Aluno> GetAllAlunos(params Expression<Func<Aluno, object>>[] includes);
        Aluno FindAlunoByAlunoId(int Id1);
        IQueryable<Aluno> FindAluno(Expression<Func<Aluno, bool>> where1);
        Aluno GerarMatricula(Aluno aluno);
    }

}


