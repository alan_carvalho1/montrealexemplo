
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;
using System.Data.Entity.Repository.Attributes;

namespace GF.Data.Model
{
    
    public partial class AlunoRepository : RepositoryBase<GFContext>, IAlunoRepository
    {
        public AlunoRepository(IGFContext Model) : base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += AlunoRepository_RepositoryBaseExceptionRaised;
        }
        public AlunoRepository()
        {
            base.RepositoryBaseExceptionRaised += AlunoRepository_RepositoryBaseExceptionRaised;
        }
        void AlunoRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public Aluno AddAluno(Aluno vAluno)
        {
            vAluno.DataModificacao = DateTime.Now;
            vAluno.Excluido = ConstantesModel.Excluido.NAO;
            
            Aluno result = Add<Aluno>(vAluno);
            return result;
        }
        public Aluno AddAlunoWithoutTransaction(Aluno vAluno)
        {
            vAluno.DataModificacao = DateTime.Now;
            vAluno.Excluido = ConstantesModel.Excluido.NAO;
            SetUseTransaction(false);
            
            Aluno result = Add<Aluno>(vAluno);
            return result;
        }
        public bool AddMultipleAlunos(List<Aluno> vAlunos)
        {
            bool result = false;
            foreach (Aluno vAluno in vAlunos)
            {
                vAluno.DataModificacao = DateTime.Now;
                vAluno.Excluido = ConstantesModel.Excluido.NAO;
                Aluno result2 = Add<Aluno>(vAluno);
                result = true;
            }
            return result;
        }

        public bool AddMultipleAlunosWithCommit(List<Aluno> vAlunos)
        {
            bool result = false;

            foreach (Aluno vAluno in vAlunos)
            {
                vAluno.DataModificacao = DateTime.Now;
                vAluno.Excluido = ConstantesModel.Excluido.NAO;
                Aluno result2 = Add<Aluno>(vAluno);
                CommitTransaction(startNewTransaction: true);
                result = true;
            }
            return result;
        }
        public bool AddOrUpdateAluno(Aluno vAluno)
        {
            vAluno.DataModificacao = DateTime.Now;
            vAluno.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = AddOrUpdate<Aluno>(vAluno);
            return result;
        }
        public bool AddOrUpdateMultipleAlunos(List<Aluno> vAlunos)
        {
            bool result = false;
            foreach (Aluno vAluno in vAlunos)
            {
                vAluno.DataModificacao = DateTime.Now;
                vAluno.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdateAluno(vAluno);
            }
            return result;
        }

        public bool AddOrUpdateMultipleAlunosWithCommit(List<Aluno> vAlunos)
        {
            bool result = false;
            foreach (Aluno vAluno in vAlunos)
            {
                vAluno.DataModificacao = DateTime.Now;
                vAluno.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdateAluno(vAluno);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeleteAluno(Aluno vAluno)
        {
            vAluno.DataModificacao = DateTime.Now;
            vAluno.Excluido = ConstantesModel.Excluido.SIM;
            bool result = false;
            result = Update<Aluno>(vAluno);
            return result;
        }

        

        public bool UpdateAluno(Aluno vAluno)
        {
            vAluno.DataModificacao = DateTime.Now;
            //vAluno.Excluido = ConstantesModel.Excluido.NAO;
            //AtualizarFks(vAluno);
            bool result = false;
            result = Update<Aluno>(vAluno);
            return result;
        }

        public bool UpdateMultipleAlunos(List<Aluno> vAlunos)
        {
            bool result = false;
            foreach (Aluno vAluno in vAlunos)
            {
                vAluno.DataModificacao = DateTime.Now;
                //vAluno.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdateAluno(vAluno);
            }
            return result;
        }
        public bool UpdateMultipleAlunosWithCommit(List<Aluno> vAlunos)
        {
            bool result = false;
            foreach (Aluno vAluno in vAlunos)
            {
                vAluno.DataModificacao = DateTime.Now;
                //vAluno.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdateAluno(vAluno);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public IQueryable<Aluno> GetAllAlunos()
        {
            IQueryable<Aluno> result = GetAll<Aluno>().Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }

        public Aluno GerarMatricula(Aluno aluno)
        {
            Aluno validaAluno = GetAllAlunos(y => y.Pessoa).Where(a => a.Matricula.Contains(DateTime.Now.Year.ToString())).OrderByDescending(k => k.ID).FirstOrDefault();
            int matricula = 0;
            if (validaAluno != null)
            {
                matricula = int.Parse(validaAluno.Matricula);
                matricula++;
                aluno.Matricula = matricula.ToString();
            }
            else
            {
                aluno.Matricula = DateTime.Now.Year.ToString() + "0001";
            }

            return validaAluno;
        }

        public IQueryable<Aluno> GetAllAlunos(params Expression<Func<Aluno, object>>[] includes)
        {
            IQueryable<Aluno> result = GetAll<Aluno>(includes).Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }
        public Aluno FindAlunoByAlunoId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            Aluno result = Find<Aluno>(x => x.ID == Id1).Where(x => x.Excluido != ConstantesModel.Excluido.SIM).FirstOrDefault();
            return result;
        }
        /*
        public Aluno FindAlunoByAlunoCodigo(string codigo)
        {
            Aluno result = Find<Aluno>(x => x.Codigo == codigo).FirstOrDefault();
            return result;
        }*/
        public IQueryable<Aluno> FindAluno(Expression<Func<Aluno, bool>> where1)
        {
            IQueryable<Aluno> result = Find<Aluno>(where1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
    }


    //******************************************************************************************************************************************************

}




