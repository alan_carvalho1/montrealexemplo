
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;


namespace GF.Data.Model
{
    [DataContract]
    public partial class Aluno : EntidadeBase
    {

        [DataMember(Order = 2, IsRequired = true)]
        [Display(Name = "Pessoa")]
        public int? Pessoa_ID { get; set; }

        [DataMember(Order = 3, IsRequired = true)]
        [Display(Name = "Matr�cula")]
        [MaxLength(8, ErrorMessage = "Tamanho m�ximo do campo Matr�cula: 8 caracteres. ")]
        public string Matricula { get; set; }

        [DataMember(Order = 4, IsRequired = false)]
        [Display(Name = "Local de Treino")]
        [MaxLength(200, ErrorMessage = "Tamanho m�ximo do campo Local: 200 caracteres. ")]
        public string LocalTreino { get; set; }

        [DataMember(Order = 5, IsRequired = false)]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "E-mail em formato inv�lido.")]
        [MaxLength(200, ErrorMessage = "Tamanho m�ximo do campo Email: 200 caracteres. ")]
        public string Email { get; set; }

        [DataMember(Order = 6, IsRequired = false)]
        [Display(Name = "Profiss�o")]
        [MaxLength(200, ErrorMessage = "Tamanho m�ximo do campo Profiss�o: 200 caracteres. ")]
        public string Profissao { get; set; }

        [DataMember(Order = 7, IsRequired = false)]
        [Display(Name = "Sexo")]
        [MaxLength(200, ErrorMessage = "Tamanho m�ximo do campo Sexo: 200 caracteres. ")]
        public string Sexo { get; set; }

        [DataMember(Order = 7, IsRequired = false)]
        [Display(Name = "Estado Civil")]
        [MaxLength(200, ErrorMessage = "Tamanho m�ximo do campo Estado Civil: 200 caracteres. ")]
        public string EstadoCivil { get; set; }

        [NotMapped]
        public string Pessoa_Nome { get; set; }

        [NotMapped]
        public string Pessoa_CPF { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Aluno")]
        public virtual Pessoa Pessoa { get; set; }


        [ForeignKey("Aluno_contrato")]
        public virtual ICollection<Contrato> Contratos { get; set; }
        //relacionamentos entre tabelas
        //public virtual ICollection<LancamentoCombustivel> LancamentoCombustivels { get; set; }

        public Aluno()
        {
            Contratos = new List<Contrato>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}

