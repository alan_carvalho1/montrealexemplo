
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Aluno
    public partial class AlunoConfiguration : EntityTypeConfiguration<Aluno>
    {
        public AlunoConfiguration()
            : this("dbo")
        {
        }

        public AlunoConfiguration(string schema)
        {
            ToTable(schema + ".Aluno");
            HasKey(x => x.ID);
			Property(x => x.ID).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            
        	//Property(x => x.Pessoa_ID).HasColumnName("Pessoa_ID").IsRequired().HasColumnType("int");
        	
        	Property(x => x.Matricula).HasColumnName("Matricula").IsRequired().HasColumnType("varchar");
            Property(x => x.LocalTreino).HasColumnName("LocalTreino").IsOptional().HasColumnType("varchar");
            Property(x => x.Email).HasColumnName("Email").IsOptional().HasColumnType("varchar");
            Property(x => x.Profissao).HasColumnName("Profissao").IsOptional().HasColumnType("varchar");
            Property(x => x.Sexo).HasColumnName("Sexo").IsOptional().HasColumnType("varchar");
            Property(x => x.EstadoCivil).HasColumnName("EstadoCivil").IsOptional().HasColumnType("varchar");

            Property(x => x.DataModificacao).HasColumnName("DataModificacao").IsRequired().HasColumnType("datetime");
        	
        	Property(x => x.Excluido).HasColumnName("Excluido").IsRequired().HasColumnType("char");

            HasRequired(a => a.Pessoa).WithMany(b => b.Alunos).HasForeignKey(c => c.Pessoa_ID);



            InitializePartial();
        }
        partial void InitializePartial();
    }

}





