
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    public partial interface IPessoaRepository : IRepositoryBase
    {
        Pessoa AddPessoa(Pessoa vPessoa);
        Pessoa AddPessoaWithoutTransaction(Pessoa vPessoa);
        bool AddMultiplePessoas(List<Pessoa> vPessoas);
        bool AddMultiplePessoasWithCommit(List<Pessoa> vPessoas);
        bool AddOrUpdatePessoa(Pessoa vPessoa);
        bool AddOrUpdateMultiplePessoas(List<Pessoa> vPessoas);
        bool AddOrUpdateMultiplePessoasWithCommit(List<Pessoa> vPessoas);
        bool DeletePessoa(Pessoa vPessoa);
        bool UpdatePessoa(Pessoa vPessoa);
        bool UpdateMultiplePessoas(List<Pessoa> vPessoas);
        IQueryable<Pessoa> GetAllPessoas();
        IQueryable<Pessoa> GetAllPessoas(params Expression<Func<Pessoa, object>>[] includes);
        Pessoa FindPessoaByPessoaId(int Id1);
        IQueryable<Pessoa> FindPessoa(Expression<Func<Pessoa, bool>> where1);
    }

}


