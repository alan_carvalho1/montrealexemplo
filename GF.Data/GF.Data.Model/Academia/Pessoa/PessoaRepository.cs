
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    
    public partial class PessoaRepository : RepositoryBase<GFContext>, IPessoaRepository
    {
        public PessoaRepository(IGFContext Model) : base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += PessoaRepository_RepositoryBaseExceptionRaised;
        }

        public PessoaRepository()
        {
            base.RepositoryBaseExceptionRaised += PessoaRepository_RepositoryBaseExceptionRaised;
        }
        void PessoaRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public Pessoa AddPessoa(Pessoa vPessoa)
        {
            vPessoa.DataModificacao = DateTime.Now;
            vPessoa.Excluido = ConstantesModel.Excluido.NAO;
            
            Pessoa result = Add<Pessoa>(vPessoa);
            return result;
        }
        public Pessoa AddPessoaWithoutTransaction(Pessoa vPessoa)
        {
            vPessoa.DataModificacao = DateTime.Now;
            vPessoa.Excluido = ConstantesModel.Excluido.NAO;
            SetUseTransaction(false);
            
            Pessoa result = Add<Pessoa>(vPessoa);
            return result;
        }
        public bool AddMultiplePessoas(List<Pessoa> vPessoas)
        {
            bool result = false;
            foreach (Pessoa vPessoa in vPessoas)
            {
                vPessoa.DataModificacao = DateTime.Now;
                vPessoa.Excluido = ConstantesModel.Excluido.NAO;
                Pessoa result2 = Add<Pessoa>(vPessoa);
                result = true;
            }
            return result;
        }

        public bool AddMultiplePessoasWithCommit(List<Pessoa> vPessoas)
        {
            bool result = false;

            foreach (Pessoa vPessoa in vPessoas)
            {
                vPessoa.DataModificacao = DateTime.Now;
                vPessoa.Excluido = ConstantesModel.Excluido.NAO;
                Pessoa result2 = Add<Pessoa>(vPessoa);
                CommitTransaction(startNewTransaction: true);
                result = true;
            }
            return result;
        }
        public bool AddOrUpdatePessoa(Pessoa vPessoa)
        {
            vPessoa.DataModificacao = DateTime.Now;
            vPessoa.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = AddOrUpdate<Pessoa>(vPessoa);
            return result;
        }
        public bool AddOrUpdateMultiplePessoas(List<Pessoa> vPessoas)
        {
            bool result = false;
            foreach (Pessoa vPessoa in vPessoas)
            {
                vPessoa.DataModificacao = DateTime.Now;
                vPessoa.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdatePessoa(vPessoa);
            }
            return result;
        }

        public bool AddOrUpdateMultiplePessoasWithCommit(List<Pessoa> vPessoas)
        {
            bool result = false;
            foreach (Pessoa vPessoa in vPessoas)
            {
                vPessoa.DataModificacao = DateTime.Now;
                vPessoa.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdatePessoa(vPessoa);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeletePessoa(Pessoa vPessoa)
        {
            vPessoa.DataModificacao = DateTime.Now;
            vPessoa.Excluido = ConstantesModel.Excluido.SIM;
            bool result = false;
            result = Update<Pessoa>(vPessoa);
            return result;
        }
        public bool UpdatePessoa(Pessoa vPessoa)
        {
            vPessoa.DataModificacao = DateTime.Now;
            vPessoa.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = Update<Pessoa>(vPessoa);
            return result;
        }

        public bool UpdateMultiplePessoas(List<Pessoa> vPessoas)
        {
            bool result = false;
            foreach (Pessoa vPessoa in vPessoas)
            {
                vPessoa.DataModificacao = DateTime.Now;
                vPessoa.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdatePessoa(vPessoa);
            }
            return result;
        }
        public bool UpdateMultiplePessoasWithCommit(List<Pessoa> vPessoas)
        {
            bool result = false;
            foreach (Pessoa vPessoa in vPessoas)
            {
                vPessoa.DataModificacao = DateTime.Now;
                vPessoa.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdatePessoa(vPessoa);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public IQueryable<Pessoa> GetAllPessoas()
        {
            IQueryable<Pessoa> result = GetAll<Pessoa>().Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }

        public IQueryable<Pessoa> GetAllPessoas(params Expression<Func<Pessoa, object>>[] includes)
        {
            IQueryable<Pessoa> result = GetAll<Pessoa>(includes).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
        public Pessoa FindPessoaByPessoaId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            Pessoa result = Find<Pessoa>(x => x.ID == Id1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO).FirstOrDefault();
            return result;
        }
        /*
        public Pessoa FindPessoaByPessoaCodigo(string codigo)
        {
            Pessoa result = Find<Pessoa>(x => x.Codigo == codigo).FirstOrDefault();
            return result;
        }*/
        public IQueryable<Pessoa> FindPessoa(Expression<Func<Pessoa, bool>> where1)
        {
            IQueryable<Pessoa> result = Find<Pessoa>(where1).Where(x => x.Excluido == ConstantesModel.Excluido.NAO);
            return result;
        }
    }


    //******************************************************************************************************************************************************

}




