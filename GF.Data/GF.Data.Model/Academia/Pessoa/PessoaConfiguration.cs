
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Pessoa
    public partial class PessoaConfiguration : EntityTypeConfiguration<Pessoa>
    {
        public PessoaConfiguration()
            : this("dbo")
        {
        }

        public PessoaConfiguration(string schema)
        {
            ToTable(schema + ".Pessoa");
            HasKey(x => x.ID);
			Property(x => x.ID).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

        	Property(x => x.Nome).HasColumnName("Nome").IsRequired().HasColumnType("varchar");
        	
        	Property(x => x.DataNascimento).HasColumnName("DataNascimento").HasColumnType("datetime");
        	
        	Property(x => x.Telefone_Fixo).HasColumnName("Telefone_Fixo").HasColumnType("varchar");
        	
        	Property(x => x.Telefone_Celular).HasColumnName("Telefone_Celular").HasColumnType("varchar");
        	
        	Property(x => x.Rua).HasColumnName("Rua").HasColumnType("varchar");
        	
        	Property(x => x.Numero).HasColumnName("Numero").HasColumnType("varchar");
        	
        	Property(x => x.Bairro).HasColumnName("Bairro").HasColumnType("varchar");
        	
        	Property(x => x.Cidade).HasColumnName("Cidade").HasColumnType("varchar");
        	
        	Property(x => x.Estado).HasColumnName("Estado").HasColumnType("varchar");
        	
        	Property(x => x.CEP).HasColumnName("CEP").HasColumnType("varchar");

            Property(x => x.Identidade).HasColumnName("Identidade").HasColumnType("varchar");

            Property(x => x.CPF).HasColumnName("CPF").HasColumnType("varchar");

            Property(x => x.DataModificacao).HasColumnName("DataModificacao").HasColumnType("datetime");
        	
        	Property(x => x.Excluido).HasColumnName("Excluido").IsRequired().HasColumnType("char");
        	


            InitializePartial();
        }
        partial void InitializePartial();
    }

}





