
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;


namespace GF.Data.Model
{
    [DataContract]
    public partial class Pessoa : EntidadeBase
    {
        
        
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo Nome: 255 caracteres. ")]
        [Required]
        [Display(Name = "Nome")]
        [DataMember(Order = 2, IsRequired = true)]
        public string Nome { get; set; }
                
        [Display(Name = "Data de Nascimento")]
        [DataMember(Order = 3, IsRequired = true)]
        [Required]
        public DateTime? DataNascimento { get; set; }
        
        [DataMember(Order = 4, IsRequired = false)]
        [Display(Name = "Telefone Fixo")]
        [MaxLength(20, ErrorMessage = "Tamanho m�ximo do campo Telefone Fixo: 20 caracteres. ")]
        public string Telefone_Fixo { get; set; }
        
        [DataMember(Order = 5, IsRequired = false)]
        [Display(Name = "Telefone Celular")]
        [MaxLength(20, ErrorMessage = "Tamanho m�ximo do campo Celular: 20 caracteres. ")]
        public string Telefone_Celular { get; set; }
        
        [DataMember(Order = 6, IsRequired = false)]
        [Display(Name = "Rua")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo Rua: 255 caracteres. ")]
        public string Rua { get; set; }
        
        [DataMember(Order = 7, IsRequired = false)]
        [Display(Name = "N�mero")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo N�mero: 255 caracteres. ")]
        public string Numero { get; set; }
        
        [DataMember(Order = 8, IsRequired = false)]
        [Display(Name = "Bairro")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo Bairro: 255 caracteres. ")]
        public string Bairro { get; set; }
        
        [DataMember(Order = 9, IsRequired = false)]
        [Display(Name = "Cidade")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo Cidade: 255 caracteres. ")]
        public string Cidade { get; set; }
        
        [DataMember(Order = 10, IsRequired = false)]
        [Display(Name = "Estado")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo Estado: 255 caracteres. ")]
        public string Estado { get; set; }
        
        [DataMember(Order = 11, IsRequired = false)]
        [Display(Name = "CEP")]
        [MaxLength(255, ErrorMessage = "Tamanho m�ximo do campo CEP: 255 caracteres. ")]
        public string CEP { get; set; }

        [DataMember(Order = 12, IsRequired = false)]
        [MaxLength(15, ErrorMessage = "Tamanho m�ximo do campo Identidade: 15 caracteres. ")]
        public string Identidade { get; set; }

        [Required]
        [Display(Name = "CPF")]        
        [DataMember(Order = 13, IsRequired = false)]
        public string CPF { get; set; }



        //relacionamentos entre tabelas
        [ForeignKey("Pessoa_funcionario")]
        public virtual ICollection<Funcionario> Funcionarios { get; set; }
        [ForeignKey("Pessoa_aluno")]
        public virtual ICollection<Aluno> Alunos { get; set; }

        public Pessoa()
        {
            Funcionarios = new List<Funcionario>();
            Alunos = new List<Aluno>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}

