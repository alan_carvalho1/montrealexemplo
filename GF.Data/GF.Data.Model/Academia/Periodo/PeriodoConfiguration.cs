using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Periodo
    public partial class PeriodoConfiguration : EntityTypeConfiguration<Periodo>
    {
        public PeriodoConfiguration()
            : this("dbo")
        {
        }

        public PeriodoConfiguration(string schema)
        {
            ToTable(schema + ".Periodo");
            HasKey(x => x.ID);
			Property(x => x.ID).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(x => x.Descricao).HasColumnName("Descricao").HasColumnType("varchar");
            Property(x => x.Duracao).HasColumnName("Duracao").HasColumnType("int");



            InitializePartial();
        }
        partial void InitializePartial();
    }

}





