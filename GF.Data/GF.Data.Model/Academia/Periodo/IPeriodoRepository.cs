
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    public partial interface IPeriodoRepository : IRepositoryBase
    {
        Periodo AddPeriodo(Periodo vPeriodo);
        Periodo AddPeriodoWithoutTransaction(Periodo vPeriodo);
        bool AddMultiplePeriodos(List<Periodo> vPeriodos);
        bool AddMultiplePeriodosWithCommit(List<Periodo> vPeriodos);
        bool AddOrUpdatePeriodo(Periodo vPeriodo);
        bool AddOrUpdateMultiplePeriodos(List<Periodo> vPeriodos);
        bool AddOrUpdateMultiplePeriodosWithCommit(List<Periodo> vPeriodos);
        bool DeletePeriodo(Periodo vPeriodo);
        bool UpdatePeriodo(Periodo vPeriodo);
        bool UpdateMultiplePeriodos(List<Periodo> vPeriodos);
        IQueryable<Periodo> GetAllPeriodos();
        IQueryable<Periodo> GetAllPeriodos(params Expression<Func<Periodo, object>>[] includes);
        Periodo FindPeriodoByPeriodoId(int Id1);
        IQueryable<Periodo> FindPeriodo(Expression<Func<Periodo, bool>> where1);
        IQueryable<Periodo> FindPeriodo(Expression<Func<Periodo, bool>> where, params Expression<Func<Periodo, object>>[] includes);
    }

}


