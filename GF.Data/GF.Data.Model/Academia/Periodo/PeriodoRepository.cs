
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    
    public partial class PeriodoRepository : RepositoryBase<GFContext>, IPeriodoRepository
    {
        public PeriodoRepository(IGFContext Model) : base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += PeriodoRepository_RepositoryBaseExceptionRaised;
        }

        public PeriodoRepository()
        {
            base.RepositoryBaseExceptionRaised += PeriodoRepository_RepositoryBaseExceptionRaised;
        }
        void PeriodoRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public Periodo AddPeriodo(Periodo vPeriodo)
        {
            vPeriodo.DataModificacao = DateTime.Now;
            vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
            //bool result = false;
            Periodo result = Add<Periodo>(vPeriodo);
            return result;
        }
        public Periodo AddPeriodoWithoutTransaction(Periodo vPeriodo)
        {
            vPeriodo.DataModificacao = DateTime.Now;
            vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
            SetUseTransaction(false);
            //bool result = false;
            Periodo result = Add<Periodo>(vPeriodo);
            return result;
        }
        public bool AddMultiplePeriodos(List<Periodo> vPeriodos)
        {
            bool result = false;
            foreach (Periodo vPeriodo in vPeriodos)
            {
                vPeriodo.DataModificacao = DateTime.Now;
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
                Periodo result2 = Add<Periodo>(vPeriodo);
                result = true;
            }
            return result;
        }

        public bool AddMultiplePeriodosWithCommit(List<Periodo> vPeriodos)
        {
            bool result = false;

            foreach (Periodo vPeriodo in vPeriodos)
            {
                vPeriodo.DataModificacao = DateTime.Now;
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
                Periodo result2 = Add<Periodo>(vPeriodo);
                CommitTransaction(startNewTransaction: true);
                result = true;
            }
            return result;
        }
        public bool AddOrUpdatePeriodo(Periodo vPeriodo)
        {
            vPeriodo.DataModificacao = DateTime.Now;
            vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = AddOrUpdate<Periodo>(vPeriodo);
            return result;
        }
        public bool AddOrUpdateMultiplePeriodos(List<Periodo> vPeriodos)
        {
            bool result = false;
            foreach (Periodo vPeriodo in vPeriodos)
            {
                vPeriodo.DataModificacao = DateTime.Now;
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdatePeriodo(vPeriodo);
            }
            return result;
        }

        public bool AddOrUpdateMultiplePeriodosWithCommit(List<Periodo> vPeriodos)
        {
            bool result = false;
            foreach (Periodo vPeriodo in vPeriodos)
            {
                vPeriodo.DataModificacao = DateTime.Now;
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdatePeriodo(vPeriodo);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeletePeriodo(Periodo vPeriodo)
        {
            vPeriodo.DataModificacao = DateTime.Now;
            vPeriodo.Excluido = ConstantesModel.Excluido.CONTRATO_ENCERRADO;
            bool result = false;
            result = Update<Periodo>(vPeriodo);
            return result;
        }
        public bool UpdatePeriodo(Periodo vPeriodo)
        {
            vPeriodo.DataModificacao = DateTime.Now;
            if(vPeriodo.Excluido != ConstantesModel.Excluido.CONTRATO_ENCERRADO)
            {
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
            }
            
            bool result = false;
            result = Update<Periodo>(vPeriodo);
            return result;
        }

        public bool UpdateMultiplePeriodos(List<Periodo> vPeriodos)
        {
            bool result = false;
            foreach (Periodo vPeriodo in vPeriodos)
            {
                vPeriodo.DataModificacao = DateTime.Now;
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdatePeriodo(vPeriodo);
            }
            return result;
        }
        public bool UpdateMultiplePeriodosWithCommit(List<Periodo> vPeriodos)
        {
            bool result = false;
            foreach (Periodo vPeriodo in vPeriodos)
            {
                vPeriodo.DataModificacao = DateTime.Now;
                vPeriodo.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdatePeriodo(vPeriodo);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public IQueryable<Periodo> GetAllPeriodos()
        {
            IQueryable<Periodo> result = GetAll<Periodo>().Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }

        public IQueryable<Periodo> GetAllPeriodos(params Expression<Func<Periodo, object>>[] includes)
        {
            IQueryable<Periodo> result = GetAll<Periodo>(includes).Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }
        public Periodo FindPeriodoByPeriodoId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            Periodo result = Find<Periodo>(x => x.ID == Id1).Where(x => x.Excluido != ConstantesModel.Excluido.SIM).FirstOrDefault();
            return result;
        }
        /*
        public Periodo FindPeriodoByPeriodoCodigo(string codigo)
        {
            Periodo result = Find<Periodo>(x => x.Codigo == codigo).FirstOrDefault();
            return result;
        }*/
        public IQueryable<Periodo> FindPeriodo(Expression<Func<Periodo, bool>> where1)
        {
            IQueryable<Periodo> result = Find<Periodo>(where1).Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }

        public IQueryable<Periodo> FindPeriodo(Expression<Func<Periodo, bool>> where, params Expression<Func<Periodo, object>>[] includes)
        {
            IQueryable<Periodo> result = Find<Periodo>(where, includes);
            return result;
        }
    }


    //******************************************************************************************************************************************************

}




