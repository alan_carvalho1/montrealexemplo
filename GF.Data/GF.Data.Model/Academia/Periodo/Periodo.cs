
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;


namespace GF.Data.Model
{
    [DataContract]
    public partial class Periodo : EntidadeBase
    {
	
        [DataMember(Order = 2, IsRequired = false)]
        [Display(Name = "Descri��o")]
        public string Descricao { get; set; }
        
        [DataMember(Order = 3, IsRequired = false)]
        [Display(Name = "Dura��o")]
        public int Duracao { get; set; }

        [ForeignKey("Plano_Servico")]
        public virtual ICollection<Plano_Servico> Plano_Servicos { get; set; }

        public Periodo()
        {
            Plano_Servicos = new List<Plano_Servico>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}

