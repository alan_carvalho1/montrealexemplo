
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    
    public partial class ContratoRepository : RepositoryBase<GFContext>, IContratoRepository
    {
        public ContratoRepository(IGFContext Model) : base(throwExceptions: true, useTransactions: false, model: (DbContext)Model)
        {
            base.RepositoryBaseExceptionRaised += ContratoRepository_RepositoryBaseExceptionRaised;
        }

        public ContratoRepository()
        {
            base.RepositoryBaseExceptionRaised += ContratoRepository_RepositoryBaseExceptionRaised;
        }
        void ContratoRepository_RepositoryBaseExceptionRaised(Exception exception)
        {
            throw new NotImplementedException();
        }
        public Contrato AddContrato(Contrato vContrato)
        {
            vContrato.DataModificacao = DateTime.Now;
            vContrato.Excluido = ConstantesModel.Excluido.NAO;
            //bool result = false;
            Contrato result = Add<Contrato>(vContrato);
            return result;
        }
        public Contrato AddContratoWithoutTransaction(Contrato vContrato)
        {
            vContrato.DataModificacao = DateTime.Now;
            vContrato.Excluido = ConstantesModel.Excluido.NAO;
            SetUseTransaction(false);
            //bool result = false;
            Contrato result = Add<Contrato>(vContrato);
            return result;
        }
        public bool AddMultipleContratos(List<Contrato> vContratos)
        {
            bool result = false;
            foreach (Contrato vContrato in vContratos)
            {
                vContrato.DataModificacao = DateTime.Now;
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
                Contrato result2 = Add<Contrato>(vContrato);
                result = true;
            }
            return result;
        }

        public bool AddMultipleContratosWithCommit(List<Contrato> vContratos)
        {
            bool result = false;

            foreach (Contrato vContrato in vContratos)
            {
                vContrato.DataModificacao = DateTime.Now;
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
                Contrato result2 = Add<Contrato>(vContrato);
                CommitTransaction(startNewTransaction: true);
                result = true;
            }
            return result;
        }
        public bool AddOrUpdateContrato(Contrato vContrato)
        {
            vContrato.DataModificacao = DateTime.Now;
            vContrato.Excluido = ConstantesModel.Excluido.NAO;
            bool result = false;
            result = AddOrUpdate<Contrato>(vContrato);
            return result;
        }
        public bool AddOrUpdateMultipleContratos(List<Contrato> vContratos)
        {
            bool result = false;
            foreach (Contrato vContrato in vContratos)
            {
                vContrato.DataModificacao = DateTime.Now;
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdateContrato(vContrato);
            }
            return result;
        }

        public bool AddOrUpdateMultipleContratosWithCommit(List<Contrato> vContratos)
        {
            bool result = false;
            foreach (Contrato vContrato in vContratos)
            {
                vContrato.DataModificacao = DateTime.Now;
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
                result = AddOrUpdateContrato(vContrato);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public bool DeleteContrato(Contrato vContrato)
        {
            vContrato.DataModificacao = DateTime.Now;
            vContrato.Excluido = ConstantesModel.Excluido.SIM;
            bool result = false;
            result = Update<Contrato>(vContrato);
            return result;
        }
        public bool UpdateContrato(Contrato vContrato)
        {
            vContrato.DataModificacao = DateTime.Now;
            if(vContrato.Excluido != ConstantesModel.Excluido.CONTRATO_ENCERRADO)
            {
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
            }
            
            bool result = false;
            result = Update<Contrato>(vContrato);
            return result;
        }

        public bool UpdateMultipleContratos(List<Contrato> vContratos)
        {
            bool result = false;
            foreach (Contrato vContrato in vContratos)
            {
                vContrato.DataModificacao = DateTime.Now;
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdateContrato(vContrato);
            }
            return result;
        }
        public bool UpdateMultipleContratosWithCommit(List<Contrato> vContratos)
        {
            bool result = false;
            foreach (Contrato vContrato in vContratos)
            {
                vContrato.DataModificacao = DateTime.Now;
                vContrato.Excluido = ConstantesModel.Excluido.NAO;
                result = UpdateContrato(vContrato);
                CommitTransaction(startNewTransaction: true);
            }
            return result;
        }
        public IQueryable<Contrato> GetAllContratos()
        {
            IQueryable<Contrato> result = GetAll<Contrato>().Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }

        public IQueryable<Contrato> GetAllContratos(params Expression<Func<Contrato, object>>[] includes)
        {
            IQueryable<Contrato> result = GetAll<Contrato>(includes).Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }
        public Contrato FindContratoByContratoId(int Id1) //Por enquanto s� suporta chave primaria simples autoincremento
        {
            Contrato result = Find<Contrato>(x => x.ID == Id1).Where(x => x.Excluido != ConstantesModel.Excluido.SIM).FirstOrDefault();
            return result;
        }
        /*
        public Contrato FindContratoByContratoCodigo(string codigo)
        {
            Contrato result = Find<Contrato>(x => x.Codigo == codigo).FirstOrDefault();
            return result;
        }*/
        public IQueryable<Contrato> FindContrato(Expression<Func<Contrato, bool>> where1)
        {
            IQueryable<Contrato> result = Find<Contrato>(where1).Where(x => x.Excluido != ConstantesModel.Excluido.SIM);
            return result;
        }

        public IQueryable<Contrato> FindContrato(Expression<Func<Contrato, bool>> where, params Expression<Func<Contrato, object>>[] includes)
        {
            IQueryable<Contrato> result = Find<Contrato>(where, includes);
            return result;
        }
    }


    //******************************************************************************************************************************************************

}




