using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // Contrato
    public partial class ContratoConfiguration : EntityTypeConfiguration<Contrato>
    {
        public ContratoConfiguration()
            : this("dbo")
        {
        }

        public ContratoConfiguration(string schema)
        {
            ToTable(schema + ".Contrato");
            HasKey(x => x.ID);
			Property(x => x.ID).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

			
        
        
            Property(x => x.Aluno_ID).HasColumnName("Aluno_ID").HasColumnType("int");
        
            Property(x => x.PlanoServico_ID).HasColumnName("Plano_Servico_ID").HasColumnType("int");
            //Property(x => x.Transacoes_ID).HasColumnName("Transacoes_ID").HasColumnType("int");
            Property(x => x.DiaBasePagamento).HasColumnName("DiaBasePagamento").HasColumnType("int");
        
            Property(x => x.DataInicio).HasColumnName("DataInicio").HasColumnType("datetime");
            Property(x => x.Encerramento).HasColumnName("Encerramento").HasColumnType("datetime");
            Property(x => x.Multa).HasColumnName("Multa").HasColumnType("decimal");
            Property(x => x.ValorMensalidade).HasColumnName("ValorMensalidade").HasColumnType("decimal");

            Property(x => x.DataModificacao).HasColumnName("DataModificacao").HasColumnType("datetime");
        
            Property(x => x.Excluido).HasColumnName("Excluido").HasColumnType("char");

            HasOptional(a => a.Aluno).WithMany(b => b.Contratos).HasForeignKey(c => c.Aluno_ID);
            HasOptional(a => a.PlanoServico).WithMany(b => b.Contratos).HasForeignKey(c => c.PlanoServico_ID);

            InitializePartial();
        }
        partial void InitializePartial();
    }

}





