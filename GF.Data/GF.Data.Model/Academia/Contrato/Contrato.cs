
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;


namespace GF.Data.Model
{
    [DataContract]
    public partial class Contrato : EntidadeBase
    {
	
        [DataMember(Order = 2, IsRequired = false)]
        [Display(Name = "Aluno")]
        public int? Aluno_ID { get; set; }
        
        [DataMember(Order = 3, IsRequired = false)]
        [Display(Name = "Plano")]
        public int? PlanoServico_ID { get; set; }
        
        [DataMember(Order = 4, IsRequired = true)]
        [Display(Name = "Dia Base Pagamento")]
        [Range(1, 31,ErrorMessage = "Valor para {0} deve estar entre {1} e {2}.")]
        [Required]
        public int DiaBasePagamento { get; set; }
        
        [DataMember(Order = 5, IsRequired = true)]
        [Display(Name = "Data de In�cio")]
        [Required]
        public DateTime? DataInicio { get; set; }

        [DataMember(Order = 6, IsRequired = false)]
        [Display(Name = "Multa")]
        public decimal? Multa { get; set; }

        [DataMember(Order = 7, IsRequired = false)]
        [Display(Name = "Data de Encerramento")]
        public DateTime? Encerramento { get; set; }

        [DataMember(Order = 8, IsRequired = true)]
        [Display(Name = "Valor da Mensalidade")]
        public decimal ValorMensalidade { get; set; }

        /*[DataMember(Order = 8, IsRequired = false)]
        [Display(Name = "Transa��o")]
        public int? Transacoes_ID { get; set; }*/

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Aluno")]
        public virtual Aluno Aluno { get; set; }

        [ScriptIgnore(ApplyToOverrides = true)]
        [ForeignKey("Plano")]
        public virtual Plano_Servico PlanoServico { get; set; }

        

        [ForeignKey("RegistroFrequencia_ID")]
        public virtual ICollection<RegistroAtividade> RegistroAtividades { get; set; }

        [NotMapped]
        public string Aluno_Pessoa_Nome { get; set; }

        [NotMapped]
        public string Ano { get; set; }

        public Contrato()
        {
            
            InitializePartial();
        }

        partial void InitializePartial();
    }

}

