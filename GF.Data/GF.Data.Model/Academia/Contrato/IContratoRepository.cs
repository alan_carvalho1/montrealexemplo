
using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    public partial interface IContratoRepository : IRepositoryBase
    {
        Contrato AddContrato(Contrato vContrato);
        Contrato AddContratoWithoutTransaction(Contrato vContrato);
        bool AddMultipleContratos(List<Contrato> vContratos);
        bool AddMultipleContratosWithCommit(List<Contrato> vContratos);
        bool AddOrUpdateContrato(Contrato vContrato);
        bool AddOrUpdateMultipleContratos(List<Contrato> vContratos);
        bool AddOrUpdateMultipleContratosWithCommit(List<Contrato> vContratos);
        bool DeleteContrato(Contrato vContrato);
        bool UpdateContrato(Contrato vContrato);
        bool UpdateMultipleContratos(List<Contrato> vContratos);
        IQueryable<Contrato> GetAllContratos();
        IQueryable<Contrato> GetAllContratos(params Expression<Func<Contrato, object>>[] includes);
        Contrato FindContratoByContratoId(int Id1);
        IQueryable<Contrato> FindContrato(Expression<Func<Contrato, bool>> where1);
        IQueryable<Contrato> FindContrato(Expression<Func<Contrato, bool>> where, params Expression<Func<Contrato, object>>[] includes);
    }

}


