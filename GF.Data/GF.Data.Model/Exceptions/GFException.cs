﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GF.Data.Model
{
    public class GFException : Exception
    {

        public GFException() : base() { }

        public GFException(string message)
            : base(message)
        {
        }

        public GFException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }

    public class GFDuplicateException : Exception
    {

        public GFDuplicateException() : base() { }

        public GFDuplicateException(string message)
            : base(message)
        {
        }

        public GFDuplicateException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}