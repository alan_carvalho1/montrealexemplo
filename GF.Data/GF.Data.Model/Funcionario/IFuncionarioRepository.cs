// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
//[Template: Repository Interfaces][3]
	public partial interface IFuncionarioRepository : IRepositoryBase
    {
        Funcionario AddFuncionario(Funcionario funcionario);
        Funcionario AddFuncionarioWithoutTransaction(Funcionario funcionario);
        bool AddMultipleFuncionarios(List<Funcionario> funcionarios);
        bool AddMultipleFuncionariosWithCommit(List<Funcionario> funcionarios);
        bool AddOrUpdateFuncionario(Funcionario funcionario);
        bool AddOrUpdateMultipleFuncionarios(List<Funcionario> funcionarios);
        bool AddOrUpdateMultipleFuncionariosWithCommit(List<Funcionario> funcionarios);
        bool DeleteFuncionario(Funcionario funcionario);
        bool UpdateFuncionario(Funcionario funcionario);
        bool UpdateMultipleFuncionarios(List<Funcionario> funcionarios);
		IQueryable<Funcionario> GetAllFuncionarios();
		IQueryable<Funcionario> GetAllFuncioanrios(params Expression<Func<Funcionario, object>>[] includes); 
		Funcionario FindFuncionarioByFuncionarioId(int Id1);
		IQueryable<Funcionario> FindFuncionario(Expression<Func<Funcionario, bool>> where1);
        IQueryable<Funcionario> FindFuncionario(Expression<Func<Funcionario, bool>> where, params Expression<Func<Funcionario, object>>[] includes);
    }

}
// </auto-generated>
