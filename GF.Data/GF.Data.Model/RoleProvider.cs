﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using System.Web;
//using System.Web.Security;
using System.Configuration;
using System.Collections.Specialized;
using GF.Data.Model;


namespace GF.Data.Security
{

  

  public sealed class GFRoleProvider :   System.Web.Security.RoleProvider
  {

        UnitOfWork db;
        string pApplicationName;
        public override string ApplicationName
        {
          get { return pApplicationName; }
          set { pApplicationName = value; }
        } 

        public string applicationName { get { return "GF"; } set { pApplicationName = "GF"; } }
        public GFRoleProvider ()
        {
            db = new UnitOfWork();
        }

        public GFRoleProvider (UnitOfWork db1)
        {
            db = db1;
        }
      
        public override void AddUsersToRoles(string[]  Logins, string[] rolenames)
        {
            foreach (string Role in rolenames)
            {
                Grupo G = db.GrupoRepository.FindGrupo(g => g.Nome == Role).FirstOrDefault();
                if (G!=null)
                {
                    foreach (string Login in Logins)
                    {
                        if (IsUserInRole(Login,Role)) continue;
                        Funcionario U = db.FuncionarioRepository.FindFuncionario(u => u.Login == Login).FirstOrDefault();
                        if (U!=null)
                        {
                            db.FuncionarioGrupoRepository.Add(new FuncionarioGrupo { Grupo_ID = G.ID, Funcionario_ID = U.ID, Excluido = ConstantesModel.Excluido.NAO, DataModificacao = DateTime.Now });
                        }
                    }
                }
            }
        }

        public override void CreateRole(string rolename)
        { 
            throw new ProviderException("Não Implementado");
        }

        public override bool DeleteRole(string rolename, bool throwOnPopulatedRole)
        {
            throw new ProviderException("Não Implementado");
        }

        public override string[] GetAllRoles()
        {
            List<string> L = new List<string>();
            foreach (Grupo G in db.GrupoRepository.GetAllGrupos().Where(a => a.Excluido == ConstantesModel.Excluido.NAO))
            {
                L.Add(G.Nome);
            }
            return L.ToArray();
        }

        public override string[] GetRolesForUser(string Login)
        {
      
            var Q = (from U in db.Model.Funcionarios 
                        join GU in db.Model.FuncionariosGrupos on U.ID equals GU.Funcionario_ID
                        join G in db.Model.Grupos on GU.Grupo_ID equals G.ID
                        where U.Login==Login
                     select G.Nome);
            string[] R = Q.ToArray();
            return R;
        }

        public override string[] GetUsersInRole(string rolename)
        {
            var Q = (from U in db.Model.Funcionarios
                     join P in db.Model.Pessoas on U.Pessoa_ID equals P.ID
                         select P.Nome);
            string[] R = Q.ToArray();
            /*using (System.IO.StreamWriter file = 
            new System.IO.StreamWriter(@"C:\temp\r.txt"))
            {
           
                    file.WriteLine(rolename);
            
            }*/
            return R;
        }

        public override bool IsUserInRole(string Login, string rolename)
        {
            Funcionario Q = (from U in db.Model.Funcionarios
                     join GU in db.Model.FuncionariosGrupos on U.ID equals GU.Funcionario_ID
                     join G in db.Model.Grupos on GU.Grupo_ID equals G.ID
                     where (U.Login.ToLower() == Login.ToLower()) && (G.Nome.ToLower() == rolename.ToLower())
                     select U).FirstOrDefault();
            /*using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"C:\tmp\r.txt"))
            {
                
                {
                    file.WriteLine(Login);
                }
            }*/
            
            if (Q != null) 
                return true; 
            else 
                return false;
        
        
        }

        public override void RemoveUsersFromRoles(string[] Logins, string[] rolenames)
        {
            foreach (string Role in rolenames)
            {
                Grupo G = db.GrupoRepository.FindGrupo(g => g.Nome == Role).FirstOrDefault();
                if (G!=null)
                {
                    foreach (string Login in Logins)
                    {
                        Funcionario U = db.FuncionarioRepository.FindFuncionario(u => u.Login == Login).FirstOrDefault();
                        if (U!=null)
                        {
                            FuncionarioGrupo UG = db.FuncionarioGrupoRepository.FindFuncionarioGrupo(x => x.Funcionario_ID == U.ID && x.Grupo_ID == G.ID).FirstOrDefault();
                            if (UG != null) db.FuncionarioGrupoRepository.DeleteFuncionarioGrupo(UG);

                        }
                    }
                }
            }
        }

        public override bool RoleExists(string rolename)
        {
            throw new ProviderException("Não Implementado");
        }

        public override string[] FindUsersInRole(string rolename, string usernameToMatch)
        {
            throw new ProviderException("Não Implementado");
        }

        public override void Initialize(string name, NameValueCollection config)
        {

            // 
            // Initialize values from web.config. 
            // 

            if (name == null || name.Length == 0)
                name = "GFRoleProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Sample ODBC Role provider");
            }

            // Initialize the abstract base class. 
            base.Initialize(name, config);


            if (config["applicationName"] == null || config["applicationName"].Trim() == "")
            {
                pApplicationName = "GFRoleProvider";
            }
            else
            {
                pApplicationName = "GFRoleProvider";
            }


            if (config["writeExceptionsToEventLog"] != null)
            {
                if (config["writeExceptionsToEventLog"].ToUpper() == "TRUE")
                {
                    //pWriteExceptionsToEventLog = true;
                }
            }

        }
  }

  public class ProviderException : Exception 
  {
      public ProviderException(string Message) : base(Message) { }
 
  }
}


