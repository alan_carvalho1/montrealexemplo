// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
    // BemAnterior
    public partial class BemAnteriorConfiguration : EntityTypeConfiguration<BemAnterior>
    {
        public BemAnteriorConfiguration()
            : this("dbo")
        {
        }
 
        public BemAnteriorConfiguration(string schema)
        {
            ToTable(schema + ".BemAnterior");
            HasKey(x => x.Id);

            Property(x => x.Id).HasColumnName("Id").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.Numero).HasColumnName("NUMERO").IsOptional().HasColumnType("int");
            Property(x => x.Depen).HasColumnName("DEPEN").IsOptional().HasColumnType("int");
            Property(x => x.Nomedepen).HasColumnName("NOMEDEPEN").IsOptional().IsUnicode(false).HasColumnType("varchar").HasMaxLength(50);
            Property(x => x.Com).HasColumnName("COM").IsOptional().HasColumnType("int");
            Property(x => x.Dtaqu).HasColumnName("DTAQU").IsOptional().IsUnicode(false).HasColumnType("varchar").HasMaxLength(10);
            Property(x => x.Cont).HasColumnName("CONT").IsOptional().HasColumnType("int");
            Property(x => x.Vlaqui).HasColumnName("VLAQUI").IsOptional().HasColumnType("decimal").HasPrecision(18,2);
            Property(x => x.Vldeprec).HasColumnName("VLDEPREC").IsOptional().HasColumnType("decimal").HasPrecision(18,2);
            Property(x => x.Vlcontabil).HasColumnName("VLCONTABIL").IsOptional().HasColumnType("decimal").HasPrecision(18,2);
            Property(x => x.Descricao).HasColumnName("DESCRICAO").IsOptional().IsUnicode(false).HasColumnType("varchar").HasMaxLength(500);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
// </auto-generated>
