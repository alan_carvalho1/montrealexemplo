// <auto-generated>
// -------------------------------------------------------------------------------------
// Codigo gerado automaticamente - as classes s�o parciais para permitir customiza��o
// Altera��es neste arquivo ser�o perdidas se o c�digo for gerando novamente
// -------------------------------------------------------------------------------------
// TargetFrameworkVersion = 4.51
#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

using System;
using System.Data.Entity.Repository;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Collections.ObjectModel;
using System.Linq;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.Serialization;
using System.Threading;
using DatabaseGeneratedOption = System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption;

namespace GF.Data.Model
{
//[Template: POCO CLASS][4]
    // Usuario 
       [DataContract]
    
    public partial class Usuario : EntidadeBase
    {

		[DataMember(Order = 2, IsRequired = true)]
		[Required(ErrorMessage = "Campo obrigatório")] 
		[MaxLength(15, ErrorMessage = "Tamanho máximo do campo: 15 caracteres. ")]
		public string NomeCurto { get; set; } // NomeCurto


		[DataMember(Order = 3, IsRequired = true)]
		[Required(ErrorMessage = "Campo obrigatório")] 
		[MaxLength(200, ErrorMessage = "Tamanho máximo do campo: 200 caracteres. ")]
		public string Login { get; set; } // Login


		[DataMember(Order = 4, IsRequired = true)]
		[Required(ErrorMessage = "Campo obrigatório")] 
		[MaxLength(50, ErrorMessage = "Tamanho máximo do campo: 50 caracteres. ")]
		public string Nome { get; set; } // Nome


		[DataMember(Order = 5, IsRequired = false)]
				[MaxLength(200, ErrorMessage = "Tamanho máximo do campo: 200 caracteres. ")]
		public string Email { get; set; } // Email


		[DataMember(Order = 6, IsRequired = false)]
				[MaxLength(1, ErrorMessage = "Tamanho máximo do campo: 1 caracteres. ")]
		public override string Status { get; set; } // Status

        [DataMember(Order = 7, IsRequired = true)]
        [Required(ErrorMessage = "Campo obrigatório")]
        [MaxLength(50, ErrorMessage = "Tamanho máximo do campo: 50 caracteres. ")]
        public string Senha { get; set; } // Senha


        // Reverse navigation
        public virtual ICollection<UsuarioGrupo> UsuariosGrupos { get; set; } // Usuario_Grupo.FK_dbo.Usuario_Grupo_dbo.Usuario_IdUsuario
        
        public Usuario()
        {
            UsuariosGrupos = new List<UsuarioGrupo>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
